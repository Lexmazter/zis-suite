# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* TBD

### Version ###

* Qt Version used - LGPLv3 License - Qt 5.8

* Current ZiS Suite version - v0.0.0

### 3rd Party Modules ###

* QCharts.js - Based on Charts.js v1.1.0 - MIT License - https://github.com/Naruto-kyun/qchart.js/tree/v1.0.1

* QtXlsx v0.2.2 - MIT License - https://github.com/Alexander-r/QtXlsxWriter (with pageSetup disabled)

* CuteReport 1.3.1 - GPL/LGPLv3 License - https://cute-report.com/en/download (with some manual changes - untracked)

* Qt-Table-Printer - BSD license - https://github.com/T0ny0/Qt-Table-Printer/blob/master/LICENSE (documentation shall contain the copy-right notice and the disclaimer)

### Tech Used ###

* [Qt](https://www.qt.io/) - An awesome cross-platform IDE.
* [Dillinger](http://dillinger.io/) - Awesome web platform that made this document so handsome!
* [Icons8](https://icons8.com/) - Not actually a technology, but a nice platform for good-looking FREE icons. (Big thanks!)

### How do I get set up? ###

 * All you need to do is download the Qt IDE for your operating system, clone this repository, open the Qt Creator, click Open Project and then select ZiSSuite .pro.

    After that, you should be able to build, debug, edit and so on.

### Contribution guidelines ###

* __*Coding Guidelines*__
* Use camelCase as standard naming convention in the project -> ~~this_new_class~~ -> thisNewClass | ~~This_New_Function~~ -> thisNewFunction

* __*Icons Guidelines*__
* Use icons from Icons8, 48x48 dimensions from the __Color__ pack.

* __*Documentation Guidelines*__
* Doxygen from Qt will be used as a documentation purpose.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### To Do List ###
 * Add more information in this document
 * Additional comments need to be done
 * Doxygen documentation needs to be completed for most of the classes
 
* [further functionalities]:
*   -add functionality for configurations/settings  based on xml file
*   -implemention of basic charts 
*   -Rapoarte, bonuri, facturi (legat de iesiri)
*   -Expand database with more functionality
* [general tables]:
*   -~~create and use defines for the hardcoded column/row ids in tables~~
*   -add MessageBox warning when unsaved changes present
* [vanzari_produse_dialog]:
*   -fix the bug in vanzari_dialog for bills with only one item/quantity 
*   -~~propose the actual date as default bill name~~
*   -substract the sold quantity from nomenclator's stock for the sold items
* [receptii_form]:
*  -rework the form in a similar way as vanzari_form
*  -create the possibility to add new item in nomenclator if not available
*  -~~propose the actual date as default bill name~~

### Feature List ###
* Use ~~striketrough~~ when a feature is considered finished.
* This feature-list has been inspired from other tools - change accordingly.

* [EMITERE DOCUMENTE]
* - Facturi
* - Facturi Proforme
* - Facturi Avans
* - Facturi Stornare (per produs sau per suma)
* - Facturi Stornari Partiale (per produs sau per suma)
* - Facturi pe baza de Proforma
* - Facturi pe baza de Aviz
* - Facturi pe baza de Chitanta
* - Chitante pe baza de facturi
* - Chitante de Avans
* - Avize
* - Emitere documente in alb
* - Aplicare Discount (valoare fixa sau procent)
* - Facturi Externe
* - Posibilitate Adaugare Limbi noi si Modificare Traduceri Existente
* - Facturi in alte monede
* - Posibilitate Evidenta Preturi in alte Monede
* - Corectare Documente Emise
* - Anulare Documente
* - Emitere documente folosind numere cu mai mult de 2 zecimale
* - Posibilitate adaugare detalii pentru produse
* [MANAGEMENT DOCUMENTE]
* - Tiparire documente in masa cu 1 click
* - Coloana de Observatii in Rapoarte
* [INCASARI]
* - Incasare facturi cu/fara chitanta
* - Incasari
* - Incasare mai multe Facturi pe o singura Chitanta
* [INCASARI CU BON - CONECTARE LA CASA DE MARCAT]
* - Emitere factura si bon direct din program
* - Emitere bon fara factura din program
* - Introducere CIF client pe Bon Fiscal pt deducerea TVA
* - Incasare cu card a bonurilor fiscale
* - Incasare cu card a bonurilor fiscale
* - Eltrade, Activa, Datecs, Sapel, Partner
* [FORMAT / MODELE DOCUMENTE]
* - Model Factura A4
* - 1 model	3 modele	3 modele
* - Model Factura A5
* - 1 model	3 modele	3 modele
* - Factura + Chitanta pe o foaie A4
* - 2 Facturi + 2 Chitante pe o foaie A4
* - 3 Documente pe o foaie A4
* [PERSONALIZARE DOCUMENTE]
* - Documente cu logo/sigla
* - Documente cu ștampila
* - Personalizare Documente cu Fundal
* - Documentele apar semnate si stampilate
* - Posibilitate adaugare date in antet
* - Afisare/Ascundere info despre emitent
* - Introducerea mai multor IBAN-uri in documente.
* [SALVARE DOCUMENTE / DATE]
* - Salvare in Program
* - Salvare in format .PDF
* - Salvare / Backup Baza de Date
* - Restaurare date dintr-o Salvare / Backup
* [SERII DOCUMENTE]
* - Gestionare numar nelimitat de serii
* - TVA
* - Facilitati pt TVA la Incasare
* - Multiple Cote TVA (inclusiv SDD si SFDD)
* - Definire Cote TVA Proprii
* - Suport pentru Neplatitori de TVA
* [FACTURARE INTELIGENTA]
* - Invatare pe masura utilizarii
* - Autocompletare documente cu informatii Invatate (emitere documente in 30 de secunde)
* - Preluare Date Clienti de pe Internet(dupa CIF)
* - Posibilitate Precompletare facturi (pentru facturi recurente)
* - Trimiterea documentelor prin e-mail Direct din program
* - Posibilitatea de autocompletare a mesajului din email cu datele clientului
* - Curs Valutar Preluat de pe Internet
* - Panou informatii importante
* - IBAN-uri inteligente
* [RETEA]
* - Utilizare in Retea
* [NOMENCLATOARE]
* - Clienti
* - Produse
* - Import Excel
* - Export Excel
* [SISTEM MULTI-UTILIZATOR]
* - Intrare in program pe baza de parola
* - Jurnal Activitate pt fiecare dintre Utilizatori
* - Adaugare / Modificare / Stergere Utilizatori
* - Alocare Drepturi Emitere pentru Utilizatori
* [RAPOARTE]
* - Rapoarte: facturi, chitante, avize, proforme
* - Raport Incasari
* - Raport operatiuni client (Fisa client)
* - Grafice
* - Declaratia 394
* - Decizie de Inseriere
* - Export Rapoarte
* [FACILITATI SUPLIMENTARE]
* - Posibilitate Trimitere Sugestii din Program
* - Verificare Aparitie Versiune Noua