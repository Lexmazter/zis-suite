#include "currencyinfo_helper.h"

CurrencyInfo_Helper::CurrencyInfo_Helper(QWidget *parent):QWidget(parent)
{
    m_parent = parent;

    tempPage = new QWebEnginePage;

    // Use this function to safely delete on exit
    tempPage->setParent(parent);

    connect(tempPage, SIGNAL(loadFinished(bool)), this, SLOT(pageLoadFinished(bool)));

    link_BNR_ConversionRate = "http://www.cursbnr.ro/";

    tempPage->load(link_BNR_ConversionRate);

    // Set the wait cursor while we get the info
    QApplication::setOverrideCursor(Qt::WaitCursor);

}

void CurrencyInfo_Helper::pageLoadFinished(bool state)
{
    switch(state)
    {
    case true:
    {
        QString javaScriptCheckForError = "document.getElementById(\"mainbox\").innerText";

        QString javaScriptParseTable = "var c = document.getElementById(\"tabel_valute\");"
                                       "var table = c.rows;"
                                       "var text = \"\";"
                                       "for (i=1; i<table.length; i++)"
                                       "{"
                                       " text += table[i].cells[0].textContent + \",\" + table[i].cells[2].textContent + \",\";  "
                                       "}"
                                       "";

//        tempPage->runJavaScript(javaScriptCheckForError, [this] (const QVariant &result)
//        {
//            qDebug() << result;

//            if(result != QVariant::Invalid)
//            {
//                tempPage->blockSignals(true);
//                QString message = result.toString().trimmed();

//                // Prepare a messagebox
//                QMessageBox msgBox(m_parent);
//                QPixmap icon(":/Icons/Error-48.png");
//                msgBox.setIconPixmap(icon);
//                msgBox.setText(tr("Nu se pot prelua datele!"));
//                msgBox.setInformativeText(tr("Următoarea eroare a fost returnată: \n") + message);
//                msgBox.setStandardButtons(QMessageBox::Ok);
//                msgBox.setDefaultButton(QMessageBox::Ok);
//                msgBox.exec();

//                emit loadFinished(false);

//                // Restore the cursor after parsing the data
//                QApplication::restoreOverrideCursor();

//                return;
//            }
//        });

        tempPage->runJavaScript(javaScriptParseTable, [this] (const QVariant &result)
        {
            qDebug() << result;

            if(result != QVariant::Invalid)
            {
                // JavaScript callback using Lambda
                QString str = result.toString().remove("\n").remove("\t").trimmed();
                QStringList list = str.split(',');
                loadResultStringToList(list);

                emit loadFinished(true);

                // Restore the cursor after parsing the data
                QApplication::restoreOverrideCursor();
            }
        });

        break;
    }

    case false:
        // Prepare a messagebox
        QMessageBox msgBox(m_parent);
        QPixmap icon(":/Icons/Disconnected-48.png");
        msgBox.setIconPixmap(icon);
        msgBox.setText(tr("Nu se poate incărca pagina!"));
        msgBox.setInformativeText(tr("Înainte de a încerca din nou, vă rugăm să verificați conexiunea la internet."));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();

        emit loadFinished(false);

        // Restore the cursor after parsing the data
        QApplication::restoreOverrideCursor();
        break;
    }
}

void CurrencyInfo_Helper::loadResultStringToList(QStringList list)
{
    for(int i = 0; i < list.count(); i+=2)
    {
        // prevent index out of bounds error
        if(i+1 < list.count())
        {
            infoCurrency_Map.insert(list[i].trimmed().toUtf8(), list[i+1].trimmed().toUtf8());
            qDebug() << "list[i] = " << list[i].trimmed() << "list[i+1] = " << list[i+1].trimmed();
        }
    }
}

QHash <QString, QString> *CurrencyInfo_Helper::getInfoCurrency()
{
    return &infoCurrency_Map;
}
