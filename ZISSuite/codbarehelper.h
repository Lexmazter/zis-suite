#ifndef CODBAREHELPER_H
#define CODBAREHELPER_H

#include <QWidget>
#include <QtWebEngineWidgets>
#include <QMovie>

namespace Ui {
class codBareHelper;
}

class codBareHelper : public QWidget
{
    Q_OBJECT

public:
    explicit codBareHelper(QWidget *parent = 0);
    ~codBareHelper();

private slots:
    void on_toolButtonProducator_clicked();

    void on_toolButtonProdus_clicked();

    void pageLoadFinished(bool);

private:
    Ui::codBareHelper *ui;
    QWebEnginePage* tempPage;
    QUrl linkGEPIR;
    QMovie movie;
    void loadStringToListWidget(QStringList list);
};

#endif // CODBAREHELPER_H
