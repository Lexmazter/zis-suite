#include "chartswidget.h"
#include "ui_chartswidget.h"
#include <QQuickItem>

/* These are used for JS type selection */
#define CHART_BAR_JS 1
#define CHART_DOUGHNUT_JS 2
#define CHART_LINE_JS 3
#define CHART_PIE_JS 4

/* These are used for coloring */
#define COLOR_FILL 0
#define COLOR_STROKE 1
#define COLOR_RESET 2

ChartsWidget::ChartsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChartsWidget)
{
    ui->setupUi(this);

    m_datasetNumber = 0;

    // Better performance with QQuickView than QQuickWidget!!!
    view = new QQuickView();
    view->setSource(QUrl("qrc:/chartHelper.qml"));
    QWidget *horse = QWidget::createWindowContainer(view);
    view->setResizeMode(QQuickView::SizeRootObjectToView);

    ui->frame->layout()->addWidget(horse);
}

ChartsWidget::~ChartsWidget()
{
    delete view;
    delete ui;
}

/************************
 * Local functions start
 ************************/

int getRandomUint8()
{
    // Helper function for randomly generating a value from 0-255
    int random = (qrand() % ((255+1)-0)+0);

    return random;
}

QColor getRandomColor()
{
    // Get a random color, we use this for hex format
    QColor newColor;
    newColor.setRed(getRandomUint8());
    newColor.setGreen(getRandomUint8());
    newColor.setBlue(getRandomUint8());

    return newColor;
}

QString getRgbaColor(int type)
{
    // Get a random color prepared in RGBA format for JSON
    static int colorR = getRandomUint8();
    static int colorG = getRandomUint8();
    static int colorB = getRandomUint8();

    switch(type){
    case COLOR_FILL:
        return QString("\"rgba("+QString::number(colorR)+","+QString::number(colorG)+","+QString::number(colorB)+",0.5)\"");
        break;

    case COLOR_STROKE:
        return QString("\"rgba("+QString::number(colorR)+","+QString::number(colorG)+","+QString::number(colorB)+",1)\"");
        break;

    case COLOR_RESET:
        colorR = getRandomUint8();
        colorG = getRandomUint8();
        colorB = getRandomUint8();
        return "";
        break;
    default:
        return QString::number(colorR)+","+QString::number(colorG)+","+QString::number(colorB);
        break;
    }
}

/************************
 * Local functions end
 ************************/

void ChartsWidget::addDataset(int chartType ,QString name, QString data, QString labels)
{
    // we cannot change the labels after the first set
    // for multi sets charts
    if(m_datasetNumber == 0)
    {
        chartLabels = labels;
    }

    // we save the chart type because we use it for show method
    this->chartType = chartType;

    switch(chartType)
    {
    // chart line, area and bar - multiple datasets supported
    case CHART_LINE:
    case CHART_AREA:
    case CHART_BAR:
    {
        // Store all the relevant data in the container
        DatasetListContainer temp;
        temp.id = m_datasetNumber;
        temp.type = chartType;
        temp.colorRgbaFill = getRgbaColor(COLOR_FILL);
        temp.colorRgbaStroke = getRgbaColor(COLOR_STROKE);
        temp.dataString = data;
        QStringList horse = getRgbaColor(255).split(',');
        temp.color = QColor(horse[0].toInt(), horse[1].toInt(), horse[2].toInt());
        temp.name = name;

        // Reset the color after we save it
        getRgbaColor(COLOR_RESET);

        // Add the container to the list
        datasetList.append(temp);

        // Incerement the dataset number so we can see how many we have
        m_datasetNumber++;

        break;
    }
    // chart doughnut and pie - single dataset only
    case CHART_PIE:
    case CHART_DOUGHNUT:
        chartValues = data;
        break;

    }
}

void ChartsWidget::clearDatasets()
{
    datasetList.clear();
    chartType = 0;
    m_datasetNumber = 0;
    chartLabels = "";
    chartValues = "";
}

int ChartsWidget::datasetsCount()
{
    return m_datasetNumber;
}

void ChartsWidget::showChart()
{
    QQuickItem *object = view->rootObject();

    switch (chartType) {
    case CHART_LINE: // Grafic Line

        QMetaObject::invokeMethod(object, "setType", Q_ARG(QVariant, CHART_LINE_JS));

        QMetaObject::invokeMethod(object, "setOptions", Q_ARG(QVariant, "{\"bezierCurve\": false, \"datasetFill\": false}"));

        QMetaObject::invokeMethod(object, "setData", Q_ARG(QVariant, getDatasetString()));
        break;
    case CHART_AREA: // Grafic Arie
        QMetaObject::invokeMethod(object, "setType", Q_ARG(QVariant, CHART_LINE_JS));

        QMetaObject::invokeMethod(object, "setOptions", Q_ARG(QVariant, "{\"bezierCurve\": true, \"datasetFill\": true}"));

        QMetaObject::invokeMethod(object, "setData", Q_ARG(QVariant, getDatasetString()));
        break;

    case CHART_BAR: // Grafic Bare
        QMetaObject::invokeMethod(object, "setType", Q_ARG(QVariant, CHART_BAR_JS));

        QMetaObject::invokeMethod(object, "setData", Q_ARG(QVariant, getDatasetString()));

        break;

    case CHART_PIE: // Grafic Pie
    {
        // Clean the legend if there is something there
        if(!ui->frameLegend->layout()->isEmpty())
        {
            QLayoutItem* item;
            while(( item = ui->frameLegend->layout()->takeAt(0)) != NULL)
            {
                delete item->widget();
                delete item;
            }
        }

        QMetaObject::invokeMethod(object, "setType", Q_ARG(QVariant, CHART_PIE_JS));

        QStringList labelList = chartLabels.split(',');
        QStringList valueList = chartValues.split(',');
        QString pieData = "[";

        for(int i=0; i < valueList.count(); i++)
        {
            if(i != (valueList.count()-1))
            {
                QColor tempColor = getRandomColor().lighter(120);
                pieData += "{\"value\":"+valueList[i]+", \"color\": \""+tempColor.name()+"\"},";
                setLegendRound(tempColor,labelList[i],valueList[i]);

            }
            else
            {
                QColor tempColor = getRandomColor().lighter(120);
                pieData += "{\"value\":"+valueList[i]+", \"color\": \""+tempColor.name()+"\"}";
                setLegendRound(tempColor,labelList[i],valueList[i]);
            }
        }

        pieData += "]";

        QMetaObject::invokeMethod(object, "setData", Q_ARG(QVariant, pieData));

        break;
    }

    case CHART_DOUGHNUT: // Grafic Doughnut
    {
        // Clean the legend if there is something there
        if(!ui->frameLegend->layout()->isEmpty())
        {
            QLayoutItem* item;
            while(( item = ui->frameLegend->layout()->takeAt(0)) != NULL)
            {
                delete item->widget();
                delete item;
            }
        }

        QMetaObject::invokeMethod(object, "setType", Q_ARG(QVariant, CHART_DOUGHNUT_JS));

        QStringList labelList = chartLabels.split(',');
        QStringList valueList = chartValues.split(',');
        QString douData = "[";

        for(int i=0; i < valueList.count(); i++)
        {
            if(i != (valueList.count()-1))
            {
                QColor tempColor = getRandomColor().lighter(120);
                douData += "{\"value\":"+valueList[i]+", \"color\": \""+tempColor.name()+"\"},";
                setLegendRound(tempColor,labelList[i],valueList[i]);
            }
            else
            {
                QColor tempColor = getRandomColor().lighter(120);
                douData += "{\"value\":"+valueList[i]+", \"color\": \""+tempColor.name()+"\"}";
                setLegendRound(tempColor,labelList[i],valueList[i]);
            }
        }

        douData += "]";

        QMetaObject::invokeMethod(object, "setData", Q_ARG(QVariant, douData));

        break;
    }
    default:
        break;
    }
}

QString ChartsWidget::getJsonCompatibleLabel()
{
    // Parse the labels in string form to be JSON ready
    QString retVal = "";
    QStringList tempVal = chartLabels.split(',');

    for(int i = 0; i < tempVal.count(); i++)
    {
        if(i != (tempVal.count()-1))
        {
            retVal += "\"" +tempVal[i]+ "\",";
        }
        else
        {
            retVal += "\"" +tempVal[i]+ "\"";
        }
    }

    return retVal;
}

QString ChartsWidget::getDatasetString()
{
    QString retVal = "";

    // Clean the legend if there is something there
    if(!ui->frameLegend->layout()->isEmpty())
    {
        QLayoutItem* item;
        while(( item = ui->frameLegend->layout()->takeAt(0)) != NULL)
        {
            delete item->widget();
            delete item;
        }
    }

    // Parse the dataset and add everything
    for(int i =0; i < datasetList.count(); i++)
    {
        if(i==0)
        {
            retVal = "{"
                     "\"labels\": ["+getJsonCompatibleLabel()+"],"
                                                              "\"datasets\": [{"
                                                              "\"fillColor\": "+datasetList[i].colorRgbaFill+","
                                                                                                             "\"strokeColor\": "+datasetList[i].colorRgbaStroke+","
                                                                                                                                                                "\"pointColor\": "+datasetList[i].colorRgbaStroke+","
                                                                                                                                                                                                                  "\"pointStrokeColor\": \"#ffffff\","
                                                                                                                                                                                                                  "\"data\": ["+datasetList[i].dataString+"]}]}";
        }
        else
        {
            retVal.remove(retVal.count()-2, 2);
            retVal += ",{"
                      "\"fillColor\": "+datasetList[i].colorRgbaFill+","
                                                                     "\"strokeColor\": "+datasetList[i].colorRgbaStroke+","
                                                                                                                        "\"pointColor\": "+datasetList[i].colorRgbaStroke+","
                                                                                                                                                                          "\"pointStrokeColor\": \"#ffffff\","
                                                                                                                                                                          "\"data\": ["+datasetList[i].dataString+"]}]}";
        }

        // Set the color label for the legend
        QLabel *setColorLabel = new QLabel(this);
        setColorLabel->setFrameShape(QFrame::Shape::Panel);
        setColorLabel->setText("    ");
        setColorLabel->sizePolicy().setHorizontalPolicy(QWidget::sizePolicy().Fixed);
        QPalette pal = setColorLabel->palette();
        setColorLabel->setLineWidth(2);
        pal.setColor(setColorLabel->foregroundRole(), datasetList[i].color.lighter(80));
        pal.setColor(setColorLabel->backgroundRole(), datasetList[i].color.lighter(120));
        setColorLabel->setPalette(pal);
        setColorLabel->setAutoFillBackground(true); // rem this to see the difference
        ui->frameLegend->layout()->addWidget(setColorLabel);

        // Set the name of the dataset near the color
        QLabel *setColorName = new QLabel(this);
        setColorName->setStyleSheet("QLabel { color : #6a6a6a; }");
        setColorName->setText(" "+datasetList[i].name);
        ui->frameLegend->layout()->addWidget(setColorName);
    }

    return retVal;
}

void ChartsWidget::setLegendRound(QColor color, QString label, QString value)
{
    QString retVal = "";

    // Set the color label for the legend
    QLabel *setColorLabel = new QLabel(this);
    setColorLabel->setFrameShape(QFrame::Shape::Panel);
    setColorLabel->setText("    ");
    setColorLabel->sizePolicy().setHorizontalPolicy(QWidget::sizePolicy().Fixed);
    QPalette pal = setColorLabel->palette();
    setColorLabel->setLineWidth(2);
    pal.setColor(setColorLabel->foregroundRole(), color.lighter(80));
    pal.setColor(setColorLabel->backgroundRole(), color.lighter(120));
    setColorLabel->setPalette(pal);
    setColorLabel->setAutoFillBackground(true); // rem this to see the difference
    ui->frameLegend->layout()->addWidget(setColorLabel);

    // Set the name of the dataset near the color
    QLabel *setColorName = new QLabel(this);
    setColorName->setStyleSheet("QLabel { color : #6a6a6a; }");
    setColorName->setText(label+": "+value);
    ui->frameLegend->layout()->addWidget(setColorName);

}
