#ifndef CURRENCYINFO_HELPER_H
#define CURRENCYINFO_HELPER_H

#include <QWidget>
#include <QtWebEngineWidgets>
#include <QMovie>
#include <QUrl>

class CurrencyInfo_Helper:public QWidget
{
    Q_OBJECT

signals:
    void loadFinished(bool state);

public:
    CurrencyInfo_Helper(QWidget *parent = 0);
    QHash <QString, QString> *getInfoCurrency();


private slots:
    void pageLoadFinished(bool);

private:
    QWidget *m_parent;
    QWebEnginePage* tempPage;
    QUrl link_BNR_ConversionRate;
    QHash <QString, QString> infoCurrency_Map;
    bool noError = false;
    void loadResultStringToList(QStringList list);
};

#endif // CURRENCYINFO_HELPER_H
