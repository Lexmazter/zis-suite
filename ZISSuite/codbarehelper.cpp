#include "codbarehelper.h"
#include "ui_codbarehelper.h"
#include <QUrl>

codBareHelper::codBareHelper(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::codBareHelper)
{
    tempPage = new QWebEnginePage;

    // Use this function to safely delete on exit
    tempPage->setParent(this);

    connect(tempPage, SIGNAL(loadFinished(bool)), this, SLOT(pageLoadFinished(bool)));

    linkGEPIR = "http://www.coduridebareonline.ro/codaloc/gepir/index.jsf";
    movie.setFileName(":/Animations/loader.gif");

    ui->setupUi(this);

    this->setWindowTitle("Coduri de bare");
    QIcon bill_icon(":/Icons/Barcode-48.png");
    this->setWindowIcon(bill_icon);
}

codBareHelper::~codBareHelper()
{
    delete ui;
}

void codBareHelper::on_toolButtonProducator_clicked()
{
    ui->listWidget->clear();
    ui->labelAnimation->setMovie(&movie);
    movie.start();

    tempPage->blockSignals(false);
    linkGEPIR.setQuery("v="+ui->lineEdit->text()+"&t=1");
    tempPage->load(linkGEPIR);
}

void codBareHelper::on_toolButtonProdus_clicked()
{
    ui->listWidget->clear();
    ui->labelAnimation->setMovie(&movie);
    movie.start();

    tempPage->blockSignals(false);
    linkGEPIR.setQuery("v="+ui->lineEdit->text()+"&t=0");
    tempPage->load(linkGEPIR);
}

void codBareHelper::pageLoadFinished(bool state)
{
    switch(state)
    {
    case true:
    {
        QString javaScriptCheckForError = "document.getElementsByClassName(\"ui-messages-error-detail\")[0].innerText";
        QString javaScriptParseTable = "var c = document.getElementsByClassName(\"ui-datatable-even\")[0].childNodes;"
                                       "var text = \"\";"
                                       "for (i=0; i<c.length; i++)"
                                       "{"
                                       " text += c[i].textContent + \",\";  "
                                       "}"
                                       "                                        ";

        tempPage->runJavaScript(javaScriptCheckForError, [this] (const QVariant &result)
        {
            qDebug() << result;

            if(result != QVariant::Invalid)
            {
                tempPage->blockSignals(true);
                QString message = result.toString();

                // Prepare a messagebox
                QMessageBox msgBox(this);
                QPixmap icon(":/Icons/Barcode Error-48.png");
                msgBox.setIconPixmap(icon);
                msgBox.setText(tr("Nu se pot prelua datele!"));
                msgBox.setInformativeText(tr("Următoarea eroare a fost returnată: ") + message);
                msgBox.setStandardButtons(QMessageBox::Ok);
                msgBox.setDefaultButton(QMessageBox::Ok);
                msgBox.exec();
                return;
            }
        });
        tempPage->runJavaScript(javaScriptParseTable, [this] (const QVariant &result)
        {
            if(result != QVariant::Invalid)
            {
                // JavaScript callback using Lambda
                QString str = result.toString();
                QStringList list = str.split(',');
                loadStringToListWidget(list);
            }
        });
        break;
    }

    case false:
        // Prepare a messagebox
        QMessageBox msgBox(this);
        QPixmap icon(":/Icons/Disconnected-48.png");
        msgBox.setIconPixmap(icon);
        msgBox.setText(tr("Nu se poate incărca pagina!"));
        msgBox.setInformativeText(tr("Înainte de a încerca din nou, vă rugăm să verificați conexiunea la internet."));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();
        break;
    }
}

void codBareHelper::loadStringToListWidget(QStringList list)
{   
    for(int i = 0; i < list.count(); i++)
    {
        if(list[i]!= "")
        {
            ui->listWidget->addItem(list[i].trimmed());
        }
    }

    movie.stop();
    ui->labelAnimation->setPixmap(QPixmap(":/Icons/Barcode-48.png"));
}
