#ifndef EXPORT_DIALOGBOX_H
#define EXPORT_DIALOGBOX_H

#include <QDialog>
#include <QListWidgetItem>

namespace Ui {
class Export_DialogBox;
}

class Export_DialogBox : public QDialog
{
    Q_OBJECT

public:
    explicit Export_DialogBox(QWidget *parent = 0);
    ~Export_DialogBox();

private slots:
    void on_pushButton_Close_clicked();

    void on_saveButton_clicked();

    void excelExportFunction(const QString fileName);
    void pdfExportFunction(const QString fileName);
private:
    Ui::Export_DialogBox *ui;
};

#endif // EXPORT_DIALOGBOX_H
