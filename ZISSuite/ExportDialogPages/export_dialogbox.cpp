#include "export_dialogbox.h"
#include "ui_export_dialogbox.h"
#include "mainwindow.h"
#include "xlsxdocument.h"
#include "ExportDialogPages/tableprinter.h"
#include <QtPrintSupport/QPrinter>
#include <QPainter>
#include <QMovie>

#define EXCEL (0)
#define PDF   (1)

Export_DialogBox::Export_DialogBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Export_DialogBox)
{
     ui->setupUi(this);
     this->setWindowTitle("Export");
     QIcon export_icon(":/Icons/Export-48.png");
     this->setWindowIcon(export_icon);

}

Export_DialogBox::~Export_DialogBox()
{
    delete ui;
}

void Export_DialogBox::pdfExportFunction(const QString fileName)
{
    if(fileName.contains(".pdf"))
    {
        MainWindow *mainWindow = MainWindow::getInstance();
        QTableView *actualTable = mainWindow->getCurrentTable();

        if(actualTable != nullptr)
        {
            QPrinter printer(QPrinter::ScreenResolution);
            printer.setOutputFileName(fileName);
            printer.setPaperSize(QPrinter::A4);
            printer.setOutputFormat(QPrinter::PdfFormat);
            printer.setOrientation(QPrinter::Landscape);

            QPainter painter(&printer);
            painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
            TablePrinter tablePrinter(&painter, &printer);
            QFont headerFont( "Tokyo" );
            headerFont.setPointSize( 10 );
            headerFont.setWeight( QFont::Bold );
            tablePrinter.setHeadersFont(headerFont);
            QVector<int> columnStretch = QVector<int>();
            for(int i=0; i< actualTable->model()->columnCount(); i++)
            {
                //default values for column stretch
                columnStretch.insert(i, 1);
            }
            QVector<QString> headerVector = QVector<QString>();
            for(int i=0; i < actualTable->horizontalHeader()->model()->columnCount(); i++)
            {
//                QString tempVal = actualTable->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString();
//                if(tempVal.length() > 5)
//                    columnStretch[i] = tempVal.length()%5 + 1 ;

                headerVector.insert(i, actualTable->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString());
            }
            if(! tablePrinter.printTable(actualTable->model(), columnStretch, headerVector)) {
                qDebug() << tablePrinter.lastError();
            }
            //            actualTable->render( &painter );
            painter.end();
        }
        else{
            QMessageBox::warning(this, tr("Avertizare"), tr("Niciun tabel nu este activ !"));
        }
    }
    else
    {
        QMessageBox::warning(this, tr("Avertizare"), tr("Verifica calea fisierului !"));
    }
}

void Export_DialogBox::excelExportFunction(const QString fileName)
{
    if(fileName.contains(".xlsx"))
    {
        // our document
        QXlsx::Document xlsx;
        xlsx.setDocumentProperty("creator", "SoftRocks");

        // use the format for the header
        QXlsx::Format formatHeader;
        formatHeader.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        formatHeader.setBorderStyle(QXlsx::Format::BorderMedium);

        MainWindow *mainWindow = MainWindow::getInstance();
        QTableView *currentTable = mainWindow->getCurrentTable();

        int maxCellWidth = 0;

        if(currentTable != nullptr)
        {
            // parse all the columns
            for(int c=0; c < currentTable->model()->columnCount(); c++)
            {
                // reset the column width
                maxCellWidth = 0;

                // parse each row
                for(int r=0; r < currentTable->model()->rowCount(); r++)
                {
                    // header for the first row
                    if(r==0)
                    {
                        QString tempString = " " + currentTable->model()->headerData(c, Qt::Horizontal).toString() + " ";

                        if(maxCellWidth < tempString.size())
                            maxCellWidth = tempString.size()+1;

                        xlsx.write(r+1, c+1, tempString, formatHeader);
                    }
                    else
                    {
                        QString tempString = " " + currentTable->model()->data(currentTable->model()->index(r, c), Qt::DisplayRole).toString() + " ";

                        if(maxCellWidth < tempString.size())
                            maxCellWidth = tempString.size()+1;

                        xlsx.write(r+1,c+1,tempString);
                    }
                }

                // set the column width according to the largest cell
                xlsx.setColumnWidth(c+1, maxCellWidth);

            }

            xlsx.saveAs(QDir().toNativeSeparators(fileName).toUtf8());
        }
        else
        {
            QMessageBox::warning(this, tr("Avertizare"), tr("Niciun tabel nu este activ !"));
        }
    }
    else
    {
        QMessageBox::warning(this, tr("Avertizare"), tr("Verifica calea fisierului !"));
    }
}

void Export_DialogBox::on_pushButton_Close_clicked()
{
    this->close();
}

void Export_DialogBox::on_saveButton_clicked()
{
    // Show our loading animation
    ui->animationLabel->setVisible(true);
    QMovie *movie = new QMovie(":/Animations/loader.gif");
    ui->animationLabel->setMovie(movie);
    movie->start();

    MainWindow *mainWindow = MainWindow::getInstance();

    switch (ui->listWidget->currentIndex().row())
    {
    case EXCEL:
    {
        QString fileName = QFileDialog::getSaveFileName(this,
                                                        tr("Salvare ca.."),
                                                        mainWindow->getStatusText()+"_"+QDate::currentDate().toString().remove(" ").remove("."),
                                                        tr("Excel (*.xlsx)"));

        excelExportFunction(fileName);

        break;
    }
    case PDF:
    {
        QString fileName = QFileDialog::getSaveFileName(this,
                                                        tr("Salvare ca.."),
                                                        mainWindow->getStatusText()+"_"+QDate::currentDate().toString().remove(" ").remove("."),
                                                        tr("PDF (*.pdf)"));

        pdfExportFunction(fileName);

        break;
    }
    default:
        break;
    }

    // Hide our loading animation
    ui->animationLabel->setVisible(false);

    QMessageBox::information(this, tr("Succes!"), tr("Exportul a fost realizat cu succes!"));
}
