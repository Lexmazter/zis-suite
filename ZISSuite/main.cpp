#include <Forms/login_form.h>
#include <QApplication>
#include <QMessageBox>
#include <CuteReport>

#define D_Development_Mode

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
#ifdef D_Development_Mode
    {
        int LoginRetVal = DbHelper::Login("zis", "demo");
        MainWindow* w = MainWindow::getInstance();
        w->setCurrentUser("Development Mode");
        w->show();
    }
#else
    login_form l;
    l.show();
#endif

    return a.exec();
}
