#ifndef CUTEREPORT_HELPER_H
#define CUTEREPORT_HELPER_H

#include <CuteReport>
#include <QAbstractTableModel>
#include "firmsettings_helper.h"

class CuteReport_Helper : public QObject
{
    Q_OBJECT
public:
    explicit CuteReport_Helper(QObject *parent = 0, QString reportPath = "", QString pageName = "");
    ~CuteReport_Helper();
    void fillDetailsBandForAbstractTable(QAbstractTableModel *tableModel);
    void fillDetailsBandForList(QStringList list);
    void renderRaportPreview(QWidget *parent, QString);
    void fillNamedMemo(QString momoName, QString value);
    void fillReportDataForReceipt(QVariantMap *dataThirds, QString currentDate, QString date, QString Price, QString splledNumber, QString receiptID);
    void fillReportDataForProforma(int billID, QAbstractTableModel* customTableModel);
    void fillReportDataForBill(int billID, QAbstractTableModel* customTableModel);

private:
    CuteReport::ReportCore *cuteReport = nullptr;
    CuteReport::ReportInterface *reportObject = nullptr;
    CuteReport::ReportPreview *preview = nullptr;
    QString pageName;
    FirmSettings_Helper *FirmDataSettings;
    int C_NO_Bank_Accounts_Limit = 4;
};

#endif // CUTEREPORT_HELPER_H
