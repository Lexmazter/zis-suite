#ifndef CHARTSWIDGET_H
#define CHARTSWIDGET_H

#include <QWidget>
#include <QQuickView>
#include <QMetaObject>
#include <QLabel>

/* These are used for this class */
#define CHART_LINE 0
#define CHART_AREA 1
#define CHART_PIE 2
#define CHART_BAR 3
#define CHART_DOUGHNUT 4

namespace Ui {
class ChartsWidget;
}

class ChartsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ChartsWidget(QWidget *parent = 0);
    ~ChartsWidget();

    void showChart();
    void addDataset(int chartType, QString name, QString data, QString labels);
    void clearDatasets();
    int datasetsCount();


private:
    typedef struct DatasetListContainer_t
    {
        int id;
        int type;
        QString name;
        QColor color;
        QString colorRgbaFill;
        QString colorRgbaStroke;
        QString dataString;
    }DatasetListContainer;

    int chartType;
    QString chartLabels;
    QString chartValues; // used with pie and doughnut only
    int m_datasetNumber;
    QList<DatasetListContainer> datasetList;
    QString getJsonCompatibleLabel();
    QString getDatasetString();

    QQuickView *view;
    Ui::ChartsWidget *ui;

    void setLegendRound(QColor color, QString label, QString value);
};

#endif // CHARTSWIDGET_H
