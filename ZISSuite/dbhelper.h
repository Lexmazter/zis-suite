#ifndef DBHELPER_H
#define DBHELPER_H

#include <QString>
#include <QMessageBox>
#include <QtSql>
#include <QVariant>
#include "customsqltable.h"

/**
  Login defines
  */
#define LOGIN_SUCCESSFUL 0
#define USERNAME_FAILED 1
#define PASSWORD_FAILED 2

/**
  DB defines
  */
#define DB_NOT_FOUND 3
#define SQLITE_DB_TYPE 4
#define MYSQL_DB_TYPE 5
#define ORACLE_DB_TYPE 6

/**
  Generic defines
  */
#define UNKNOWN_ERROR 255

/**
 * @brief The DbHelper class - Static class used for database manipulation
 */
class DbHelper
{
private:
    static QSqlDatabase db;
public:
    static bool dbOpen(int type);
    static void dbClose();
    static int Login(QString username, QString password);
    static QSqlQueryModel* getModelFromQuery(QObject *parent, QString table);
    static CustomSqlTable *getTableModel(QObject* parent , QString table, ColorDelegate *delegate = nullptr);
    static void deleteFromTable(QString table, QString id);
    static void saveTableModelToDb(QAbstractItemModel *model);
    static QSqlQueryModel *getModelFromQuery(QObject *parent, QString table, QString *selectClause, QString *condition);
    static QString getSelectedDataFromTable(QString table, QString condition);
    static double getTotalBillValueForProducts(QString table, QString ids);
    static bool getSelectedItemsInList(QString table, QString selectClause, QString queryCondition, QStringList *returnedAttributes);
    static void getAvailableQuantityForProductsById(QString table, QString IDs, QMap<int, float> *returnedMap);
    static bool updateQuery(QString table, QString *setClause, QString *queryCondition);
    static bool queryToMap(QString table, QString selectClause, QString queryCondition, QVariantMap *map);
    static bool queryToListOfMaps(QString table, QString selectClause, QString queryCondition, QList<QVariantMap> *listOfMaps);
    static bool updateSQLFromMap(QString table, QVariantMap *map, QString whereClause);
    static bool insertSQLFromMap(QString table, QVariantMap *map);
};

#endif // DBHELPER_H
