#include "numberspeller_helper.h"
#include <QtMath>

NumberSpeller_Helper::NumberSpeller_Helper()
{

}

QString NumberSpeller_Helper::numberToText(uint number)
{
    //Only initialize once
    if (numbers.isEmpty())
    {
        numbers[0] = "zero";
        numbers[1] = "unu";
        numbers[2] = "doi";
        numbers[3] = "trei";
        numbers[4] = "patru";
        numbers[5] = "cinci";
        numbers[6] = "sase";
        numbers[7] = "sapte";
        numbers[8] = "opt";
        numbers[9] = "noua";
        numbers[10] = "zece";
        numbers[11] = "unsprezece";
        numbers[12] = "doisprezece";
        numbers[13] = "treisprezece";
        numbers[14] = "paisprezece";
        numbers[15] = "cinsprezece";
        numbers[16] = "saisprezece";
        numbers[17] = "saptesprezece";
        numbers[18] = "optsprezece";
        numbers[19] = "nouasprezece";
        numbers[20] = "douazeci";
        numbers[30] = "treizeci";
        numbers[40] = "patruzeci";
        numbers[50] = "cinciceci";
        numbers[60] = "saizeci";
        numbers[70] = "saptezeci";
        numbers[80] = "optzeci";
        numbers[90] = "nouazeci";
        numbers[100] = "o suta";
        numbers[1000] = "o mie";
        numbers[1000000] = "un milion";
    }

    //Only initialize once
    if (powers.isEmpty())
    {
        powers[2] = "sute";
        powers[3] = "mii";
        powers[6] = "milioane";
        powers[9] = "bilioane";
    }

    QString output;

    if (number < 21)
    {
        output = numbers[number];
    }
    else if (number <= 100)
    {
        output = numbers[10 * qFloor(number / 10)];
        uint remainder = number % 10;

        if (remainder > 0)
            output += " si " + numberToText(remainder);
    }
    else
    {
        uint power = 2;
        uint place = 0;
        QString powerString;

        //QMap::keys is ordered
        foreach (uint pow, powers.keys())
        {
            uint place_value = qPow(10, pow);
            uint tmp_place = qFloor(number / place_value);
            if (tmp_place < 1)
                break;

            place = tmp_place;
            power = pow;
            powerString = powers[pow];
        }

        if (power > 0)
        {
            output = numberToText(place) + " " + powerString;
            uint remainder = number % uint(qPow(10, power));

            if (remainder > 0)
                output += " " + numberToText(remainder);
        }
    }

    return output;
}
