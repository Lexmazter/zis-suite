#include "cutereport_helper.h"
#include <QStringListModel>


CuteReport_Helper::CuteReport_Helper(QObject *parent, QString reportPath, QString tempPageName)
    : QObject(parent)
{
    cuteReport =  new CuteReport::ReportCore(parent);// load report
    QString err;
    reportObject = cuteReport->loadReport(reportPath, &err);
    this->pageName = tempPageName;
    // if error, exit with message
    if (!reportObject) {
        qDebug() << "loadReport " + err;
        return;
    }
    FirmDataSettings = FirmSettings_Helper::getINstance();
    FirmDataSettings->updateAllFromDB();

}

CuteReport_Helper::~CuteReport_Helper()
{
    if(cuteReport != nullptr)
        delete cuteReport;
    if(preview != nullptr)
        delete preview;

}

void CuteReport_Helper::fillDetailsBandForAbstractTable(QAbstractTableModel *tableModel)
{
    //CuteReport::PageInterface * page = reportObject->page(pageName);
    CuteReport::BaseItemInterface *band = cuteReport->itemByName("soldItems", pageName, reportObject);
    CuteReport::DatasetInterface *data = cuteReport->datasetByName("datasetTblModel", reportObject);
    if(data == nullptr)
    {
        //dataset is not available -> create it
        data = cuteReport->createDatasetObject("Model");
        reportObject->addDataset(data);
        data->setProperty("addressVariable","datasetTblModel");
        band->setProperty("dataset", data->objectName());
        CuteReport::BaseItemInterface *memoDescription = cuteReport->itemByName("memo_Descriere", pageName, reportObject);
        CuteReport::BaseItemInterface *memoAmount = cuteReport->itemByName("memo_Cantitate", pageName, reportObject);
        CuteReport::BaseItemInterface *memoUM = cuteReport->itemByName("memo_UM", pageName, reportObject);
        CuteReport::BaseItemInterface *memoUnitPrice = cuteReport->itemByName("memo_PretUnit", pageName, reportObject);
        CuteReport::BaseItemInterface *memoValue = cuteReport->itemByName("memo_Valoare", pageName, reportObject);
        // get text from named and row number 1 (starting from 1)
        memoDescription->setProperty("text", QString("[%1.\"Denumire\"]").arg(data->objectName()));
        memoAmount->setProperty("text", QString("[%1.\"Cantitate\"]").arg(data->objectName()));
        memoUM->setProperty("text", QString("[%1.\"UM\"]").arg(data->objectName()));
        memoUnitPrice->setProperty("text", QString("[%1.\"Pret\"]").arg(data->objectName()));
        memoValue->setProperty("text", QString("[%1.\"Pret\"*%1.\"Cantitate\"]").arg(data->objectName()));
    }

    // Set model name the same you have set in the ModelDataset before
    reportObject->setVariableValue("datasetTblModel",quint64(tableModel));
    data->populate();
}

void CuteReport_Helper::fillReportDataForBill(int billID, QAbstractTableModel* customTableModel )
{
    //fill the sql datasets in receipt template in order to handle the images by means of cutereport features
    CuteReport::DatasetInterface * dataset_firm = cuteReport->datasetByName("dataset_firm", reportObject);
    CuteReport::DatasetInterface * dataset_thirds = cuteReport->datasetByName("dataset_thirds", reportObject);
    CuteReport::DatasetInterface * dataset_bills = cuteReport->datasetByName("dataset_bills", reportObject);
    CuteReport::DatasetInterface * dataset_bank_accounts = cuteReport->datasetByName("dataset_bank_accounts", reportObject);
    dataset_firm->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);
    dataset_thirds->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);
    dataset_bills->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);
    dataset_bank_accounts->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);


    QString billID_Formatted =  QString("%1").arg(billID, 6, 'g', -1, '0');
    //dummy memo used for data sharing with cutereport scripting
    CuteReport::BaseItemInterface *dummy_bill_id = cuteReport->itemByName("dummy_bill_id", pageName, reportObject);
    if(dummy_bill_id != nullptr )
        dummy_bill_id->setProperty("text", billID_Formatted);

    //show sold items as table on the bill
    this->fillDetailsBandForAbstractTable(customTableModel);

    //show up maximum 4 bank accounts
    QString bankAccounts = FirmDataSettings->getUsedBankAccountsAsString(C_NO_Bank_Accounts_Limit);
    FirmDataSettings->breakLinesAsHTML(&bankAccounts);
    CuteReport::BaseItemInterface *dummy_bank_accounts = cuteReport->itemByName("dummy_bank_accounts", pageName, reportObject);
    if(dummy_bank_accounts != nullptr )
        dummy_bank_accounts->setProperty("text", bankAccounts);
}

void CuteReport_Helper::fillReportDataForProforma(int billID, QAbstractTableModel* customTableModel )
{
    //fill the sql datasets in receipt template in order to handle the images by means of cutereport features
    CuteReport::DatasetInterface * dataset_firm = cuteReport->datasetByName("dataset_firm", reportObject);
    CuteReport::DatasetInterface * dataset_thirds = cuteReport->datasetByName("dataset_thirds", reportObject);
    CuteReport::DatasetInterface * dataset_bills = cuteReport->datasetByName("dataset_bills", reportObject);
    CuteReport::DatasetInterface * dataset_bank_accounts = cuteReport->datasetByName("dataset_bank_accounts", reportObject);
    dataset_firm->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);
    dataset_thirds->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);
    dataset_bills->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);
    dataset_bank_accounts->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);


    QString billID_Formatted = QString("%1").arg(billID, 6, 'g', -1, '0');
    //dummy memo used for data sharing with cutereport scripting
    CuteReport::BaseItemInterface *dummy_bill_id = cuteReport->itemByName("dummy_bill_id", pageName, reportObject);
    if(dummy_bill_id != nullptr )
        dummy_bill_id->setProperty("text", billID_Formatted);

    //show sold items as table on the bill
    this->fillDetailsBandForAbstractTable(customTableModel);

    //show up maximum 4 bank accounts
    QString bankAccounts = FirmDataSettings->getUsedBankAccountsAsString(C_NO_Bank_Accounts_Limit);
    FirmDataSettings->breakLinesAsHTML(&bankAccounts);
    CuteReport::BaseItemInterface *dummy_bank_accounts = cuteReport->itemByName("dummy_bank_accounts", pageName, reportObject);
    if(dummy_bank_accounts != nullptr )
        dummy_bank_accounts->setProperty("text", bankAccounts);
}

void CuteReport_Helper::fillReportDataForReceipt( QVariantMap *dataThirds, QString currentDate, QString date, QString Price,QString spelledNumber, QString receiptID)
{

    //fill the sql dataset in receipt template in order to handle the images by means of cutereport features
    CuteReport::DatasetInterface * dataImage = cuteReport->datasetByName("dataset_firm", reportObject);
    dataImage->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);

    /* uncomment the following section if you want to print the bank accounts on receipt */
    //    CuteReport::BaseItemInterface *memo_accounts = cuteReport->itemByName("memo_accounts", pageName, reportObject);
    //    QString bankAccounts = FirmDataSettings->getUsedBankAccountsAsString(C_NO_Bank_Accounts_Limit);
    //    FirmDataSettings->breakLinesAsHTML(&bankAccounts);
    //    if(memo_accounts != nullptr)
    //        memo_accounts->setProperty("text", bankAccounts);

    //receipt data
    CuteReport::BaseItemInterface *memo_cur_date = cuteReport->itemByName("memo_cur_date", pageName, reportObject);
    if(memo_cur_date != nullptr)
        memo_cur_date->setProperty("text", currentDate);
    CuteReport::BaseItemInterface *memo_date = cuteReport->itemByName("memo_date", pageName, reportObject);
    if(memo_date != nullptr)
        memo_date->setProperty("text", date);
    CuteReport::BaseItemInterface *memo_price = cuteReport->itemByName("memo_price", pageName, reportObject);
    if(memo_price != nullptr)
        memo_price->setProperty("text", Price);
    CuteReport::BaseItemInterface *memo_recepit_id = cuteReport->itemByName("memo_receipt_id", pageName, reportObject);
    if(memo_recepit_id != nullptr)
        memo_recepit_id->setProperty("text", receiptID);
    CuteReport::BaseItemInterface *memo_spell = cuteReport->itemByName("memo_spell", pageName, reportObject);
    if(memo_spell != nullptr)
        memo_spell->setProperty("text", spelledNumber);

    //thirds data
    CuteReport::BaseItemInterface *memo_third_name = cuteReport->itemByName("memo_third_name", pageName, reportObject);
    if(memo_third_name != nullptr)
        memo_third_name->setProperty("text", dataThirds->value("Denumire"));
    CuteReport::BaseItemInterface *memo_third_cf = cuteReport->itemByName("memo_third_cf", pageName, reportObject);
    if(memo_third_cf != nullptr)
        memo_third_cf->setProperty("text", dataThirds->value("CIF"));
    CuteReport::BaseItemInterface *memo_third_regcom = cuteReport->itemByName("memo_third_regcom", pageName, reportObject);
    if(memo_third_regcom != nullptr)
        memo_third_regcom->setProperty("text", dataThirds->value("RegComert"));
    CuteReport::BaseItemInterface *memo_third_adr = cuteReport->itemByName("memo_third_adr", pageName, reportObject);
    if(memo_third_adr != nullptr)
        memo_third_adr->setProperty("text", dataThirds->value("Adresa"));
}

/* for testing purpose - while trying to add multiple pages in the same preview - not working as expecting*/
//void CuteReport_Helper::doubleReceipt(void)
//{
//    CuteReport::ReportCore *cuteReport2 =  new CuteReport::ReportCore();// load report
//    QString err;
//    CuteReport::ReportInterface *reportObject2 = nullptr;
//    for(int i=0; i<1; i++){
//        reportObject2 = cuteReport2->loadReport("../../BillsTemplates/Chitanta_Template_Duplicate.qtrp", &err);
//        QString pageName1 ="Chitanta_Template_Duplicate";
//        //fill the sql dataset in receipt template in order to handle the images by means of cutereport features
//        CuteReport::DatasetInterface * dataImage = cuteReport2->datasetByName("dataset_firm", reportObject2);
//        dataImage->setProperty("dbname",FirmSettings_Helper::DB_Application_Path);

//        //receipt data
//        CuteReport::BaseItemInterface *memo_cur_date = cuteReport2->itemByName("memo_cur_date", pageName1, reportObject2);
//        if(memo_cur_date != nullptr)
//            memo_cur_date->setProperty("text", "test");
//        CuteReport::BaseItemInterface *memo_date = cuteReport2->itemByName("memo_date", pageName1, reportObject2);
//        if(memo_date != nullptr)
//            memo_date->setProperty("text", "test");
//        CuteReport::BaseItemInterface *memo_price = cuteReport2->itemByName("memo_price", pageName1, reportObject2);
//        if(memo_price != nullptr)
//            memo_price->setProperty("text", "test");
//        CuteReport::BaseItemInterface *memo_recepit_id = cuteReport2->itemByName("memo_receipt_id", pageName1, reportObject2);
//        if(memo_recepit_id != nullptr)
//            memo_recepit_id->setProperty("text", "test");

//        CuteReport::PageInterface *page1 = reportObject2->page(pageName1);
//        reportObject->addPage(page1);
//    }
//    delete cuteReport2;
//}

void CuteReport_Helper::fillNamedMemo(QString momoName, QString value)
{
    CuteReport::BaseItemInterface *memo = cuteReport->itemByName(momoName, pageName, reportObject);
    if(memo != nullptr)
        memo->setProperty("text", value);
}

void CuteReport_Helper::fillDetailsBandForList(QStringList list)
{
    CuteReport::PageInterface * page = reportObject->page("Page");
    CuteReport::BaseItemInterface * band = cuteReport->itemByName("detail", "Page", reportObject);
    CuteReport::BaseItemInterface * memo = cuteReport->itemByName("memo", "Page", reportObject);
    CuteReport::DatasetInterface * data = cuteReport->createDatasetObject("Model");
    reportObject->addDataset(data);
    data->setProperty("addressVariable","Model1");
    band->setProperty("dataset", data->objectName());

    // get text from named and column number 1 (starting from 1)
    memo->setProperty("text", QString("[%1.\"1\"]").arg(data->objectName()));
    //     making of the test model
    QStringListModel *model = new QStringListModel();
    model->setStringList(list);// Warning!!! Link to model passed as  quint64
    // Set model name the same you have set in the ModelDataset before
    reportObject->setVariableValue("Model1",quint64(model));
    delete model;
}

void CuteReport_Helper::renderRaportPreview(QWidget* parent, QString previewName)
{
    // making report preview window
    preview = new CuteReport::ReportPreview(cuteReport);
    preview->setWindowIcon(parent->windowIcon());
    preview->setWindowTitle(previewName);

    if (reportObject) {
        // set core and set preloaded report
        preview->setReportCore(cuteReport);
        preview->connectReport(reportObject);

        //Preview window show
        preview->showMaximized();

        // report processing
        preview->run();
    }
}
