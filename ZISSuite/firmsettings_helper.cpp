#include "firmsettings_helper.h"
#include "Dialogs/firmdetails_dialog.h"

FirmSettings_Helper* FirmSettings_Helper::instance=0;
QList<QVariantMap> FirmSettings_Helper::accountsData;
QVariantMap FirmSettings_Helper::firmData;
QString FirmSettings_Helper::DB_Application_Path;

FirmSettings_Helper::FirmSettings_Helper()
{

}

void FirmSettings_Helper::updateAllFromDB()
{
    updateFirmDataFromDB();
    updateAccountsDataFromDB();
}

QVariantMap* FirmSettings_Helper::getFirmData()
{
    return &firmData;
}

QList<QVariantMap>* FirmSettings_Helper::getAccountsData()
{
    return &accountsData;
}

/*singletone pattern for static instance */
FirmSettings_Helper* FirmSettings_Helper::getINstance()
{
    if(!instance)
    {
        instance = new FirmSettings_Helper();
    }
    return instance;
}

bool FirmSettings_Helper::updateFirmDataFromDB()
{
    QString conditionalQuery = "ID = " + FirmDetails_Dialog::FirmID;
    bool retVal = DbHelper::queryToMap("DateFirma", "*", conditionalQuery, &firmData);
    return retVal;
}

bool FirmSettings_Helper::updateAccountsDataFromDB()
{
    QString conditionalQuery = "Folosit = 1";
    bool retVal = DbHelper::queryToListOfMaps("ConturiBancare", "*", conditionalQuery, &accountsData);
    return retVal;
}

QVariant FirmSettings_Helper::getFirmItemByName(QString name)
{
    QVariant retVal = "";
    if(firmData.contains(name))
        retVal = firmData.value(name);
    return retVal;
}

QString FirmSettings_Helper::getAllBankAccountsAsString()
{
    QString retVal = "";
    for(int i=0; i<accountsData.count(); i++)
    {
        QVariantMap map = accountsData.at(i);
        QVariantMap::iterator iter;
        for( iter = map.begin(); iter != map.end(); ++iter) {
            if((!iter.key().contains("ID")) && (!iter.key().contains("Folosit")))
            {
                retVal += iter.key() + ": ";
                retVal += iter.value().toString() + "  ";
            }
        }
        retVal += "\n";
    }
    return retVal;
}

QString FirmSettings_Helper::getUsedBankAccountsAsString(int maxNOAccounts)
{
    int temp_counter=0;
    QString retVal = "";
    for(int i=0; i<accountsData.count(); i++)
    {
        if(accountsData.at(i).value("Folosit") == 1){
            QVariantMap map = accountsData.at(i);
            QVariantMap::iterator iter;
            for( iter = map.begin(); iter != map.end(); ++iter) {
                if((!iter.key().contains("ID")) && (!iter.key().contains("Folosit")))
                {
                    retVal += iter.key() + ": ";
                    retVal += iter.value().toString() + "  ";
                    temp_counter++;
                }
            }
            //if there is a limit of desired no accounts and we have more active then respect that
            if((maxNOAccounts>0) && (temp_counter >= maxNOAccounts))
                return retVal;
            else
                retVal += "\n";
        }
    }
    return retVal;
}

void FirmSettings_Helper::breakLinesAsHTML(QString *string)
{
    if((!string->isNull()) && (!string->isEmpty()))
        string->replace(QString("\n"), QString("<br>"));
}
