#ifndef SELECT_MODEL_DELEGATE_H
#define SELECT_MODEL_DELEGATE_H
#include <QStyledItemDelegate>
#include <QPainter>
#include <QMap>


class SelectModelDelegate:public QStyledItemDelegate
{
    Q_OBJECT
public:
    SelectModelDelegate(QWidget *parent);
    //    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

//    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

//    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    QWidget *p_WidgetParent = nullptr;
    //    QMap<int, int> *p_ModelsMap;

};

#endif // SELECT_MODEL_DELEGATE_H


