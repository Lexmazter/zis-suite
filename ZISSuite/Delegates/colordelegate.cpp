#include "colordelegate.h"
#include <QApplication>
#include <QDesktopWidget>

ColorDelegate::ColorDelegate(QObject *parent) : QStyledItemDelegate(parent) {}

QList <QString> ColorDelegate::modifiedRowsId()
{ return m_modifiedRowsId; }

QList <QString> ColorDelegate::deletedRowsId()
{ return m_deletedRowsId; }

void ColorDelegate::clearColoredRows()
{
    m_deletedRowsId.clear();
    m_modifiedRowsId.clear();
}

void ColorDelegate::addDeletedRow(QString rowId)
{
    // We don't change the color of the new rows
    if(rowId != "0")
        m_deletedRowsId.append(rowId);
}

void ColorDelegate::addModifiedRow(QString rowId)
{
    // We don't change the color of the new rows
    if(rowId != "0")
        m_modifiedRowsId.append(rowId);
}

void ColorDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    // Use as column delegate from now on
    if(m_deletedRowsId.contains(index.data().toString())) /* for delete */
    {
        // Delete color - Red
        QPixmap pixmap1(":/Icons/Delete-48.png");
        pixmap1 = pixmap1.scaled(16,16,Qt::IgnoreAspectRatio, Qt::FastTransformation);

        painter->fillRect(option.rect, QColor(255,204,204));

        if(index.column() == 0)
        {
            painter->drawPixmap(option.rect.center() - pixmap1.rect().center(), pixmap1);
        }

        // Paint only the index column, to avoid removing the column editors
        QStyledItemDelegate::paint(painter, option, index);
    }
    else if(index.data().toString() == "0") /* for add */
    {
        // Added color - Green
        QPixmap pixmap2(":/Icons/Add Row-48.png");
        pixmap2 = pixmap2.scaled(16,16,Qt::IgnoreAspectRatio, Qt::FastTransformation);

        painter->fillRect(option.rect, QColor(204,255,204));
        if(index.column() == 0)
        {
            painter->drawPixmap(option.rect.center() - pixmap2.rect().center(), pixmap2);
        }

        QStyledItemDelegate::paint(painter, option, index);
    }
    else if(m_modifiedRowsId.contains(index.data().toString())) /* for modified */
    {
        // Modified color - Yellow
        QPixmap pixmap3(":/Icons/Edit-48.png");
        pixmap3 = pixmap3.scaled(16,16,Qt::IgnoreAspectRatio, Qt::FastTransformation);

        painter->fillRect(option.rect, QColor(255,255,102));

        if(index.column() == 0)
        {
            painter->drawPixmap(option.rect.center() - pixmap3.rect().center(), pixmap3);
        }

        QStyledItemDelegate::paint(painter, option, index);
    }
    else
    {
        QStyledItemDelegate::paint(painter, option, index);
    }
}

void ColorDelegate::drawBackground(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(index);
}
