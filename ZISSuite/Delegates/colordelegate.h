#ifndef COLORDELEGATE_H
#define COLORDELEGATE_H

#include <QStyledItemDelegate>
#include <QPainter>

class ColorDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    ColorDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void addDeletedRow(QString rowId);
    void addModifiedRow(QString rowId);
    void clearColoredRows();
    QList <QString> modifiedRowsId();
    QList <QString> deletedRowsId();

protected:
    void drawBackground(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    QList <QString> m_deletedRowsId;
    QList <QString> m_modifiedRowsId;
};

#endif // COLORDELEGATE_H
