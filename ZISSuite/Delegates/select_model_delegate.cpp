#include "select_model_delegate.h"
#include "Dialogs/billmodel_dialog.h"
#include <QPushButton>
#include <QStylePainter>
#include<QApplication>



/**
 * @abstract: SelectModelDelegate::SelectModelDelegate(QObject *parent)
 * @brief: Select Bill Model Delegate
 */
SelectModelDelegate::SelectModelDelegate(QWidget *parent):QStyledItemDelegate(parent)
{
    p_WidgetParent = parent;
}

//void SelectModelDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
//{
//    QStyleOptionButton opt;
//    int s = index.data(Qt::UserRole).toInt();
////    if (s == Hovered)
////    opt.state |= QStyle::State_MouseOver;
////    if (s == Pressed)
////    opt.state |= QStyle::State_Sunken;
//    opt.state |= QStyle::State_Enabled;
//    opt.rect = option.rect.adjusted(1, 1, -1, -1);
//    opt.text = trUtf8("Button text");
//    QApplication::style()->drawControl(QStyle::CE_PushButton, &opt, painter, 0);
////    QApplication::setStyleSheet("QPushButton {margin-left: 10%; margin-right: 10%;}");

//}

QWidget *SelectModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    //    QIcon bill_icon(":/Icons/Bill-48.png");
    //    QPushButton *editor = new QPushButton(bill_icon,"Button", parent);
    //    editor->setStyleSheet("QPushButton {margin-left: 10%; margin-right: 10%;}");
    QPushButton *editor = new QPushButton(parent);
    int value = index.data().toInt();
    BillModel_Dialog *p_BillModelDialog = new BillModel_Dialog(p_WidgetParent, value);
    p_BillModelDialog->exec();
    editor->setText(QString::number(p_BillModelDialog->getBillModel_NR()));
    return editor;
}

//void SelectModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
//{
//    int value = index.model()->data(index, Qt::EditRole).toInt();
//    QPushButton *pushButton = static_cast<QPushButton*>(editor);
//    pushButton->setText(QString::number(value));

//}

void SelectModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QPushButton *pushButton = static_cast<QPushButton*>(editor);
    int value = pushButton->text().toInt();
    model->setData(index, value, Qt::EditRole);
    //    emit(model->dataChanged(index, index));
}

//void SelectModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
//{
//        editor->setGeometry(option.rect);
//}
