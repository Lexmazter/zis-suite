#include "passworddelegate.h"
#include <QPainter>

PasswordDelegate::PasswordDelegate(QObject *parent) : QItemDelegate(parent)
{

}

QWidget *PasswordDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const
{
    QLineEdit* editor = new QLineEdit(parent);
    editor->setInputMethodHints(Qt::ImhHiddenText| Qt::ImhNoPredictiveText | Qt::ImhNoAutoUppercase | Qt::ImhSensitiveData);

    return editor;
}

void PasswordDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLineEdit *textEdit = static_cast<QLineEdit*>(editor);
    textEdit->setEchoMode(QLineEdit::Password);
    textEdit->setInputMethodHints(Qt::ImhHiddenText| Qt::ImhNoPredictiveText | Qt::ImhNoAutoUppercase | Qt::ImhSensitiveData);
    model->setData(index, textEdit->text(), Qt::EditRole);
}

void PasswordDelegate::setEditorData(QWidget *editor,
                                     const QModelIndex &index) const
{
    QString value = index.model()->data(index, Qt::EditRole).toString();

    QLineEdit *textEdit = static_cast<QLineEdit*>(editor);
    textEdit->setEchoMode(QLineEdit::Password);
    textEdit->setInputMethodHints(Qt::ImhHiddenText| Qt::ImhNoPredictiveText | Qt::ImhNoAutoUppercase | Qt::ImhSensitiveData);
    textEdit->setText(value);
}

void PasswordDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}

void PasswordDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    if (option.state & QStyle::State_Selected)
                painter->fillRect(option.rect, option.palette.highlight());

    // draw the background color
    if (option.showDecorationSelected && (option.state & QStyle::State_Selected)) {
        QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
                                  ? QPalette::Normal : QPalette::Disabled;
        painter->fillRect(option.rect, option.palette.brush(cg, QPalette::Highlight));
    } else {
        QVariant value = index.data(Qt::BackgroundColorRole);
        if (value.isValid() && qvariant_cast<QColor>(value).isValid())
            painter->fillRect(option.rect, qvariant_cast<QColor>(value));
    }

    painter->setPen(QColor(Qt::gray));

    /* fill the data with ●●●● - hex code 0x25CF in Unicode-16bit */
    QString tempString = index.data().toString();
    tempString.fill(0x25CF);

    painter->drawText(option.rect, Qt::AlignVCenter, " "+tempString);
}
