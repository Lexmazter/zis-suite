#include "comboboxdelegate.h"
#include "QPainter"

ComboBoxDelegate::ComboBoxDelegate(QObject *parent, QMap<QString, QString> *itemsList)
    :QItemDelegate(parent)
{
    foreach (QString value, itemsList->values())
    {
        Items.insert(itemsList->key(value),value);
    }
}


QWidget *ComboBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const
{
    QComboBox* editor = new QComboBox(parent);

    editor->addItems(Items.values());

    return editor;
}

void ComboBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    QString currentIndex = index.data(Qt::EditRole).toString().toLower();

    if(currentIndex.isEmpty())
    {
        currentIndex = Items.begin().key();
    }

    comboBox->setCurrentIndex(indexOf(currentIndex));
}

void ComboBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    model->setData(index, itemAtPosition(comboBox->currentText()), Qt::EditRole);
}

void ComboBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}

void ComboBoxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QString currentIndex = index.data(Qt::EditRole).toString().toLower();
    QStyleOptionViewItem temp_option = option;
    temp_option.text = Items.value(currentIndex);

    // draw the background color
    if (option.showDecorationSelected && (option.state & QStyle::State_Selected)) {
        QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
                ? QPalette::Normal : QPalette::Disabled;
        painter->fillRect(option.rect, option.palette.brush(cg, QPalette::Highlight));
        painter->setPen(Qt::white);
    } else {
        QVariant value = index.data(Qt::BackgroundColorRole);
        if (value.isValid() && qvariant_cast<QColor>(value).isValid())
            painter->fillRect(option.rect, qvariant_cast<QColor>(value));
        painter->setPen(Qt::black);
    }

    painter->drawText(option.rect, Qt::AlignVCenter, " "+temp_option.text);
}

int ComboBoxDelegate::indexOf(QString value) const
{
    int index = std::distance(Items.begin(), Items.find(value));

    return index;
}

QString ComboBoxDelegate::itemAtPosition(QString text) const
{
    QString returnDefault;
    QString tempString = Items.key(text);

    if(!tempString.isEmpty())
    {
        return tempString;
    }

    returnDefault = Items.begin().key();
    return returnDefault;
}
