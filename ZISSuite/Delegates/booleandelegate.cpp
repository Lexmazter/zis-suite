#include "booleandelegate.h"

BooleanDelegate::BooleanDelegate(QObject *parent) : QItemDelegate(parent)
{

}

void BooleanDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    drawBackground(painter, option, index);
    drawCheck(painter, option, option.rect, index.data().toBool() ? Qt::Checked : Qt::Unchecked);
    drawFocus(painter, option, option.rect);
}

QWidget *BooleanDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);

    QCheckBox *editor = new QCheckBox(parent);
    editor->setStyleSheet("QCheckBox {margin-left: 43%; margin-right: 57%;}");

    return editor;
}

void BooleanDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QCheckBox * widget = qobject_cast<QCheckBox*>(editor);

    widget->setChecked(index.data().toBool());
}

void BooleanDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QCheckBox * widget = qobject_cast<QCheckBox*>(editor);

    model->setData(index, widget->checkState(), Qt::EditRole);
}
