#include "spinboxdelegate.h"

/**
 * @abstract: SpinBoxDelegate::SpinBoxDelegate(QObject *parent)
 * @brief: SpinBox delegate
 */
SpinBoxDelegate::SpinBoxDelegate(QObject *parent):QStyledItemDelegate(parent)
{
    this->rangeContraintFlag = false;
    this->showToolTipFlag = false;
}

QWidget *SpinBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QSpinBox *editor = new QSpinBox(parent);
    return editor;
}

//void SpinBoxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
//{
//    QStyleOptionViewItem op(option);

//        op.font.setBold(true);
//        op.palette.setColor(QPalette::Normal, QPalette::Background, Qt::red);
//        op.palette.setColor(QPalette::Normal, QPalette::Foreground, Qt::white);
//    return QStyledItemDelegate::paint(painter, op, index);
//}

void SpinBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    float value = index.model()->data(index, Qt::EditRole).toFloat();
    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
    if(rangeContraintFlag == true){
        spinBox->setMinimum(0);
        if(!productsAmountLimit->isEmpty())
        {
            float temp_max = productsAmountLimit->values().at(index.row());
            spinBox->setMaximum(temp_max);
            if(showToolTipFlag){
                spinBox->setToolTip("Cantitate disponibila: "+QString::number(temp_max));
            }
        }
    }
    spinBox->setValue(value);

}

void SpinBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
    spinBox->interpretText();
    float value = spinBox->value();
    model->setData(index, value, Qt::EditRole);
    emit(model->dataChanged(index, index));
}

void SpinBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}


void SpinBoxDelegate::setEditorRange(QMap<int, float> *AmountLimits){
    productsAmountLimit = AmountLimits;
    rangeContraintFlag = true;
}

void SpinBoxDelegate::setShowToolTipFlag(bool value)
{
    showToolTipFlag = value;
}
