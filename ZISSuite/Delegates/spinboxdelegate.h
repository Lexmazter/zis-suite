#ifndef DELEGATE_H
#define DELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
#include <QObject>
#include <QSize>
#include <QSpinBox>
#include <QPainter>
#include <QColor>

class SpinBoxDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:

    explicit SpinBoxDelegate(QObject *parent=0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;


    void setEditorRange(QMap<int, float> *AmountLimits);

//    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    void setShowToolTipFlag(bool value);

signals:

public slots:
private:
    bool rangeContraintFlag;
    bool showToolTipFlag;
    QMap<int, float> *productsAmountLimit;

};

#endif // DELEGATE_H
