#ifndef COMBOBOXDELEGATE_H
#define COMBOBOXDELEGATE_H

#include <QItemDelegate>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>
#include <QCompleter>

class QModelIndex;
class QWidget;
class QVariant;

class LineEditCompleterDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    LineEditCompleterDelegate(QObject *parent = 0,  QCompleter *completer = nullptr);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    //  void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    int model_value;
    QCompleter *c = 0;

};
#endif
