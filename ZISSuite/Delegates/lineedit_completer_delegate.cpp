#include "lineedit_completer_delegate.h"
#include "completer_lineedit.h"
#include "dbhelper.h"
#include "QLineEdit"

LineEditCompleterDelegate::LineEditCompleterDelegate(QObject *parent, QCompleter *completer)
    :QItemDelegate(parent)
{
    if(c != nullptr)
        delete c;

    c = completer;
}


QWidget *LineEditCompleterDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex & index) const
{
    CompleterLineEdit* editor = new CompleterLineEdit(parent);
    editor->setCompleter(c);
    editor->setText(index.data().toString());
    return editor;
}

void LineEditCompleterDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    Q_UNUSED(index);
}

void LineEditCompleterDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    CompleterLineEdit *textEdit = static_cast<CompleterLineEdit*>(editor);
    model->setData(index, textEdit->text(), Qt::EditRole);
    //TODO: the next implementation should be done in a dedicated method/class in order to keep the LineEditCompleterDelegate class abstract, independet of the table
    QString itemDescription;
    QStringList productAttributes;
    itemDescription = textEdit->text();
    QString selectClause = "ID, UM, Pret";
    QString conditionForQuery = "Denumire LIKE '" + itemDescription + "'";
    if(DbHelper::getSelectedItemsInList("Nomenclator", selectClause, conditionForQuery, &productAttributes))
    {
        if(productAttributes.length() == 3){
            int rowNO = index.row();
            QModelIndex tempIndex = model->index(rowNO, 0);
            model->setData(tempIndex, productAttributes.value(0), Qt::EditRole);
            tempIndex = model->index(rowNO, 2);
            model->setData(tempIndex, productAttributes.value(1), Qt::EditRole);
            tempIndex = model->index(rowNO, 3);
            model->setData(tempIndex, productAttributes.value(2), Qt::EditRole);
            //set a default value for the quantity column
            tempIndex = model->index(rowNO, 4);
            model->setData(tempIndex, 1, Qt::EditRole);
        }

    }
}

void LineEditCompleterDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}

//void LineEditCompleterDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
//{
//    int currentIndex = index.data(Qt::EditRole).toInt();
//    QStyleOptionViewItem temp_option = option;
//    temp_option.text = Items[currentIndex].c_str();

//    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &temp_option, painter);
//}
