#ifndef BOOLEANDELEGATE_H
#define BOOLEANDELEGATE_H

#include <QItemDelegate>
#include <QPainter>
#include <QCheckBox>

class BooleanDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    BooleanDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
};

#endif // BOOLEANDELEGATE_H
