#ifndef FIRMSETTINGS_HELPER_H
#define FIRMSETTINGS_HELPER_H
#include <QVariantMap>
#include <QList>
#include "customsqltable.h"

class FirmSettings_Helper
{
public:
    // another type of singletone
    //    static FirmSettings_Helper &GetInstance(){
    //       static FirmSettings_Helper instance;
    //       return instance;
    //    }
    static FirmSettings_Helper* getINstance();
    static void updateAllFromDB();
    static bool updateAccountsDataFromDB();
    static bool updateFirmDataFromDB();
    static QList<QVariantMap>* getAccountsData();
    static QVariantMap* getFirmData();
    static QString getAllBankAccountsAsString();
    static QString getUsedBankAccountsAsString(int maxNOAccounts);
    static QVariant getFirmItemByName(QString name);
    static void breakLinesAsHTML(QString *string);

    static QString DB_Application_Path;

private:
    FirmSettings_Helper();
    static FirmSettings_Helper *instance;
    static QList<QVariantMap> accountsData;
    static QVariantMap firmData;
};

#endif // FIRMSETTINGS_HELPER_H
