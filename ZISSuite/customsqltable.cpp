#include "customsqltable.h"
#include <QColor>
#include <QBrush>

CustomSqlTable::CustomSqlTable(QObject * parent = 0, QSqlDatabase db = QSqlDatabase(), ColorDelegate *delegate)
    : QSqlRelationalTableModel(parent, db)
{
    if(delegate)
    {
        colorDelegate = delegate;
    }
}

CustomSqlTable::~CustomSqlTable()
{

}

QVariant CustomSqlTable::data(const QModelIndex &index, int role) const
{
    if ( !index.isValid() )
        return QVariant();

    switch ( role )
    {
    case Qt::BackgroundRole:
    {
        if(colorDelegate != nullptr)
        {
            int row = index.row();
            int col = index.column();


            if(col != 0) // do not overwrite the color delate
            {
                QString tempData = index.sibling(row, 0).data().toString();

                if( tempData == "0" ){
                    // background for this row, green
                    return QVariant(QBrush (QColor(204,255,204)));
                }
                else if (colorDelegate->deletedRowsId().contains(tempData))
                {
                    // background for this row, red
                    return QVariant(QBrush (QColor(255,204,204)));
                }
                else if(colorDelegate->modifiedRowsId().contains(tempData))
                {
                    // background for this row, yellow
                    return QVariant(QBrush (QColor(255,255,102)));
                }
            }
        }

        return QSqlRelationalTableModel::data(index, role);
    }

    default:
    {
        return QSqlRelationalTableModel::data(index, role);
    }
    }

}
