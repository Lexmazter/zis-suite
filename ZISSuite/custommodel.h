#ifndef MYMODEL_H
#define MYMODEL_H

// custommodel.h
#include <QAbstractTableModel>
#include <QString>
#include <QAbstractItemModel>


class CustomModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    CustomModel(QObject *parent, QAbstractItemModel* defaultTable);
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex & index) const Q_DECL_OVERRIDE ;
    //    bool insertRow(const QModelIndex & index, const QVariant & value, int role);
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex(), QStringList *columnData = nullptr);
    char insertAdditionalColumn(QString columnName, QString *columnData);
    bool setHeaderData(int section, Qt::Orientation orientation, QString *value, int role = Qt::EditRole);
    bool removeRow(int row, const QModelIndex &parent = QModelIndex());
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    enum Errors {
        ConditionNotOk = 0,
        DifferentSize = 1,
        OperationNotPossible = 2
    };

private:
    QVector<QVector<QString>>  m_gridData;  //holds text entered into QTableView
    int COLS= 0;
    int ROWS= 0;
    QStringList headerList;
signals:
    void editCompleted(const QString &);
};
//! [Quoting ModelView Tutorial]

#endif // MYMODEL_H
