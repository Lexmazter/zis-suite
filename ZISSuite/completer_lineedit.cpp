#include "completer_lineedit.h"
#include <QCompleter>
#include <QKeyEvent>
#include <QAbstractItemView>
#include <QtDebug>
#include <QApplication>

//! [0]
CompleterLineEdit::CompleterLineEdit(QWidget *parent)
    : QLineEdit(parent), c(0)
{
    setPlaceholderText(tr(" Write text or chose from all items by using ") +
                       QKeySequence("Ctrl+E").toString(QKeySequence::NativeText));
}
//! [0]

//! [1]
CompleterLineEdit::~CompleterLineEdit()
{
}
//! [1]

//! [2]
void CompleterLineEdit::setCompleter(QCompleter *completer)
{
    if (c)
        delete c;

    c = completer;
    c->setFilterMode(Qt::MatchContains);

    if (!c)
        return;

    QLineEdit::setCompleter(c);
}
//! [2]

//! [3]
QCompleter *CompleterLineEdit::completer() const
{
    return c;
}
//! [3]

////! [4]
//void CompleterLineEdit::insertCompletion(const QString& completion)
//{
//    if (c->widget() != this)
//        return;
//    QTextCursor tc = textCursor();
//    int extra = completion.length() - c->completionPrefix().length();
//    tc.movePosition(QTextCursor::Left);
//    tc.movePosition(QTextCursor::EndOfWord);
//    tc.insertText(completion.right(extra));
//    setTextCursor(tc);
//}
////! [4]

//! [5]
void CompleterLineEdit::focusInEvent(QFocusEvent *e)
{
    //    if (c)
    //        c->setWidget(this);
    QLineEdit::focusInEvent(e);
}
//! [5]

//! [6]
void CompleterLineEdit::keyPressEvent(QKeyEvent *e)
{
    if (c && c->popup()->isVisible()) {
        // The following keys are forwarded by the completer to the widget
        switch (e->key()) {
        case Qt::Key_Enter:
        case Qt::Key_Return:
        case Qt::Key_Escape:
        case Qt::Key_Tab:
        case Qt::Key_Backtab:
            e->ignore();
            return; // let the completer do default behavior
        default:
            break;
        }
    }

    bool isShortcut = ((e->modifiers() & Qt::ControlModifier) && e->key() == Qt::Key_E); // CTRL+E
    if (!c || !isShortcut) // do not process the shortcut when we have a completer
        QLineEdit::keyPressEvent(e);

    const bool ctrlOrShift = e->modifiers() & (Qt::ControlModifier | Qt::ShiftModifier);
    if (!c || (ctrlOrShift && e->text().isEmpty()))
        return;

    if (isShortcut) {
        c->complete(); // popup it up!
    }
}
//! [6]

//! [7]
void CompleterLineEdit::mousePressEvent(QMouseEvent *e){
    setPlaceholderText(tr(""));
}
//! [7]

