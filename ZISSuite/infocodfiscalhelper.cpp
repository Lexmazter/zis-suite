#include "infocodfiscalhelper.h"

InfoCodFiscalHelper::InfoCodFiscalHelper(QWidget *parent, QString codFiscal)
    :QWidget(parent)
{
    m_parent = parent;

    tempPage = new QWebEnginePage;

    // Use this function to safely delete on exit
    tempPage->setParent(parent);

    connect(tempPage, SIGNAL(loadFinished(bool)),
            this, SLOT(pageLoadFinished(bool)));

    linkMfinante = "http://mfinante.ro/infocodfiscal.html";

    linkMfinante.setQuery("cod="+codFiscal);

    tempPage->load(linkMfinante);

    // Set the wait cursor while we get the info
    QApplication::setOverrideCursor(Qt::WaitCursor);
}

InfoCodFiscalHelper::~InfoCodFiscalHelper()
{

}

QHash<QString, QString> InfoCodFiscalHelper::getInfoFirma()
{
    return infoFirma;
}

void InfoCodFiscalHelper::pageLoadFinished(bool state)
{
    switch(state)
    {
    case true:
    {
        QString javaScriptCheckForError = "document.getElementById(\"con\").innerText";

        QString javaScriptParseTable = "var c = document.getElementsByTagName(\"tbody\")[4];"
                                       "var table = c.rows;"
                                       "var text = \"\";"
                                       "for (i=0; i<table.length; i++)"
                                       "{"
                                       " text += table[i].cells[0].textContent + \",\" + table[i].cells[1].textContent + \",\";  "
                                       "}"
                                       "";

        tempPage->runJavaScript(javaScriptCheckForError, [this] (const QVariant &result)
        {
            qDebug() << result;

            if(result != QVariant::Invalid)
            {
                tempPage->blockSignals(true);
                QString message = result.toString().trimmed().remove("Inapoi").remove("\n");

                // Prepare a messagebox
                QMessageBox msgBox(m_parent);
                QPixmap icon(":/Icons/Error-48.png");
                msgBox.setIconPixmap(icon);
                msgBox.setText(tr("Nu se pot prelua datele!"));
                msgBox.setInformativeText(tr("Următoarea eroare a fost returnată: \n") + message);
                msgBox.setStandardButtons(QMessageBox::Ok);
                msgBox.setDefaultButton(QMessageBox::Ok);
                msgBox.exec();

                emit loadFinished(false);

                // Restore the cursor after parsing the data
                QApplication::restoreOverrideCursor();

                return;
            }
        });

        tempPage->runJavaScript(javaScriptParseTable, [this] (const QVariant &result)
        {
            qDebug() << result;

            if(result != QVariant::Invalid)
            {
                // JavaScript callback using Lambda
                QString str = result.toString().remove("\n").remove("\t").trimmed();
                QStringList list = str.split(',');
                loadStringToListWidget(list);

                emit loadFinished(true);

                // Restore the cursor after parsing the data
                QApplication::restoreOverrideCursor();
            }
        });

        break;
    }

    case false:
        // Prepare a messagebox
        QMessageBox msgBox(m_parent);
        QPixmap icon(":/Icons/Disconnected-48.png");
        msgBox.setIconPixmap(icon);
        msgBox.setText(tr("Nu se poate incărca pagina!"));
        msgBox.setInformativeText(tr("Înainte de a încerca din nou, vă rugăm să verificați conexiunea la internet."));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();

        emit loadFinished(false);

        // Restore the cursor after parsing the data
        QApplication::restoreOverrideCursor();
        break;
    }
}

void InfoCodFiscalHelper::loadStringToListWidget(QStringList list)
{
    for(int i = 0; i < list.count(); i+=2)
    {
        // prevent index out of bounds error
        if(i+1 < list.count())
        {
            infoFirma.insert(list[i].trimmed().toUtf8(), list[i+1].trimmed().toUtf8());
            qDebug() << "list[i] = " << list[i].trimmed() << "list[i+1] = " << list[i+1].trimmed();
        }
    }
}
