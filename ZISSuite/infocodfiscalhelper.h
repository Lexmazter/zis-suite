#ifndef INFOCODFISCALHELPER_H
#define INFOCODFISCALHELPER_H

#include <QWidget>
#include <QtWebEngineWidgets>
#include <QMovie>
#include <QUrl>

class InfoCodFiscalHelper: public QWidget
{
    Q_OBJECT

signals:
    void loadFinished(bool state);

public:
    explicit InfoCodFiscalHelper(QWidget *parent = 0, QString codFiscal = "0");
    ~InfoCodFiscalHelper();
    QHash <QString, QString> getInfoFirma();

private slots:
    void pageLoadFinished(bool);

private:
    QWidget *m_parent;
    QWebEnginePage* tempPage;
    QUrl linkMfinante;
    QHash <QString, QString> infoFirma;
    bool noError = false;
    void loadStringToListWidget(QStringList list);
};

#endif // INFOCODFISCALHELPER_H
