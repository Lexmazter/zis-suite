#-------------------------------------------------
#
# Project created by QtCreator 2016-10-20T18:50:15
#
#-------------------------------------------------
INCLUDEPATH +=$$PWD/../CuteReport_Library/development/include/cutereport
LIBS += -L$$PWD/../CuteReport_Library
win32: CONFIG(debug, debug|release): LIBS += -lCuteReportCored -lCuteReportWidgetsd
else: LIBS += -lCuteReportCore -lCuteReportWidgets

include($$PWD/../qtxlsx/src/xlsx/qtxlsx.pri)

QT  += core gui sql quickwidgets
QT += webengine
QT  += printsupport webenginewidgets
QT  += axcontainer qml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ZISSuite
TEMPLATE = app

SOURCES += main.cpp\
    mainwindow.cpp \
    dbhelper.cpp \
    filterheaderview.cpp \
    filterlineedit.cpp \
    custommodel.cpp \
    completer_lineedit.cpp \
    codbarehelper.cpp \
    cutereport_helper.cpp \
    customsqltable.cpp \
    chartswidget.cpp \
    textformat_helper.cpp \
    chartswidget.cpp \
    firmsettings_helper.cpp \
    infocodfiscalhelper.cpp \
    Delegates/booleandelegate.cpp \
    Delegates/colordelegate.cpp \
    Delegates/comboboxdelegate.cpp \
    Delegates/lineedit_completer_delegate.cpp \
    Delegates/passworddelegate.cpp \
    Delegates/spinboxdelegate.cpp \
    Dialogs/about_dialog.cpp \
    Dialogs/bonuriproduse_dialog.cpp \
    Dialogs/facturiproduse_dialog.cpp \
    Dialogs/receiveditemsdetails_dialog.cpp \
    Dialogs/firmdetails_dialog.cpp\
    Forms/bankaccountsform.cpp \
    Forms/bonuriform.cpp \
    Forms/chartsform.cpp \
    Forms/dashboardform.cpp \
    Forms/facturiform.cpp \
    Forms/genericform.cpp \
    Forms/login_form.cpp \
    Forms/nomenclator_form.cpp \
    Forms/receptiiform.cpp \
    Forms/terti_form.cpp \
    Forms/usersform.cpp \
    Forms/newthird_form.cpp \
    ExportDialogPages/export_dialogbox.cpp \
    CustomObjects/combobox_helper.cpp \
    Forms/captchaadapter_form.cpp \
    numberspeller_helper.cpp \
    CustomObjects/xlsxsheetmodel.cpp \
    Forms/cashregister_form.cpp \
    Dialogs/importdialog.cpp \
    ExportDialogPages/tableprinter.cpp \
    Forms/currencyinfo_form.cpp \
    currencyinfo_helper.cpp \
    Dialogs/configdocuments_dialog.cpp \
    Forms/billseries_form.cpp \
    Forms/vat_form.cpp \
    Forms/stocksyncform.cpp \
    CustomObjects/customlabel.cpp \
    Delegates/select_model_delegate.cpp \
    Dialogs/billmodel_dialog.cpp


HEADERS  += mainwindow.h \
    version.h \
    dbhelper.h \
    customsqltable.h \
    filterheaderview.h \
    filterlineedit.h \
    custommodel.h \
    completer_lineedit.h \
    codbarehelper.h \
    cutereport_helper.h \
    chartswidget.h \
    textformat_helper.h \
    chartswidget.h \
    firmsettings_helper.h \
    infocodfiscalhelper.h \
    Delegates/booleandelegate.h \
    Delegates/colordelegate.h \
    Delegates/comboboxdelegate.h \
    Delegates/lineedit_completer_delegate.h \
    Delegates/passworddelegate.h \
    Delegates/readonlydelegate.h \
    Delegates/spinboxdelegate.h \
    Dialogs/about_dialog.h \
    Dialogs/bonuriproduse_dialog.h \
    Dialogs/facturiproduse_dialog.h \
    Dialogs/receiveditemsdetails_dialog.h \
    Dialogs/firmdetails_dialog.h\
    Forms/bankaccountsform.h \
    Forms/bonuriform.h \
    Forms/chartsform.h \
    Forms/dashboardform.h \
    Forms/facturiform.h \
    Forms/genericform.h \
    Forms/login_form.h \
    Forms/nomenclator_form.h \
    Forms/receptiiform.h \
    Forms/terti_form.h \
    Forms/usersform.h \
    Forms/newthird_form.h \
    ExportDialogPages/export_dialogbox.h \
    CustomObjects/combobox_helper.h \
    Forms/captchaadapter_form.h \
    numberspeller_helper.h \
    CustomObjects/xlsxsheetmodel.h \
    CustomObjects/xlsxsheetmodel_p.h \
    Forms/cashregister_form.h \
    Dialogs/importdialog.h \
    ExportDialogPages/tableprinter.h \
    Forms/currencyinfo_form.h \
    currencyinfo_helper.h \
    Dialogs/configdocuments_dialog.h \
    Forms/billseries_form.h \
    Forms/vat_form.h \
    Forms/stocksyncform.h \
    CustomObjects/customlabel.h \
    Delegates/select_model_delegate.h \
    Dialogs/billmodel_dialog.h


FORMS    += mainwindow.ui \
    codbarehelper.ui \
    chartswidget.ui \
    chartswidget.ui \
    Forms/chartsform.ui \
    Forms/dashboardform.ui \
    Forms/genericform.ui \
    Forms/login_form.ui \
    Dialogs/about_dialog.ui \
    Dialogs/bonuriproduse_dialog.ui \
    Dialogs/facturiproduse_dialog.ui \
    Dialogs/receiveditemsdetails_dialog.ui \
    Dialogs/firmdetails_dialog.ui\
    Forms/newthird_form.ui \
    ExportDialogPages/export_dialogbox.ui \
    Forms/captchaadapter_form.ui \
    Forms/cashregister_form.ui \
    Dialogs/importdialog.ui \
    Forms/currencyinfo_form.ui \
    Dialogs/configdocuments_dialog.ui \
    Forms/stocksyncform.ui \
    Dialogs/billmodel_dialog.ui

RESOURCES += \
    icons.qrc \
    animations.qrc \
    charts.qrc \
    numbers.qrc \
    pictures.qrc

DISTFILES += \
    chartHelper.qml

