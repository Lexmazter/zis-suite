#include "captchaadapter_form.h"
#include "ui_captchaadapter_form.h"

QNetworkAccessManager* nam;

CaptchaAdapter_Form::CaptchaAdapter_Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CaptchaAdapter_Form)
{
    ui->setupUi(this);

    tempPage = new QWebEnginePage;

    // Use this function to safely delete on exit
    tempPage->setParent(this);

    FirmActivityLink = "https://www.anaf.ro/inactivi/inactivi.do";

    movie.setFileName(":/Animations/loader.gif");
    ui->labelCaptha->setMovie(&movie);
    ui->labelCaptha->setAlignment(Qt::AlignCenter);
    movie.start();



    nam = new QNetworkAccessManager(this);
    connect(nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(slot_netwManagerFinished(QNetworkReply*)));

    //get new captcha  after the page has loaded
    QUrl url("https://www.anaf.ro/inactivi/kaptcha.jpg");
    QNetworkRequest request(url);
    nam->get(request);

}

CaptchaAdapter_Form::~CaptchaAdapter_Form()
{
    delete ui;
}

void CaptchaAdapter_Form::on_pushButton_clicked()
{
    if((!ui->lineEdit_CUI->text().isEmpty()) && (!ui->lineEdit->text().isEmpty()))
    {
        connect(tempPage, SIGNAL(loadFinished(bool)), this, SLOT(pageLoadFinished(bool)));
        FirmActivityLink.setQuery("inputCui="+ui->lineEdit_CUI->text() +"&captcha="+ui->lineEdit->text());
        tempPage->load(FirmActivityLink);
    }
}

void CaptchaAdapter_Form::pageLoadFinished(bool state)
{
    switch(state)
    {
    case true:
    {
        QString javaScriptCheckForError = "document.getElementsByTagName(\"a\").innerText";
        QString parseImage = "var imagelist=document.getElementsByTagName(\"img\");"
                             "var captchaImage = imagelist[0].src";
        QString javaScriptParseTable = "var c = document.getElementsByTagName(\"tbody\")[3];"
                                       "var table = c.rows;"
                                       "var text = \"\";"
                                       "for (i=0; i<table.length; i++)"
                                       "{"
                                       " text += table[i].cells[0].textContent + \",\" + table[i].cells[1].textContent + \",\";  "
                                       "}"
                                       "";
        QString javaScriptParagraph = "document.getElementsByTagName(\"p\")[3]";

        tempPage->runJavaScript(javaScriptCheckForError, [this] (const QVariant &result)
        {
            qDebug() << result;

            if(result != QVariant::Invalid)
            {
                // JavaScript callback using Lambda
                //                QString str = result.toString().remove("\n").remove("\t").trimmed();
                //                QStringList list = str.split(',');
                //                loadStringToListWidget(list);
                ui->labelCaptha->setText(result.toString());

                emit loadFinished(true);

                // Restore the cursor after parsing the data
                QApplication::restoreOverrideCursor();
            }
        });

        break;
    }

    case false:
        // Prepare a messagebox
        QMessageBox msgBox(this);
        QPixmap icon(":/Icons/Disconnected-48.png");
        msgBox.setIconPixmap(icon);
        msgBox.setText(tr("Nu se poate incărca pagina!"));
        msgBox.setInformativeText(tr("Înainte de a încerca din nou, vă rugăm să verificați conexiunea la internet."));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();

        emit loadFinished(false);

        // Restore the cursor after parsing the data
        QApplication::restoreOverrideCursor();
        break;
    }
}

void CaptchaAdapter_Form::loadListToMap(QStringList list)
{
    for(int i = 0; i < list.count(); i++)
    {
        // prevent index out of bounds error
        if(i+1 < list.count())
        {
            QueryRezult_Map.insert(list[i].trimmed().toUtf8(), list[i+1].trimmed().toUtf8());
            qDebug() << "list[i] = " << list[i].trimmed() << "list[i+1] = " << list[i+1].trimmed();
        }
    }
}

void CaptchaAdapter_Form::slot_netwManagerFinished(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Error in" << reply->url() << ":" << reply->errorString();
        return;
    }
    QVariant attribute = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (attribute.isValid()) {
        QUrl url = attribute.toUrl();
        qDebug() << "must go to:" << url;
        return;
    }
    qDebug() << "ContentType:" << reply->header(QNetworkRequest::ContentTypeHeader).toString();
    QByteArray jpegData = reply->readAll();
    QPixmap pixmap;
    pixmap.loadFromData(jpegData);
    ui->labelCaptha->setPixmap(pixmap);
}
