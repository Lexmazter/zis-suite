#ifndef TERTI_FORM_H
#define TERTI_FORM_H

#include "genericform.h"
#include "infocodfiscalhelper.h"
#include <QAction>

namespace Ui {
class terti_form;
}

class terti_form : public GenericForm
{
    Q_OBJECT

public:
    explicit terti_form(QWidget *parent = 0, QString tableName = "Terti");
    ~terti_form();

private slots:
    void on_codFiscalAction_triggered();
    void infoFiscalLoadFinished(bool state);

private:
    InfoCodFiscalHelper* codFiscalHelper;
    QAction* codFiscalAction;
    QHash<QString, int> parentHeaderHash;
    QModelIndex infoFirmaModelIndex;
};

#endif // TERTI_FORM_H
