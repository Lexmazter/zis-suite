#ifndef CURRENCYINFO_FORM_H
#define CURRENCYINFO_FORM_H

#include <QWidget>
#include "currencyinfo_helper.h"

namespace Ui {
class CurrencyInfo_Form;
}

class CurrencyInfo_Form : public QWidget
{
    Q_OBJECT

public:
    explicit CurrencyInfo_Form(QWidget *parent = 0);
    ~CurrencyInfo_Form();

private slots:
    void on_pushButton_clicked();

    void currencyInfo_LoadFinished(bool state);

private:
    Ui::CurrencyInfo_Form *ui;
    CurrencyInfo_Helper *currencyHelper;
};

#endif // CURRENCYINFO_FORM_H
