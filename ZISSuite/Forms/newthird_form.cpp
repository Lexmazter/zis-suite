#include "newthird_form.h"
#include "CustomObjects/combobox_helper.h"
#include "dbhelper.h"

NewThird_Form::NewThird_Form(QWidget *parent, int pos_x, int pos_y, QString type) :
    QDialog(parent),
    ui(new Ui::NewThird_Form)
{
    ui->setupUi(this);
    this->move(pos_x, pos_y);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    // Assign the memory only once to avoid leaks
    optionalAnimation = new QPropertyAnimation(ui->groupBox_Optional, "maximumHeight");
    overallAnimation = new QPropertyAnimation(this, "maximumHeight");

    //set up the ComboBoxes
    ComboBoxHelper *combobox_helper = new ComboBoxHelper();
    combobox_helper->setUpComboBox(ui->comboBox_Tip, COMBOBOX_TERT);
    ui->comboBox_Tip->setCurrentIndex(combobox_helper->indexOf(type.toLower()));
    combobox_helper->setUpComboBox(ui->comboBox_Tara, COMBOBOX_COUNTRY);
    ui->comboBox_Tara->setCurrentIndex(combobox_helper->indexOf("ro"));
    combobox_helper->setUpComboBox(ui->comboBox_Judet, COMBOBOX_JUDET);

}

NewThird_Form::~NewThird_Form()
{
    delete ui;
}

void NewThird_Form::on_expand_collapse_button_clicked()
{
    // collapse the box with animation
    if(ui->groupBox_Optional->maximumHeight() != 0)
    {
        initialSizeOptional = ui->groupBox_Optional->height();
        initialSizeOverall = this->height();

        optionalAnimation->setDuration(200); //value chosen according to material guidelines
        optionalAnimation->setStartValue(initialSizeOptional);
        optionalAnimation->setEndValue(0);

        overallAnimation->setDuration(200);
        overallAnimation->setStartValue(initialSizeOverall);
        overallAnimation->setEndValue(initialSizeOverall - initialSizeOptional);

        optionalAnimation->start();
        overallAnimation->start();
        QIcon expand_icon(":/Icons/Expand Arrow-48.png");
        ui->expand_collapse_button->setIcon(expand_icon);
    }
    // expand the tab with animation
    else
    {
        optionalAnimation->setDuration(300); //value chosen according to material guidelines
        optionalAnimation->setStartValue(0);
        optionalAnimation->setEndValue(initialSizeOptional);

        overallAnimation->setDuration(300);
        overallAnimation->setStartValue(initialSizeOverall - initialSizeOptional);
        overallAnimation->setEndValue(initialSizeOverall);

        optionalAnimation->start();
        overallAnimation->start();
        QIcon collapse_icon(":/Icons/Collapse Arrow-48.png");
        ui->expand_collapse_button->setIcon(collapse_icon);
    }
}
void NewThird_Form::on_btnSave_clicked()
{
    QVariantMap thirdData;
    thirdData.insert("CIF", ui->lineEdit_CIF->text().toInt());
    thirdData.insert("Denumire", ui->lineEdit_Nume->text());
    thirdData.insert("RegComert", ui->lineEdit_RegComert->text());
    thirdData.insert("Telefon", ui->lineEdit_Tel->text());
    thirdData.insert("Email", ui->lineEdit_Email->text());
    thirdData.insert("Tara", ui->comboBox_Tara->currentText());
    thirdData.insert("Judet", ui->comboBox_Judet->currentText());
    thirdData.insert("Localitate", ui->lineEdit_Loc->text());
    thirdData.insert("Adresa", ui->lineEdit_Adr->text());
    thirdData.insert("TipTert", ui->comboBox_Tip->currentText());
    thirdData.insert("Banca", ui->lineEdit_Banca->text());
    thirdData.insert("ContBancar", ui->lineEdit_ContBancar->text());
    thirdData.insert("ContBeneficiar", ui->lineEdit_ContBeneficiar->text());
    thirdData.insert("ContFurnizor", ui->lineEdit_ContFurnizor->text());
    thirdData.insert("Discount", ui->lineEdit_Discount->text());
    thirdData.insert("StareSocietate", ui->lineEdit_Status->text());
    if(ui->checkBox_VAT->isChecked())
        thirdData.insert("PlatitorTVA", 1);
    else
        thirdData.insert("PlatitorTVA", 0);
    if(ui->checkBox_Contribution->isChecked())
        thirdData.insert("ImpozitProfit", 1);
    else
        thirdData.insert("ImpozitProfit", 0);


    bool retVal = DbHelper::insertSQLFromMap("Terti", &thirdData);
    if(retVal)
        QMessageBox::information(this, tr("Info"), "Datele au fost salvate ");
    else
        QMessageBox::information(this, tr("Info"), "Datele nu au putut fi salvate ");


    this->close();
}

void NewThird_Form::on_btnCancel_clicked()
{
    this->close();
}

void NewThird_Form::infoFiscalLoadFinished(bool state)
{
    switch(state)
    {
    // Company exists, no error
    case true:
    {
        QHash <QString, QString> tempInfoFirm_Map = codFiscalHelper->getInfoFirma();

        ui->lineEdit_Nume->setText(tempInfoFirm_Map.value("Denumire platitor:"));
        ui->lineEdit_Tel->setText(tempInfoFirm_Map.value("Telefon:"));
        ui->comboBox_Judet->setCurrentIndex(ui->comboBox_Judet->findText(tempInfoFirm_Map.value("Judetul:").toUpper(), Qt::MatchWrap));
        ui->lineEdit_Adr->setText(tempInfoFirm_Map.value("Adresa:"));
        ui->lineEdit_RegComert->setText(tempInfoFirm_Map.value("Numar de inmatriculare la RegistrulComertului:"));

        //process the activity status and show it
        QString tempString =tempInfoFirm_Map.value("Stare societate:");
        if(tempString.contains("INREGISTRAT")){
            tempString.replace("INREGISTRAT", "ACTIV");
        }
        else if(tempString.contains("DIZOLVARE CU LICHIDARE(RADIERE)"))
        {
            tempString.replace("DIZOLVARE CU LICHIDARE(RADIERE)", "INACTIV");
        }
        ui->lineEdit_Status->setText(tempString);

        //process if the third is paying VAT
        tempString =tempInfoFirm_Map.value("Taxa pe valoarea adaugata (data luarii in evidenta):");
        if(tempString.isEmpty() || tempString.contains("NU") || tempString==("-"))
            ui->checkBox_VAT->setChecked(false);
        else
            ui->checkBox_VAT->setChecked(true);

        //process if the third is paying contribution
        tempString =tempInfoFirm_Map.value("Impozit pe profit (data luarii in evidenta):");
        if(tempString.isEmpty() || tempString.contains("NU") || tempString==("-"))
            ui->checkBox_Contribution->setChecked(false);
        else
            ui->checkBox_Contribution->setChecked(true);

        // TODO - Set Localitate

        break;
    }
        // Company does not exist, or some error happened
    default:
        break;
    }

    // Delete the object at the end
    //delete codFiscalHelper;
}

void NewThird_Form::on_btnTakeOverInfo_clicked()
{
    QString codFiscal = ui->lineEdit_CIF->text();

    // Get the data
    codFiscalHelper = new InfoCodFiscalHelper(this, codFiscal);

    // We can parse the data only after we get it from the web
    connect(codFiscalHelper, SIGNAL(loadFinished(bool)),
            this, SLOT(infoFiscalLoadFinished(bool)));
}


void NewThird_Form::mousePressEvent(QMouseEvent* event)
{
    if(event->button() == Qt::LeftButton)
    {
        mMoving = true;
        mLastMousePosition = event->pos();
    }
}

void NewThird_Form::mouseMoveEvent(QMouseEvent* event)
{
    if( event->buttons().testFlag(Qt::LeftButton) && mMoving)
    {
        this->move(this->pos() + (event->pos() - mLastMousePosition));
    }
}

void NewThird_Form::mouseReleaseEvent(QMouseEvent* event)
{
    if(event->button() == Qt::LeftButton)
    {
        mMoving = false;
    }
}
