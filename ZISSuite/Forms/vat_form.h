#ifndef VAT_FORM_H
#define VAT_FORM_H

#include <Forms/genericform.h>
#include "ui_genericform.h"

namespace Ui {
class VATForm;
}

class VATForm : public GenericForm
{
    Q_OBJECT

public:
    explicit VATForm(QWidget *parent = 0, QString tableName = "TVA");
    ~VATForm();

private slots:

private:

};

#endif // VAT_FORM_H
