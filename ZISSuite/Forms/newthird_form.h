#ifndef NEWTHIRD_FORM_H
#define NEWTHIRD_FORM_H

#include "ui_newthird_form.h"
#include "infocodfiscalhelper.h"
#include <qpropertyanimation.h>

namespace Ui {
class NewThird_Form;
}

class NewThird_Form : public QDialog
{
    Q_OBJECT

public:
    explicit NewThird_Form(QWidget *parent = 0, int pos_x = 0, int pos_y = 0, QString type="");
    ~NewThird_Form();

private slots:

    void on_expand_collapse_button_clicked();

    void on_btnSave_clicked();

    void on_btnCancel_clicked();

    void infoFiscalLoadFinished(bool state);

    void on_btnTakeOverInfo_clicked();

    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);


private:
    Ui::NewThird_Form *ui;
    QPoint mLastMousePosition;
    bool mMoving;
    int initialSizeOptional;
    int initialSizeOverall;
    QPropertyAnimation *optionalAnimation;
    QPropertyAnimation *overallAnimation;
    InfoCodFiscalHelper* codFiscalHelper;
};

#endif // NEWTHIRD_FORM_H
