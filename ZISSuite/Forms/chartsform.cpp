#include "chartsform.h"
#include "ui_chartsform.h"

chartsForm::chartsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::chartsForm)
{
    ui->setupUi(this);

    chartWidget = new ChartsWidget(this);

    ui->frame->layout()->addWidget(chartWidget);
}

chartsForm::~chartsForm()
{
    delete ui;
}

void chartsForm::on_toolButton_clicked()
{
    chartWidget->showChart();
}

void chartsForm::on_pushButtonAddDataset_clicked()
{
    chartWidget->addDataset(ui->comboBoxChartType->currentIndex(),ui->lineEditSetName->text(), ui->lineEditData->text(),ui->lineEditLabels->text());

    ui->labelDatasetNumber->setText(QString::number(chartWidget->datasetsCount()));

    // After the first set has been added, we cannot change the labels anymore
    if(chartWidget->datasetsCount() != 0)
        ui->lineEditLabels->setReadOnly(true);
}

void chartsForm::on_toolButton_2_clicked()
{
    chartWidget->clearDatasets();

    ui->labelDatasetNumber->setText(QString::number(chartWidget->datasetsCount()));

    ui->lineEditLabels->setReadOnly(false);
}
