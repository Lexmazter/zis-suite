#ifndef STOCKSYNCFORM_H
#define STOCKSYNCFORM_H

#include <QWidget>
#include <QFutureWatcher>

namespace Ui {
class StockSyncForm;
}

class StockSyncForm : public QWidget
{
    Q_OBJECT

public:
    explicit StockSyncForm(QWidget *parent = 0);
    ~StockSyncForm();

private slots:
    void handleFinished();
private:
    void stockSync(void);
    QFutureWatcher<void>* watcher;
    Ui::StockSyncForm *ui;
};

#endif // STOCKSYNCFORM_H
