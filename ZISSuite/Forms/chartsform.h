#ifndef CHARTSFORM_H
#define CHARTSFORM_H

#include <QWidget>
#include <chartswidget.h>

namespace Ui {
class chartsForm;
}

class chartsForm : public QWidget
{
    Q_OBJECT

public:
    explicit chartsForm(QWidget *parent = 0);
    ~chartsForm();

private slots:
    void on_toolButton_clicked();

    void on_pushButtonAddDataset_clicked();

    void on_toolButton_2_clicked();

private:
    ChartsWidget* chartWidget;
    Ui::chartsForm *ui;
};

#endif // CHARTSFORM_H
