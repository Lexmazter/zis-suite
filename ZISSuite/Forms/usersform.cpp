#include "usersform.h"

UsersForm::UsersForm(QWidget *parent, QString tableName) :
    GenericForm(parent, tableName)
{
    // Get all the headers so we know where to put what
    QMap<QString, int> headerMap;
    for(int i=0; i < GenericForm::ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        headerMap.insert(GenericForm::ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    passwordIndex = headerMap.find("Parola").value();
    password2Index = headerMap.find("Repeta Parola").value();

    passDelegate = new PasswordDelegate(this);

    GenericForm::ui->tableView->setItemDelegateForColumn(passwordIndex, passDelegate);
    GenericForm::ui->tableView->setItemDelegateForColumn(password2Index,passDelegate);

    // Beautification
    GenericForm::stretchColumnSize(5);
    GenericForm::stretchColumnSize(6);
}

UsersForm::~UsersForm()
{

}

void UsersForm::on_actionSave_triggered()
{
    bool allOkay = true;

    for(int i = 0; i <GenericForm::ui->tableView->model()->rowCount(); i++)
    {
        QModelIndex indexParola = GenericForm::ui->tableView->model()->index(i, passwordIndex);
        QModelIndex idnexParola2 = GenericForm::ui->tableView->model()->index(i, password2Index);

        if(GenericForm::ui->tableView->model()->data(indexParola).toString() !=
              GenericForm::ui->tableView->model()->data(idnexParola2).toString() )
        {
            // Prepare a messagebox
            QMessageBox msgBox(this);
            QPixmap icon(":/Icons/Password-48.png");
            msgBox.setIconPixmap(icon);
            msgBox.setText(tr("Parolele nu se potrivesc!"));
            msgBox.setInformativeText(tr("Ambele parole trebuie sa fie identice, din campul \"Parola\" si \"Repeta parola\"."));
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();

            allOkay = false;

            break;
        }
    }

    if(allOkay)
    {
        DbHelper::saveTableModelToDb(ui->tableView->model());
        GenericForm::ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

        GenericForm::clearAllColors();
    }
}
