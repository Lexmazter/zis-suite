#ifndef BONURIFORM_H
#define BONURIFORM_H

#include <Forms/genericform.h>
#include "ui_genericform.h"

namespace Ui {
class BonuriForm;
}

class BonuriForm : public GenericForm
{
    Q_OBJECT

public:
    explicit BonuriForm(QWidget *parent = 0, QString tableName = "Bonuri");
    ~BonuriForm();

private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_actionAdd_triggered();
private:

};

#endif // BONURIFORM_H
