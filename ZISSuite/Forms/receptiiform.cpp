#include "receptiiform.h"
#include "Dialogs/receiveditemsdetails_dialog.h"
#include "Delegates/booleandelegate.h"

ReceptiiForm::ReceptiiForm(QWidget *parent, QString tableName) :
    GenericForm(parent, tableName)
{
    // Beautification
    GenericForm::stretchColumnSize(1);
    GenericForm::stretchColumnSize(2);

    // Get all the headers so we know where to put what
    QMap<QString, int> headerMap;
    for(int i=0; i < ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        headerMap.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    tertIndex = headerMap.find("idTert").value();

    // Hide produse and cantitate
    if(headerMap.contains("Produse"))
        ui->tableView->setColumnHidden(headerMap.value("Produse"), true);
    if(headerMap.contains("Cantitate"))
        ui->tableView->setColumnHidden(headerMap.value("Cantitate"), true);

    // Set the relation first
    CustomSqlTable* tempTable = dynamic_cast<CustomSqlTable*>(ui->tableView->model());
    tempTable->setRelation(tertIndex, QSqlRelation("Terti", "ID", "Denumire as Tert"));
    tempTable->select();

    BooleanDelegate* boolDelegate = new BooleanDelegate(this);

    // Now we set the delegate (does not seem to work...)
    ui->tableView->setItemDelegate(new QSqlRelationalDelegate(ui->tableView));

    if(headerMap.contains("Achitat"))
        ui->tableView->setItemDelegateForColumn(headerMap.value("Achitat"),boolDelegate);

}

ReceptiiForm::~ReceptiiForm()
{

}

void ReceptiiForm::on_actionAdd_triggered()
{
    GenericForm::on_actionAdd_triggered();
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
}

void ReceptiiForm::on_actionRefresh_triggered()
{
    GenericForm::on_actionRefresh_triggered();

    // Set the relation again
    CustomSqlTable* tempTable = dynamic_cast<CustomSqlTable*>(ui->tableView->model());
    tempTable->setRelation(tertIndex, QSqlRelation("Terti", "ID", "Denumire as Tert"));
    tempTable->select();
}


void ReceptiiForm::on_tableView_doubleClicked(const QModelIndex &index)
{
    Q_UNUSED(index);

    int actual_raw = index.row();

    ReceivedItemsDetails_Dialog* receivedItemsDialog= new ReceivedItemsDetails_Dialog(this, ui->tableView, actual_raw);
    receivedItemsDialog->exec();
}
