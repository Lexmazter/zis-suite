#include "Forms/billseries_form.h"
#include "Delegates/select_model_delegate.h"

BillSeriesForm::BillSeriesForm(QWidget *parent, QString tableName) :
    GenericForm(parent, tableName)
{
    // Beautification
    GenericForm::stretchColumnSize(1);
    GenericForm::stretchColumnSize(2);

    // Get all the headers so we know where to put what
    QMap<QString, int> headerMap;
    for(int i=0; i < ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        headerMap.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    if(headerMap.contains("Model"))
    {
        SelectModelDelegate *p_SelectDelegate = new SelectModelDelegate(ui->tableView);
        ui->tableView->setItemDelegateForColumn(headerMap.value("Model"), p_SelectDelegate);
    }
}

//void BillSeriesForm::on_tableView_doubleClicked(const QModelIndex &index)
//{
//    Q_UNUSED(index);

//    ui->tableView->setEditTriggers(QAbstractItemView::DoubleClicked);
//}


BillSeriesForm::~BillSeriesForm()
{

}
