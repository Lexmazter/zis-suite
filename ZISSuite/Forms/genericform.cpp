#include "genericform.h"
#include "ui_genericform.h"
#include "Delegates/spinboxdelegate.h"
#include "Dialogs/bonuriproduse_dialog.h"
#include "mainwindow.h"
#include "Delegates/comboboxdelegate.h"
#include "CustomObjects/combobox_helper.h"

GenericForm::GenericForm(QWidget *parent, QString tableName) :
    QWidget(parent),
    ui(new Ui::GenericForm)
{
    ui->setupUi(this);

    // Save the table name to use for queries
    this->tableName = tableName;

    // We use only one color delegate for the entire form
    colorDelegateForColumn = new ColorDelegate(ui->tableView);

    filter = new FilterHeaderView(ui->tableView);
    ui->tableView->setHorizontalHeader(filter);

    ui->tableView->setModel(DbHelper::getTableModel(this, this->tableName, colorDelegateForColumn));
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->show();

    // Get all the headers so we know where to put what
    QMap<QString, int> parentHeader;
    for(int i=0; i <  ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        parentHeader.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    // TODO - Check for user permission and settings first
    if(parentHeader.contains("User"))
        ui->tableView->setColumnHidden(parentHeader.value("User"), true);

    if(parentHeader.contains("UM"))
    {
        ComboBoxDelegate* comboDelegateForUm = new ComboBoxDelegate(ui->tableView, ComboBoxHelper::getComboboxMap(COMBOBOX_UM));
        ui->tableView->setItemDelegateForColumn(parentHeader.value("UM"), comboDelegateForUm);
    }

    // Color delegate only works if set on 0 column
    ui->tableView->setItemDelegateForColumn(0, colorDelegateForColumn);

    filter->generateFilters(ui->tableView->model()->columnCount(),true);

    // Beautification should be implemented by each form!

    QHeaderView *m_horiz_header = ui->tableView->horizontalHeader();

    m_horiz_header->setSortIndicatorShown(true);
    m_horiz_header->setSectionsClickable(true);
    m_horiz_header->setSelectionMode(QAbstractItemView::SelectionMode::NoSelection);

    // connect the sorting event
    connect(m_horiz_header,SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
            this ,SLOT(sortChanged(int, Qt::SortOrder)));

    // connect the filter event
    connect(filter, SIGNAL(filterChanged(int, QString)),
            this, SLOT(on_filterChanged(int,QString)));

    // connect the data changed event for modified color
    connect(ui->tableView->model(), SIGNAL(dataChanged(QModelIndex,QModelIndex)),
            this, SLOT(onDataChanged(QModelIndex,QModelIndex)));

    // get our context menu up and running, last added appears first
    ui->tableView->insertAction(NULL,ui->actionDelete);
    ui->tableView->insertAction(ui->actionDelete,ui->actionSave);
    ui->tableView->insertAction(ui->actionSave,ui->actionRefresh);
    ui->tableView->insertAction(ui->actionRefresh,ui->actionAdd);
}

GenericForm::~GenericForm()
{
    delete ui;
}

void GenericForm::stretchColumnSize(int indexColumn)
{
    ui->tableView->horizontalHeader()->setSectionResizeMode(indexColumn, QHeaderView::Stretch);
}

void GenericForm::clearAllColors()
{
    // Remove color from all rows, as we can mark multiple rows for deletion
    // and we can also add multiple products before save
    colorDelegateForColumn->clearColoredRows();
}

void GenericForm::on_tableView_clicked(const QModelIndex &index)
{
    Q_UNUSED(index);
}

void GenericForm::sortChanged(int index, Qt::SortOrder order)
{
    bool refresh = true;

    if(static_cast<CustomSqlTable*>(ui->tableView->model())->isDirty())
    {
        int question = QMessageBox::question(this, "Modificari nesalvate!",
                                             "Exista modificari nesalvate, "
                                             "sortarea datelor va duce la pierderea lor.\n\n"
                                             "Doriti sa sortati?");

        if(question == QMessageBox::No)
        {
            refresh = false;
        }
    }

    if(refresh)
    {
        // do the sorting
        ui->tableView->model()->sort(index, order);

        // Clear colors as the add/delete events are not kept between sorting
        clearAllColors();
    }
}

void GenericForm::onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight)
{
    Q_UNUSED(bottomRight);

    // Send the index number for coloring
    QModelIndex index = ui->tableView->model()->index(topLeft.row(), 0);

    colorDelegateForColumn->addModifiedRow(index.data().toString());
}

void GenericForm::on_actionDelete_triggered()
{
    foreach (QModelIndex index, ui->tableView->selectionModel()->selectedIndexes())
    {
        // Send the index number for coloring
        QModelIndex tempIndex = ui->tableView->model()->index(index.row(), 0);

        // Color the marked for delete row
        colorDelegateForColumn->addDeletedRow(tempIndex.data().toString());

        // We now mark for deletion the row
        ui->tableView->model()->removeRows(tempIndex.row(),1);
    }
}

void GenericForm::on_actionAdd_triggered()
{
    if(ui->tableView->currentIndex().isValid())
    {
        // Add our new row above the selected row
        ui->tableView->model()->insertRow(ui->tableView->currentIndex().row());
    }
    else
    {
        // Add our new row at the beginning
        ui->tableView->model()->insertRow(0);
    }

    if(userIndex != 0) // Some tables might not use this field
    {
        // Set the current user by default for history purposes
        QModelIndex tempModelIndex = ui->tableView->model()->index(ui->tableView->currentIndex().row()-1, userIndex);
        ui->tableView->model()->setData(tempModelIndex, MainWindow::getInstance()->currentUser().toString(), Qt::EditRole);
    }

    // Set the edit triggers
    ui->tableView->setEditTriggers(QAbstractItemView::AllEditTriggers);
}

void GenericForm::on_actionRefresh_triggered()
{
    bool refresh = true;

    if(static_cast<CustomSqlTable*>(ui->tableView->model())->isDirty())
    {
        int question = QMessageBox::question(this, "Modificari nesalvate!",
                                             "Exista modificari nesalvate, "
                                             "reimprospatarea datelor va duce la pierderea lor.\n\n"
                                             "Doriti sa reimprospatati?");

        if(question == QMessageBox::No)
            refresh = false;
    }

    if(refresh)
    {
        ui->tableView->setModel(DbHelper::getTableModel(this, tableName, colorDelegateForColumn));
        ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->tableView->show();

        // connect the data changed event for modified color for the new model
        connect(ui->tableView->model(), SIGNAL(dataChanged(QModelIndex,QModelIndex)),
                this, SLOT(onDataChanged(QModelIndex,QModelIndex)));

        clearAllColors();
    }
}

void GenericForm::on_actionSave_triggered()
{
    DbHelper::saveTableModelToDb(ui->tableView->model());
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    clearAllColors();
}

void GenericForm::on_tableView_doubleClicked(const QModelIndex &index)
{
    Q_UNUSED(index);

    ui->tableView->setEditTriggers(QAbstractItemView::AllEditTriggers);
}

void GenericForm::on_filterChanged(int column, QString value)
{
    bool refresh = true;

    if(static_cast<CustomSqlTable*>(ui->tableView->model())->isDirty())
    {
        int question = QMessageBox::question(this, "Modificari nesalvate!",
                                             "Exista modificari nesalvate, "
                                             "filtrarea datelor va duce la pierderea lor.\n\n"
                                             "Doriti sa filtrati?");

        if(question == QMessageBox::No)
        {
            refresh = false;
            filter->clearFilters();
        }
    }

    if(refresh)
    {
        QString filterClause;

        if(column == 0)
        {
            filterClause = tableName+"."+ ui->tableView->model()->headerData(column, Qt::Horizontal).toString() +" LIKE '%"+ value +"%'";
        }
        else
        {
            filterClause = ui->tableView->model()->headerData(column, Qt::Horizontal).toString() +" LIKE '%"+ value +"%'";
        }

        static_cast<CustomSqlTable*>(ui->tableView->model())->setFilter(filterClause);

        // Clear the colors as the deletion or add are not kept between filter events
        clearAllColors();
    }
}

QTableView* GenericForm::getTableInstance()
{
    return ui->tableView;
}
