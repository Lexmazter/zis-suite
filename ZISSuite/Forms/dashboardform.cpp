#include "dashboardform.h"
#include "ui_dashboardform.h"
#include <dbhelper.h>
#include <QDate>
#include <QSqlQueryModel>
#include <QGraphicsPixmapItem>
#include "mainwindow.h"

#define DATA_ARRAY 1
#define LABEL_ARRAY 0

DashboardForm::DashboardForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DashboardForm)
{
    ui->setupUi(this);

    chartMonthlyPerDay = nullptr;
    chartMonthly = nullptr;
    chartYearly = nullptr;

    loadData();
}

DashboardForm::~DashboardForm()
{
    delete ui;
}

void DashboardForm::loadData()
{
    // Show yearly status widget
    QString* tempString = getTotalThisYear("receptii");

    if (chartYearly)
        delete chartYearly;

    chartYearly = new ChartsWidget(this);
    chartYearly->setToolTip("Raportul cu receptiile si facturile pe fiecare luna din an.");
    chartYearly->addDataset(CHART_BAR, "Receptii", tempString[DATA_ARRAY], tempString[LABEL_ARRAY]);

    tempString = getTotalThisYear("vanzari");

    chartYearly->addDataset(CHART_BAR, "Facturi", tempString[DATA_ARRAY], tempString[LABEL_ARRAY]);

    ui->groupBoxYear->layout()->addWidget(chartYearly);
    chartYearly->showChart();

    // Show monthly per day widget
    tempString = getTotalPerDayThisMonth("receptii");

    if (chartMonthlyPerDay)
        delete chartMonthlyPerDay;

    chartMonthlyPerDay = new ChartsWidget(this);
    chartMonthlyPerDay->setToolTip("Raportul cu receptiile si facturile pe fiecare zi din luna.");
    chartMonthlyPerDay->addDataset(CHART_BAR, "Receptii", tempString[DATA_ARRAY], tempString[LABEL_ARRAY]);

    tempString = getTotalPerDayThisMonth("vanzari");

    chartMonthlyPerDay->addDataset(CHART_BAR, "Facturi", tempString[DATA_ARRAY], tempString[LABEL_ARRAY]);

    ui->groupBoxMonth->layout()->addWidget(chartMonthlyPerDay);
    chartMonthlyPerDay->showChart();

    // Show monthly status widget
    QString vanzari = getTotalVanzariThisMonth();
    QString receptii = getTotalReceptiiThisMonth();

    if (chartMonthly)
        delete chartMonthly;

    chartMonthly = new ChartsWidget(this);
    chartMonthly->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred);
    chartMonthly->setToolTip("Raportul dintre vanzari (bonuri + facturi) si receptii (intrari).");
    chartMonthly->addDataset(CHART_DOUGHNUT, "", vanzari+","+receptii, "Vanzari, Receptii");
    ui->groupBoxMonth->layout()->addWidget(chartMonthly);
    chartMonthly->showChart();

    // draw notification dummy
    setNotificationIcon(ui->facturiButton,getTotalScadenteUntilToday("vanzari"));
    setNotificationIcon(ui->intrariButton,getTotalScadenteUntilToday("receptii"));

    // Get current company logo and name
    getCompanyLogoAndName("1");
}

void DashboardForm::setNotificationIcon(QToolButton* source, QString number)
{
    QPixmap pix;
    pix = source->icon().pixmap(source->icon().availableSizes().last());
    QPainter paint(&pix);
    QPixmap notification;

    switch(number.toInt())
    {
    case 0:
        notification.load(":/Icons/Numbers/0.png");
        break;
    case 1:
        notification.load(":/Icons/Numbers/1.png");
        break;
    case 2:
        notification.load(":/Icons/Numbers/2.png");
        break;
    case 3:
        notification.load(":/Icons/Numbers/3.png");
        break;
    case 4:
        notification.load(":/Icons/Numbers/4.png");
        break;
    case 5:
        notification.load(":/Icons/Numbers/5.png");
        break;
    case 6:
        notification.load(":/Icons/Numbers/6.png");
        break;
    case 7:
        notification.load(":/Icons/Numbers/7.png");
        break;
    case 8:
        notification.load(":/Icons/Numbers/8.png");
        break;
    case 9:
        notification.load(":/Icons/Numbers/9.png");
        break;
    default:
        notification.load(":/Icons/Numbers/9+.png");
        break;

    }
    notification = notification.scaled(24,24,Qt::IgnoreAspectRatio,Qt::SmoothTransformation);

    paint.drawPixmap(source->rect().topLeft(),notification);

    source->setIcon(QIcon(pix));
}

QString* DashboardForm::getTotalThisYear(QString table)
{
    QString* retVal = new QString[2] {"",""};
    QString selectClause = "sum(total)";
    QString condition = "";

    QDate tempDate = QDate::currentDate();

    QDate workingDate;
    workingDate.setDate(tempDate.year(),1,1);

    for(int i = 1; i <= tempDate.month(); i++)
    {
        if(i != 1)
        {
            retVal[LABEL_ARRAY].append(","); // Contains month label
            retVal[DATA_ARRAY].append(","); // Contains month value
        }

        condition = "data like \"%-" + workingDate.toString("MM-yyyy")+"%\"";

        QSqlQueryModel* tempModel = DbHelper::getModelFromQuery(this, table, &selectClause, &condition);

        if(tempModel->rowCount() != 0)
        {
            QModelIndex tempIndex = tempModel->index(0,0);
            QString tempString = tempModel->data(tempIndex).toString();

            if(tempString.isEmpty())
                retVal[DATA_ARRAY].append("0");
            else
                retVal[DATA_ARRAY].append(tempString);
        }
        else
        {
            retVal[DATA_ARRAY].append("0");
        }

        retVal[LABEL_ARRAY].append(workingDate.toString("MMMM"));

        workingDate = workingDate.addMonths(1);
    }

    return retVal;
}

QString* DashboardForm::getTotalPerDayThisMonth(QString table)
{
    QString* retVal = new QString[2] {"",""};
    QString selectClause = "sum(total)";
    QString condition = "";

    QDate tempDate = QDate::currentDate();

    QDate workingDate;
    workingDate.setDate(tempDate.year(),tempDate.month(),1);

    for(int i = 1; i <= tempDate.day(); i++)
    {
        if(i != 1)
        {
            retVal[LABEL_ARRAY].append(","); // Contains day label
            retVal[DATA_ARRAY].append(","); // Contains day value
        }

        condition = "data like \"" + workingDate.toString("dd-MM-yyyy")+"%\"";

        QSqlQueryModel* tempModel = DbHelper::getModelFromQuery(this, table, &selectClause, &condition);

        if(tempModel->rowCount() != 0)
        {
            QModelIndex tempIndex = tempModel->index(0,0);
            QString tempString = tempModel->data(tempIndex).toString();

            if(tempString.isEmpty())
                retVal[DATA_ARRAY].append("0");
            else
                retVal[DATA_ARRAY].append(tempString);
        }
        else
        {
            retVal[DATA_ARRAY].append("0");
        }

        retVal[LABEL_ARRAY].append(workingDate.toString("dddd") + " " +workingDate.toString("dd"));

        workingDate = workingDate.addDays(1);
    }

    return retVal;
}

QString DashboardForm::getTotalVanzariThisMonth()
{
    QString retVal = "";
    QString table = "vanzari";
    QString selectClause = "sum(total)";
    QString condition = "data like \"%-";

    QDate tempDate = QDate::currentDate();

    condition.append(tempDate.toString("MM-yyyy")+"%\"");

    QSqlQueryModel* tempModel = DbHelper::getModelFromQuery(this, table, &selectClause, &condition);

    if(tempModel->rowCount() != 0)
    {
        QModelIndex tempIndex = tempModel->index(0,0);
        retVal = tempModel->data(tempIndex).toString();
    }

    table = "bonuri";

    tempModel = DbHelper::getModelFromQuery(this, table, &selectClause, &condition);

    if(tempModel->rowCount() != 0)
    {
        QModelIndex tempIndex = tempModel->index(0,0);
        double tempDoubleVanzari = retVal.toDouble();
        double tempDoubleBonuri = tempModel->data(tempIndex).toDouble();

        retVal = QString::number(tempDoubleBonuri + tempDoubleVanzari);
    }

    if (retVal.isEmpty())
    {
        return "0";
    }
    else
    {
        return retVal;
    }
}

QString DashboardForm::getTotalReceptiiThisMonth()
{
    QString retVal = "";
    QString table = "receptii";
    QString selectClause = "sum(total)";
    QString condition = "data like \"%-";

    QDate tempDate = QDate::currentDate();

    condition.append(tempDate.toString("MM-yyyy")+"\"");

    QSqlQueryModel* tempModel = DbHelper::getModelFromQuery(this, table, &selectClause, &condition);

    if(tempModel->rowCount() != 0)
    {
        QModelIndex tempIndex = tempModel->index(0,0);
        retVal = tempModel->data(tempIndex).toString();
    }

    if (retVal.isEmpty())
    {
        return "0";
    }
    else
    {
        return retVal;
    }
}

QString DashboardForm::getTotalScadenteUntilToday(QString table)
{
    int tempInt = 0;
    QString retVal = "";
    QString selectClause = "count(id)";
    QString condition = "";

    QDate tempDate = QDate::currentDate();

    QDate workingDate;
    workingDate.setDate(tempDate.year(),1,1);

    for(int i = 1; i <= tempDate.month(); i++)
    {
        condition = "achitat=0 and data like \"%-" + workingDate.toString("MM-yyyy")+"\"";

        QSqlQueryModel* tempModel = DbHelper::getModelFromQuery(this, table, &selectClause, &condition);

        if(tempModel->rowCount() != 0)
        {
            QModelIndex tempIndex = tempModel->index(0,0);
            tempInt += tempModel->data(tempIndex).toInt();
        }

        workingDate = workingDate.addMonths(1);
    }

    return retVal.number(tempInt);
}

void DashboardForm::getCompanyLogoAndName(QString companyId)
{
    QString conditionalQuery = "ID = " + companyId;
    QVariantMap *companyData = new QVariantMap;

    bool retVal = DbHelper::queryToMap("DateFirma", "*", conditionalQuery, companyData);

    if(retVal)
    {
        // Set logo
        QPixmap outPixmap = QPixmap();
        outPixmap.loadFromData(companyData->value("Logo").toByteArray());

        outPixmap = outPixmap.scaledToHeight(48, Qt::SmoothTransformation);

        ui->logoButton->setIconSize(QSize(outPixmap.width(), outPixmap.height()));

        ui->logoButton->setIcon(QIcon(outPixmap));

        // Set name
        ui->logoButton->setText(companyData->value("Denumire").toString().trimmed());
    }
}

void DashboardForm::on_logoButton_clicked()
{
    MainWindow *tempWindow = MainWindow::getInstance();

    QMetaObject::invokeMethod(tempWindow,
                              "on_toolButton_firm_clicked",
                              Qt::QueuedConnection);
}

void DashboardForm::on_facturiButton_clicked()
{
    MainWindow *tempWindow = MainWindow::getInstance();

    QMetaObject::invokeMethod(tempWindow,
                              "slotChoseFacturi",
                              Qt::QueuedConnection);
}

void DashboardForm::on_intrariButton_clicked()
{
    MainWindow *tempWindow = MainWindow::getInstance();

    QMetaObject::invokeMethod(tempWindow,
                              "on_toolButtonIntrari_clicked",
                              Qt::QueuedConnection);
}
