#include "stocksyncform.h"
#include "ui_stocksyncform.h"
#include "dbhelper.h"
#include "customsqltable.h"
#include <QtConcurrent/QtConcurrent>
#include "QFutureWatcher"
#include "QMovie"
#include "custommodel.h"

#define NOMENCLATOR_STOC_COLUMN (4)
#define PRODUSE_COLUMN          (0)

StockSyncForm::StockSyncForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StockSyncForm)
{
    ui->setupUi(this);

    // Show our loading animation
    ui->animationLabel->setVisible(true);
    QMovie *movie = new QMovie(this);
    movie->setFileName(":/Animations/loader.gif");
    ui->animationLabel->setMovie(movie);
    movie->start();

    // Start the import in a new thread
    watcher = new QFutureWatcher<void>();
    connect(watcher, SIGNAL(finished()), this, SLOT(handleFinished()));

    QFuture<void> future = QtConcurrent::run(this, &StockSyncForm::stockSync);
    watcher->setFuture(future);
}

StockSyncForm::~StockSyncForm()
{
    delete ui;
}

void StockSyncForm::handleFinished()
{
    this->close();
}

QMap<int, int> getMapFromTable(QSqlQueryModel* table)
{
    QMap<int, int> theMap;
    QMap<QString, int> header;
    int produseColumn = 0;
    int cantitateColumn = 0;

    for(int i=0; i <  table->columnCount(); i++)
    {
        header.insert(table->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    if(header.contains("Produse"))
        produseColumn = header.value("Produse");

    if(header.contains("Cantitate"))
        cantitateColumn = header.value("Cantitate");

    for (int i = 0; i < table->rowCount(); i++)
    {
        QStringList produseList = table->index(i, produseColumn).data().toString().split(",");
        QStringList cantitateList = table->index(i, cantitateColumn).data().toString().split(",");

        for(int i = 0; i < produseList.count(); i++)
        {
            int produs = QString(produseList.value(i)).toInt();
            int cantitate = QString(cantitateList.value(i)).toInt();

            if(theMap.contains(produs))
            {
                // Add the quantities as we have only one key
                cantitate += theMap.take(produs);
                theMap.insert(produs, cantitate);
            }
            else
            {
                theMap.insert(produs, cantitate);
            }
        }
    }

    return theMap;
}

void StockSyncForm::stockSync()
{
    // Create a dummy object to use as a parent in this thread
    QObject* dummyParent = new QObject();

    // Get all tables
    QSqlQueryModel* receptiiTable = DbHelper::getModelFromQuery(dummyParent, "Receptii");
    QSqlQueryModel* vanzariTable = DbHelper::getModelFromQuery(dummyParent, "vanzari");
    QSqlQueryModel* bonuriTable = DbHelper::getModelFromQuery(dummyParent, "bonuri");

    CustomSqlTable* nomenclatorTable = DbHelper::getTableModel(dummyParent,"nomenclator", nullptr);

    // We want to save on field change, not on submit
    nomenclatorTable->setEditStrategy(QSqlTableModel::OnFieldChange);

    // Start parsing the receptii
    QMap<int, int> receptiiMap = getMapFromTable(receptiiTable);

    // Start parsing vanzari
    QMap<int, int> vanzariMap = getMapFromTable(vanzariTable);

    // Start parsing bonuri
    QMap<int, int> bonuriMap = getMapFromTable(bonuriTable);

    // Start updating the quantities in nomenclator
    for(int i = 0; i < nomenclatorTable->rowCount(); i++)
    {
        QModelIndex indexId = nomenclatorTable->index(i, PRODUSE_COLUMN);
        int id = indexId.data().toInt();

        QModelIndex indexStock = nomenclatorTable->index(i, NOMENCLATOR_STOC_COLUMN);

        // Reset to 0 the stock first
        int quantity = 0;

        // Add the stock from the received list
        if(receptiiMap.contains(id))
        {
            quantity = receptiiMap.value(id);
        }

        // Substract the stock from the output list
        if(vanzariMap.contains(id))
        {
            quantity -= vanzariMap.value(id);
        }

        // Substract the stock from the bill list
        if(bonuriMap.contains(id))
        {
            quantity -= bonuriMap.value(id);
        }

        // Set the quantity in the table
        nomenclatorTable->setData(indexStock, quantity);
    }

    // Delete the dummy parent in order to delete all the stuff from here
    delete dummyParent;
}
