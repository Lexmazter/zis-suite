#include "cashregister_form.h"
#include "ui_cashregister_form.h"
#include "Dialogs/facturiproduse_dialog.h"

CashRegister_Form::CashRegister_Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CashRegister_Form)
{
    ui->setupUi(this);
}

CashRegister_Form::~CashRegister_Form()
{
    delete ui;
}

void CashRegister_Form::on_toolButton_Receipt_clicked()
{

}

void CashRegister_Form::on_toolButton_Other_In_clicked()
{

}

void CashRegister_Form::on_toolButton_Bill_Out_clicked()
{

}

void CashRegister_Form::on_toolButton_Ticket_Out_clicked()
{

}

void CashRegister_Form::on_toolButton_Other_Out_clicked()
{

}
