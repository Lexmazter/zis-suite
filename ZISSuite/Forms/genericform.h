#ifndef GENERICFORM_H
#define GENERICFORM_H

#include <QWidget>
#include "dbhelper.h"
#include "filterheaderview.h"
#include "Delegates/colordelegate.h"

namespace Ui {
class GenericForm;
}

class GenericForm : public QWidget
{
    Q_OBJECT

public:
    explicit GenericForm(QWidget *parent = 0, QString tableName = "");
    ~GenericForm();

    QTableView* getTableInstance();
    void stretchColumnSize(int indexColumn);

public slots:
    virtual void on_actionSave_triggered();

protected slots:
    virtual void on_tableView_doubleClicked(const QModelIndex &index);

    virtual void on_actionAdd_triggered();

    virtual void on_actionDelete_triggered();

    virtual void on_actionRefresh_triggered();

    virtual void onDataChanged(const QModelIndex& ,const QModelIndex&);

    virtual void sortChanged(int index, Qt::SortOrder order);

private slots:

    void on_filterChanged(int column, QString value);

    void on_tableView_clicked(const QModelIndex &index);

protected:
    Ui::GenericForm *ui;
    void clearAllColors();

private:
    ColorDelegate* colorDelegateForColumn;
    FilterHeaderView* filter;
    QString tableName = "";
    int userIndex = 0;
};

#endif // GENERICFORM_H
