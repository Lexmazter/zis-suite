#ifndef VANZARIFORM_H
#define VANZARIFORM_H

#include "genericform.h"
#include "ui_genericform.h"

namespace Ui {
class FacturiForm;
}

class FacturiForm : public GenericForm
{
    Q_OBJECT

public:
    explicit FacturiForm(QWidget *parent = 0, QString tableName = "Vanzari");
    ~FacturiForm();

public slots:


private slots:

    void on_tableView_doubleClicked(const QModelIndex &index);
    void on_actionRefresh_triggered();
    void on_actionProformaInvoice_triggered();
    void on_actionReceipt_triggered();
    void on_actionPrintBill_triggered();
    void on_actionAdd_triggered();

private:
    int tertIndex = 0;
    int seriesIndex = 0;
    void setContextMenuActionsForTable(void);
};

#endif // VANZARIFORM_H
