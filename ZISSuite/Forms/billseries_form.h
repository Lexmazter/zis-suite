#ifndef BILLSERIES_H
#define BILLSERIES_H

#include <Forms/genericform.h>
#include "ui_genericform.h"

namespace Ui {
class BillSeriesForm;
}

class BillSeriesForm : public GenericForm
{
    Q_OBJECT

public:
    explicit BillSeriesForm(QWidget *parent = 0, QString tableName = "SeriiFacturi");
    ~BillSeriesForm();

protected slots:
//    void on_tableView_doubleClicked(const QModelIndex &index);

private:

};

#endif // BILLSERIES_H
