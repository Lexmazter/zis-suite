#include "currencyinfo_form.h"
#include "ui_currencyinfo_form.h"

CurrencyInfo_Form::CurrencyInfo_Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CurrencyInfo_Form)
{
    ui->setupUi(this);
}

CurrencyInfo_Form::~CurrencyInfo_Form()
{
    delete ui;
}

void CurrencyInfo_Form::on_pushButton_clicked()
{
    currencyHelper = new CurrencyInfo_Helper(this);
    // We can parse the data only after we get it from the web
    connect(currencyHelper, SIGNAL(loadFinished(bool)),
            this, SLOT(currencyInfo_LoadFinished(bool)));
}

void CurrencyInfo_Form::currencyInfo_LoadFinished(bool state)
{
    switch(state)
    {
    // Company exists, no error
    case true:
    {
        QHash <QString, QString> *currencyRate = currencyHelper->getInfoCurrency();
        QHash <QString, QString>::Iterator iter;
        QString tempString="";
        for( iter = currencyRate->begin(); iter != currencyRate->end(); ++iter) {
            tempString += iter.key() + ": " +iter.value() +"\n";

        }
        ui->textEdit->setText(tempString);
        break;
    }
    default:
        break;
    }
}
