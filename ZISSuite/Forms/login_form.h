#ifndef LOGIN_FORM_H
#define LOGIN_FORM_H

#include <QWidget>
#include "mainwindow.h"
#include "dbhelper.h"
#include <QMessageBox>
#include <QString>

namespace Ui {
class login_form;
}

class login_form : public QWidget
{
    Q_OBJECT

public:
    explicit login_form(QWidget *parent = 0);
    ~login_form();

private slots:
    void on_login_button_clicked();

    void on_username_text_textChanged(const QString &arg1);

    void on_password_text_textChanged(const QString &arg1);

    void on_actionLogin_triggered();

private:
    Ui::login_form *ui;
};

#endif // LOGIN_FORM_H
