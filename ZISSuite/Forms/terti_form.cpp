#include "terti_form.h"
#include <ui_genericform.h>
#include <Delegates/comboboxdelegate.h>
#include <Delegates/booleandelegate.h>
#include <CustomObjects/combobox_helper.h>

terti_form::terti_form(QWidget *parent, QString tableName) :
    GenericForm(parent, tableName)
{
    // Beautification
    GenericForm::stretchColumnSize(2);

    for(int i=0; i <  ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        parentHeaderHash.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    if(parentHeaderHash.contains("TipTert"))
    {
        ComboBoxDelegate* comboDelegateForTert = new ComboBoxDelegate(ui->tableView, ComboBoxHelper::getComboboxMap(COMBOBOX_TERT));
        ui->tableView->setItemDelegateForColumn(parentHeaderHash.value("TipTert"), comboDelegateForTert);
    }
    if(parentHeaderHash.contains("Tara"))
    {
        ComboBoxDelegate* comboDelegateForCountry = new ComboBoxDelegate(ui->tableView, ComboBoxHelper::getComboboxMap(COMBOBOX_COUNTRY));
        ui->tableView->setItemDelegateForColumn(parentHeaderHash.value("Tara"), comboDelegateForCountry);
    }
    if(parentHeaderHash.contains("Judet"))
    {
        ComboBoxDelegate* comboDelegateForJudet = new ComboBoxDelegate(ui->tableView, ComboBoxHelper::getComboboxMap(COMBOBOX_JUDET));
        ui->tableView->setItemDelegateForColumn(parentHeaderHash.value("Judet"), comboDelegateForJudet);
    }
    if(parentHeaderHash.contains("PlatitorTVA"))
    {
        BooleanDelegate* boolDelegate = new BooleanDelegate(this);
        ui->tableView->setItemDelegateForColumn(parentHeaderHash.value("PlatitorTVA"), boolDelegate);
    }
    if(parentHeaderHash.contains("ImpozitProfit"))
    {
        BooleanDelegate* boolDelegate = new BooleanDelegate(this);
        ui->tableView->setItemDelegateForColumn(parentHeaderHash.value("ImpozitProfit"), boolDelegate);
    }

    codFiscalAction = new QAction(this);
    codFiscalAction->setIcon(QIcon(":/Icons/Business Contact-48.png"));
    codFiscalAction->setText("Preia date");
    ui->tableView->addAction(codFiscalAction);

    connect(codFiscalAction, SIGNAL(triggered()),
            this, SLOT(on_codFiscalAction_triggered()));
}

terti_form::~terti_form()
{

}

void terti_form::on_codFiscalAction_triggered()
{
    // Save the current index to be used when data is taken
    infoFirmaModelIndex = ui->tableView->model()->index(ui->tableView->currentIndex().row(), parentHeaderHash.value("CIF"));

    QString codFiscal = ui->tableView->model()->data(infoFirmaModelIndex).toString();

    // Get the data
    codFiscalHelper = new InfoCodFiscalHelper(this, codFiscal);

    // We can parse the data only after we get it from the web
    connect(codFiscalHelper, SIGNAL(loadFinished(bool)),
            this, SLOT(infoFiscalLoadFinished(bool)));

}

void terti_form::infoFiscalLoadFinished(bool state)
{
    switch(state)
    {
    // Company exists, no error
    case true:
    {
        QHash <QString, QString> tempInfoFirma = codFiscalHelper->getInfoFirma();

        // Set denumire
        QModelIndex tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("Denumire"));
        ui->tableView->model()->setData(tempIndex, tempInfoFirma.value("Denumire platitor:"));

        // Set telefon
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("Telefon"));
        ui->tableView->model()->setData(tempIndex, tempInfoFirma.value("Telefon:"));

        // Set tara - only RO for this feature
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("Tara"));
        ui->tableView->model()->setData(tempIndex, "ro");

        // Set judet
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("Judet"));
        ui->tableView->model()->setData(tempIndex, tempInfoFirma.value("Judetul:").toLower());

        // Set adresa
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("Adresa"));
        ui->tableView->model()->setData(tempIndex, tempInfoFirma.value("Adresa:"));

        // Set RegComert
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("RegComert"));
        ui->tableView->model()->setData(tempIndex, tempInfoFirma.value("Numar de inmatriculare la RegistrulComertului:"));

        // TODO - Set Localitate

        //process the activity status and show it
        QString tempString =tempInfoFirma.value("Stare societate:");
        if(tempString.contains("INREGISTRAT")){
            tempString.replace("INREGISTRAT", "ACTIV");
        }
        else if(tempString.contains("DIZOLVARE CU LICHIDARE(RADIERE)"))
        {
            tempString.replace("DIZOLVARE CU LICHIDARE(RADIERE)", "INACTIV");
        }
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("StareSocietate"));
        ui->tableView->model()->setData(tempIndex, tempString);

        //process if the third is paying VAT
        tempString =tempInfoFirma.value("Taxa pe valoarea adaugata (data luarii in evidenta):");
        if(tempString.isEmpty() || tempString.contains("NU") || tempString==("-"))
            tempString = "0";
        else
            tempString = "1";
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("PlatitorTVA"));
        ui->tableView->model()->setData(tempIndex, tempString.toInt());

         //process if the third is paying contribution
        tempString =tempInfoFirma.value("Impozit pe profit (data luarii in evidenta):");
        if(tempString.isEmpty() || tempString.contains("NU") || tempString==("-"))
            tempString = "0";
        else
            tempString = "1";
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("ImpozitProfit"));
        ui->tableView->model()->setData(tempIndex, tempString.toInt());

        // Set tip tert - Client as default
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("TipTert"));
        ui->tableView->model()->setData(tempIndex, "Client");

        // Set discount - 0 as default
        tempIndex = ui->tableView->model()->index(infoFirmaModelIndex.row(), parentHeaderHash.value("Discount"));
        ui->tableView->model()->setData(tempIndex, 0);

        break;
    }
        // Company does not exist, or some error happened
    default:
        break;
    }

    // Delete the object at the end
    //delete codFiscalHelper;
}
