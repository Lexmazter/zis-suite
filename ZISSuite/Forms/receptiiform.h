#ifndef RECEPTIIFORM_H
#define RECEPTIIFORM_H

#include "genericform.h"
#include "ui_genericform.h"

namespace Ui {
class ReceptiiForm;
}

class ReceptiiForm : public GenericForm
{
    Q_OBJECT

public:
    explicit ReceptiiForm(QWidget *parent = 0, QString tableName = "Receptii");
    ~ReceptiiForm();

public slots:

private slots:
    void on_actionAdd_triggered();
    void on_actionRefresh_triggered();
    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    int tertIndex = 0;
};

#endif // RECEPTIIFORM_H
