#ifndef CAPTCHAADAPTER_FORM_H
#define CAPTCHAADAPTER_FORM_H

#include <QWidget>
#include <QtWebEngineWidgets>
#include <QMovie>

namespace Ui {
class CaptchaAdapter_Form;
}

class CaptchaAdapter_Form : public QWidget
{
    Q_OBJECT

public:
    explicit CaptchaAdapter_Form(QWidget *parent = 0);
    ~CaptchaAdapter_Form();

signals:
    void loadFinished(bool state);

private slots:
    void on_pushButton_clicked();
    void pageLoadFinished(bool);
    void slot_netwManagerFinished(QNetworkReply *reply);
    void loadListToMap(QStringList list);

private:
    Ui::CaptchaAdapter_Form *ui;
    QWebEnginePage* tempPage;
    QUrl FirmActivityLink;
    QMovie movie;
    QImage * image;
    QPixmap screenImage;
    QPainter * p;
    QBuffer *buffer;
    QByteArray bytes;
    int Request;
    QHash <QString, QString> QueryRezult_Map;
    QString FirmID;

};

#endif // CAPTCHAADAPTER_FORM_H
