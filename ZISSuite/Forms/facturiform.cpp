#include "Forms/facturiform.h"
#include "Dialogs/facturiproduse_dialog.h"
#include "Delegates/booleandelegate.h"

FacturiForm::FacturiForm(QWidget *parent, QString tableName) :
    GenericForm(parent, tableName)
{
    // Beautification
    GenericForm::stretchColumnSize(1);
    GenericForm::stretchColumnSize(3);

    // Get all the headers so we know where to put what
    QMap<QString, int> headerMap;
    for(int i=0; i < ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        headerMap.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }



    // Hide products and quantity
    if(headerMap.contains("Produse"))
        ui->tableView->setColumnHidden(headerMap.value("Produse"), true);
    if(headerMap.contains("Cantitate"))
        ui->tableView->setColumnHidden(headerMap.value("Cantitate"), true);

    tertIndex = headerMap.find("idTert").value();
    // Set the relation first
    CustomSqlTable* tempTable = dynamic_cast<CustomSqlTable*>(ui->tableView->model());
    tempTable->setRelation(tertIndex, QSqlRelation("Terti", "ID", "Denumire as Tert"));
    tempTable->select();

    // Now we set the delegate
    ui->tableView->setItemDelegateForColumn(tertIndex, new QSqlRelationalDelegate(ui->tableView));

    seriesIndex = headerMap.find("Serie").value();
    // Set the relation first
    tempTable = dynamic_cast<CustomSqlTable*>(ui->tableView->model());
    tempTable->setRelation(seriesIndex, QSqlRelation("SeriiFacturi", "ID", "Serie as Serie"));
    tempTable->select();

    // Now we set the delegate
    ui->tableView->setItemDelegateForColumn(seriesIndex, new QSqlRelationalDelegate(ui->tableView));

    BooleanDelegate* boolDelegate = new BooleanDelegate(this);

    if(headerMap.contains("Achitat"))
        ui->tableView->setItemDelegateForColumn(headerMap.value("Achitat"),boolDelegate);

    //add context menu actions to the parent form action list
    setContextMenuActionsForTable();

}

FacturiForm::~FacturiForm()
{

}



void FacturiForm::setContextMenuActionsForTable()
{
    QAction *printBillAction = new QAction(QIcon(":/Icons/Print-48.png"), "Scoate Factura", this);
    ui->tableView->insertAction(ui->actionAdd, printBillAction);
    QMenu* subMenuPrintBill = new QMenu("Scoate Factura");
    QAction *actionReceipt = new QAction(QIcon(":/Icons/Paid-48.png"), "Chitanta", this);
    QAction *actionProformaInvoice = new QAction(QIcon(":/Icons/No Cash-48.png"), "Factura Proforma", this);
    QAction *actionPrintBill = new QAction(QIcon(":/Icons/Purchase Order-48.png"), "Factura Normala", this);
    subMenuPrintBill->insertAction(NULL, actionReceipt);
    subMenuPrintBill->insertAction(actionReceipt, actionProformaInvoice);
    subMenuPrintBill->insertAction(actionProformaInvoice, actionPrintBill);
    printBillAction->setMenu(subMenuPrintBill);
    connect(actionReceipt,SIGNAL(triggered()), this,SLOT(on_actionReceipt_triggered()));
    connect(actionProformaInvoice,SIGNAL(triggered()), this,SLOT(on_actionProformaInvoice_triggered()));
    connect(actionPrintBill,SIGNAL(triggered()), this,SLOT(on_actionPrintBill_triggered()));
}


void FacturiForm::on_actionReceipt_triggered()
{
    foreach (QModelIndex index, ui->tableView->selectionModel()->selectedIndexes())
    {
        int actual_raw = index.row();

        FacturiProduseDialog* produse_dialog= new FacturiProduseDialog(this, ui->tableView, actual_raw);
        produse_dialog->on_actionReceipt_triggered();
        delete produse_dialog;
    }
}

void FacturiForm::on_actionProformaInvoice_triggered()
{
    foreach (QModelIndex index, ui->tableView->selectionModel()->selectedIndexes())
    {
        int actual_raw = index.row();

        FacturiProduseDialog* produse_dialog= new FacturiProduseDialog(this, ui->tableView, actual_raw);
        produse_dialog->on_actionProformaInvoice_triggered();
        delete produse_dialog;
    }
}

void FacturiForm::on_actionPrintBill_triggered()
{
    foreach (QModelIndex index, ui->tableView->selectionModel()->selectedIndexes())
    {
        int actual_raw = index.row();

        FacturiProduseDialog* produse_dialog= new FacturiProduseDialog(this, ui->tableView, actual_raw);
        produse_dialog->on_actionPrintBill_triggered();
        delete produse_dialog;
    }
}

void FacturiForm::on_tableView_doubleClicked(const QModelIndex &index)
{
    Q_UNUSED(index);

    int actual_raw = index.row();

    FacturiProduseDialog* produse_dialog= new FacturiProduseDialog(this, ui->tableView, actual_raw);
    produse_dialog->show();
}

void FacturiForm::on_actionRefresh_triggered()
{
    GenericForm::on_actionRefresh_triggered();

    // Set the relation again
    CustomSqlTable* tempTable = dynamic_cast<CustomSqlTable*>(ui->tableView->model());
    tempTable->setRelation(tertIndex, QSqlRelation("Terti", "ID", "Denumire as Tert"));
    tempTable->setRelation(seriesIndex, QSqlRelation("SeriiFacturi", "ID", "Serie as Serie"));
    tempTable->select();
}

void FacturiForm::on_actionAdd_triggered()
{
    GenericForm::on_actionAdd_triggered();
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
}
