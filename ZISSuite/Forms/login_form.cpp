#include "login_form.h"
#include "ui_login_form.h"
#include <QToolTip>

login_form::login_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::login_form)
{
    ui->setupUi(this);

    this->addAction(ui->actionLogin);
}

login_form::~login_form()
{
    delete ui;
}

/**
 * @brief login_form::on_login_button_clicked
 */
void login_form::on_login_button_clicked()
{
    int LoginRetVal = DbHelper::Login(ui->username_text->text(), ui->password_text->text());

    switch(LoginRetVal)
    {
        case LOGIN_SUCCESSFUL:
        {
            MainWindow* w = MainWindow::getInstance();
            w->setCurrentUser(ui->username_text->text());
            w->show();
            this->close();
            break;
        }


        case USERNAME_FAILED:
        {
            // Prepare a messagebox
            QMessageBox msgBox(this);
            QPixmap icon(":/Icons/Gender Neutral User-48.png");
            msgBox.setIconPixmap(icon);
            msgBox.setText(tr("Utilizatorul nu exista!"));
            msgBox.setInformativeText(tr("Pentru a continua se recomanda contactarea departamentului de support."));
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();
            break;
        }

        case PASSWORD_FAILED:
        {
            // Prepare a messagebox
            QMessageBox msgBox(this);
            QPixmap icon(":/Icons/Password-48.png");
            msgBox.setIconPixmap(icon);
            msgBox.setText(tr("Parola nu este corectă!"));
            msgBox.setInformativeText(tr("Pentru a continua se recomandă contactarea departamentului de support."));
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();
            break;
        }

        case DB_NOT_FOUND:
        {
            // Prepare a messagebox
            QMessageBox msgBox(this);
            QPixmap icon(":/Icons/Database-48.png");
            msgBox.setIconPixmap(icon);
            msgBox.setText(tr("Nu se poate conecta la baza de date!"));
            msgBox.setInformativeText(tr("Pentru a continua se recomandă contactarea departamentului de support."));
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();
            break;
        }
        default:
        {
            // Prepare a messagebox
            QMessageBox msgBox(this);
            QPixmap icon(":/Icons/Error-48.png");
            msgBox.setIconPixmap(icon);
            msgBox.setText(tr("Eroare necunoscută!"));
            msgBox.setInformativeText(tr("Pentru a continua se recomandă contactarea departamentului de support."));
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();
            break;
        }
    }
}

/**
 * @brief login_form::on_username_text_textChanged - this function is used to avoid special characters in the username field.
 * @param arg1
 */
void login_form::on_username_text_textChanged(const QString &arg1)
{
    // protect against SQL injection - no special character allowed
    if (arg1.contains(QRegularExpression("[^A-Za-z0-9]")))
    {
        QString removedSpecial = ui->username_text->text().remove(QRegularExpression("[^A-Za-z0-9]"));
        ui->username_text->setText(removedSpecial);
        QToolTip::showText(ui->username_text->mapToGlobal(QPoint()), tr("Nu se acceptă caractere speciale."));
    }
}

/**
 * @brief login_form::on_password_text_textChanged - this function is used to avoid special characters in the password field.
 * @param arg1
 */
void login_form::on_password_text_textChanged(const QString &arg1)
{
    // protect against SQL injection - no special character allowed
    if (arg1.contains(QRegularExpression("[^A-Za-z0-9]")))
    {
        QString removedSpecial = ui->password_text->text().remove(QRegularExpression("[^A-Za-z0-9]"));
        ui->password_text->setText(removedSpecial);
        QToolTip::showText(ui->password_text->mapToGlobal(QPoint()), tr("Nu se acceptă caractere speciale."));
    }
}

void login_form::on_actionLogin_triggered()
{
    on_login_button_clicked();
}
