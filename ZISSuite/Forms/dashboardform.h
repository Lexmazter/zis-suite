#ifndef DASHBOARDFORM_H
#define DASHBOARDFORM_H

#include <QWidget>
#include <chartswidget.h>
#include <QToolButton>

namespace Ui {
class DashboardForm;
}

class DashboardForm : public QWidget
{
    Q_OBJECT

public:
    explicit DashboardForm(QWidget *parent = 0);
    ~DashboardForm();

    void loadData();
private slots:
    void on_logoButton_clicked();

    void on_facturiButton_clicked();

    void on_intrariButton_clicked();

private:
    Ui::DashboardForm *ui;
    ChartsWidget* chartMonthly;
    ChartsWidget* chartMonthlyPerDay;
    ChartsWidget* chartYearly;
    QString getTotalReceptiiThisMonth();
    QString getTotalVanzariThisMonth();
    QString *getTotalThisYear(QString table);
    QString *getTotalPerDayThisMonth(QString table);
    void setNotificationIcon(QToolButton *source, QString number);
    QString getTotalScadenteUntilToday(QString table);
    void getCompanyLogoAndName(QString companyId);
};

#endif // DASHBOARDFORM_H
