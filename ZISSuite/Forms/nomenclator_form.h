#ifndef NOMENCLATOR_FORM_H
#define NOMENCLATOR_FORM_H

#include "genericform.h"

namespace Ui {
class nomenclator_form;
}

class nomenclator_form : public GenericForm
{
    Q_OBJECT

public:
    explicit nomenclator_form(QWidget *parent = 0, QString tableName = "Nomenclator");
    ~nomenclator_form();

private:
};

#endif // NOMENCLATOR_FORM_H
