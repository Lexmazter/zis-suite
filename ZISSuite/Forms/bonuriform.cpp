#include "bonuriform.h"
#include "Dialogs/bonuriproduse_dialog.h"

BonuriForm::BonuriForm(QWidget *parent, QString tableName) :
    GenericForm(parent, tableName)
{
    // Beautification
    GenericForm::stretchColumnSize(1);
    GenericForm::stretchColumnSize(2);

    // Get all the headers so we know where to put what
    QMap<QString, int> headerMap;
    for(int i=0; i < ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        headerMap.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    // Hide produse and cantitate
    if(headerMap.contains("Produse"))
        ui->tableView->setColumnHidden(headerMap.value("Produse"), true);
    if(headerMap.contains("Cantitate"))
        ui->tableView->setColumnHidden(headerMap.value("Cantitate"), true);
}

BonuriForm::~BonuriForm()
{

}

void BonuriForm::on_tableView_doubleClicked(const QModelIndex &index)
{
    int actual_raw = index.row();

    BonuriProduse_Dialog* bonuri_dialog= new BonuriProduse_Dialog(this, GenericForm::ui->tableView, actual_raw);
    bonuri_dialog->show();
}

void BonuriForm::on_actionAdd_triggered()
{
    GenericForm::on_actionAdd_triggered();
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
}
