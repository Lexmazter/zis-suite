#ifndef BANKACCOUNTSFORM_H
#define BANKACCOUNTSFORM_H

#include <Forms/genericform.h>
#include "ui_genericform.h"

namespace Ui {
class BankAccountsForm;
}

class BankAccountsForm : public GenericForm
{
    Q_OBJECT

public:
    explicit BankAccountsForm(QWidget *parent = 0, QString tableName = "ConturiBancare");
    ~BankAccountsForm();

private slots:

private:

};

#endif // BANKACCOUNTSFORM_H
