#include "Forms/bankaccountsform.h"
#include "Delegates/booleandelegate.h"

BankAccountsForm::BankAccountsForm(QWidget *parent, QString tableName) :
    GenericForm(parent, tableName)
{
    // Beautification
    GenericForm::stretchColumnSize(1);
    GenericForm::stretchColumnSize(2);

    // Get all the headers so we know where to put what
    QMap<QString, int> headerMap;
    for(int i=0; i < ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        headerMap.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    if(headerMap.contains("Folosit"))
    {
        BooleanDelegate* boolDelegate = new BooleanDelegate(this);
        ui->tableView->setItemDelegateForColumn(headerMap.value("Folosit"), boolDelegate);
    }
}

BankAccountsForm::~BankAccountsForm()
{

}
