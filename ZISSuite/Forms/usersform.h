#ifndef USERSFORM_H
#define USERSFORM_H

#include <Forms/genericform.h>
#include <ui_genericform.h>
#include <Delegates/passworddelegate.h>

namespace Ui {
class UsersForm;
}

class UsersForm : public GenericForm
{
    Q_OBJECT

public:
    explicit UsersForm(QWidget *parent = 0, QString tableName = "Users");
    ~UsersForm();

private slots:
    void on_actionSave_triggered();

private:
    int passwordIndex;
    int password2Index;
    PasswordDelegate *passDelegate;
};

#endif // USERSFORM_H
