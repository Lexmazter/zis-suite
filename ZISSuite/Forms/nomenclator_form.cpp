#include "nomenclator_form.h"
#include "ui_genericform.h"

nomenclator_form::nomenclator_form(QWidget *parent, QString tableName) :
    GenericForm(parent, tableName)
{
    // Beautification
    GenericForm::stretchColumnSize(1);
}

nomenclator_form::~nomenclator_form()
{

}
