#ifndef CASHREGISTER_FORM_H
#define CASHREGISTER_FORM_H

#include <QWidget>

namespace Ui {
class CashRegister_Form;
}

class CashRegister_Form : public QWidget
{
    Q_OBJECT

public:
    explicit CashRegister_Form(QWidget *parent = 0);
    ~CashRegister_Form();

private slots:
    void on_toolButton_Receipt_clicked();

    void on_toolButton_Other_In_clicked();

    void on_toolButton_Bill_Out_clicked();

    void on_toolButton_Ticket_Out_clicked();

    void on_toolButton_Other_Out_clicked();

private:
    Ui::CashRegister_Form *ui;
};

#endif // CASHREGISTER_FORM_H
