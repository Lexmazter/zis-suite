#include "custommodel.h"


CustomModel::CustomModel(QObject *parent, QAbstractItemModel *defaultModel)
    :QAbstractTableModel(parent)
{
    ROWS = defaultModel->rowCount();
    COLS = defaultModel->columnCount();
    for(int i=0; i<ROWS; i++)
    {
        QVector<QString> col_data; //create a QVector of QStrings
        for(int j=0; j<COLS; j++)
        {
            col_data.push_back(defaultModel->data(defaultModel->index(i, j)).toString());
        }
        m_gridData.push_back(col_data);//add the created vector as a line in your 2D vector
    }
    //remove duplicated products
    //    for(int i=0; i<ROWS; i++)
    //    {
    //        int occurrences_count = 1;
    //        for(int dup=i+1; dup<ROWS; dup++)
    //        {
    //            if(m_gridData[i][0] == m_gridData[dup][0])
    //            {
    //                occurrences_count++;
    //                m_gridData.remove(dup);
    //                ROWS--;
    //            }
    //        }
    //        m_gridData[i][COLS-1] = QString::number(occurrences_count);
    //    }

}

//-----------------------------------------------------------------
int CustomModel::rowCount(const QModelIndex & /*parent*/) const
{
    return ROWS;
}

//-----------------------------------------------------------------
int CustomModel::columnCount(const QModelIndex & /*parent*/) const
{
    return COLS;
}

//-----------------------------------------------------------------
QVariant CustomModel::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
    case Qt::EditRole:
    case Qt::DisplayRole:
        return m_gridData[index.row()][index.column()];
        break;

    default:
        return QVariant();
        break;
    }
}

//-----------------------------------------------------------------
bool CustomModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole)
    {
        //save value from editor to member m_gridData
        m_gridData[index.row()][index.column()] = value.toString();
        //for presentation purposes only: build and emit a joined string
        QString result;
        for (int row= 0; row < ROWS; row++)
        {
            for(int col= 0; col < COLS; col++)
            {
                result += m_gridData[row][col] + ' ';
            }
        }
        emit editCompleted( result );

        // emit dataChanged so we can notify the others
        QModelIndex dummy;
        emit dataChanged(dummy, dummy);
    }
    return true;
}

//-----------------------------------------------------------------
//bool MyModel::insertRow(int arow, const QModelIndex &aparent)
bool CustomModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if (count < 1 ||  row < -1)
    {
        return false;
    }
    else if(row == -1)
    {
        //the model is empty - make it possible to add the first element
        row = 0;
    }

    beginInsertRows(QModelIndex(), row, row + count - 1);
    //save value from editor to member m_gridData
    QVector<QString> newRow;
    for(int j=0; j<COLS; j++)
    {
        newRow.push_back("");
    }
    m_gridData.insert(row, newRow);
    ROWS++;
    endInsertRows();

    return true;
}

bool CustomModel::removeRow(int row, const QModelIndex &parent)
{
    if (row < -1)
    {
        return false;
    }
    if(ROWS == 0)
    {
        return false;
    }

    beginRemoveRows(QModelIndex(), row, row);
    m_gridData.remove(row);
    ROWS--;
    endRemoveRows();

    return true;
}

//-----------------------------------------------------------------
Qt::ItemFlags CustomModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

/*
 *  Description: Overridden method insertColumns
 * */
bool CustomModel::insertColumns(int column, int count, const QModelIndex &parent,  QStringList *columnData)
{
    if (count < 1 || column < 0 )
        return false;

    beginInsertColumns(QModelIndex(), column, column + count - 1);
    for(int j=0; j<ROWS; j++)
    {
        if((columnData != nullptr) && (columnData->length() == ROWS))
        {
            m_gridData[j].insert(column, columnData->value(j));
        }
        else
        {
            m_gridData[j].insert(column, "");
        }
    }
    COLS++;
    endInsertColumns();

    return true;
}

/*
 * Description: insert aditionall column with the data from string param split by ", "
 */
char CustomModel::insertAdditionalColumn(QString columnName, QString *columnData){


    //create new column representing the amout of items
    QStringList items = columnData->split(", ");
    int items_length = items.length();
    if(items_length<1)
        return OperationNotPossible;
    else if((ROWS >0) && (items_length != ROWS))
        return DifferentSize;

    const QModelIndex &parent = QModelIndex();
    insertColumns(COLS, 1,parent, &items);
    this->setHeaderData(COLS, Qt::Horizontal, &columnName);

    //    else if((columnData != nullptr) && (*columnData != ""))
    //    {
    //        //only one element in the data string - create new column representing the amout of items
    //        COLS++;

    //    }
}
/*
 * Description: set the header data for custom model
 */
bool CustomModel::setHeaderData(int section, Qt::Orientation orientation, QString *value, int role){
    Q_UNUSED(section);
    Q_UNUSED(role);
    headerList.append(value->split(", "));
    if(headerList.length() < 0)
        return false;
    for(int i=0; i < headerList.length(); i++)
    {
        //        this->setHeaderData(i, orientation, headerList->value(i), role);
        emit headerDataChanged(orientation, 0, headerList.length());
    }
    return true;
}

QVariant CustomModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
    {
        QString headerData;
        if(orientation == Qt::Horizontal && (headerList.length() >= section))
        {
            headerData  = headerList.value(section);
            return headerData;
        }
        else if(orientation == Qt::Vertical)
        {
            return QString::number(section);
        }
    }
    return QVariant::Invalid;
}
