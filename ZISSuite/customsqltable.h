#ifndef CUSTOMSQLTABLE_H
#define CUSTOMSQLTABLE_H

#include <QSqlRelationalTableModel>
#include <Delegates/colordelegate.h>

class CustomSqlTable : public QSqlRelationalTableModel
{
public:
    CustomSqlTable(QObject * parent, QSqlDatabase db, ColorDelegate* delegate = nullptr);
    ~CustomSqlTable();

    Qt::ItemFlags flags ( const QModelIndex & index ) const
    {
        if (index.column() != 0)
            return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
        else
            return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }

    QVariant data(const QModelIndex &index, int role) const;
private slots:

private:
    ColorDelegate* colorDelegate = nullptr;
};

#endif // CUSTOMSQLTABLE_H
