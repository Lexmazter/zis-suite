#ifndef NUMBERSPEL_HELPER_H
#define NUMBERSPEL_HELPER_H
#include <Qmap>


class NumberSpeller_Helper
{
public:
    NumberSpeller_Helper();
    QString numberToText(uint number);
private:
    QMap<uint, QString> numbers;
    QMap<uint, QString> powers;
};

#endif // NUMBERSPEL_HELPER_H
