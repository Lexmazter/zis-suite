#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ExportDialogPages/export_dialogbox.h"
#include "Dialogs/firmdetails_dialog.h"
#include "Forms/dashboardform.h"
#include "Forms/captchaadapter_form.h"
#include <QCloseEvent>
#include <QPrintDialog>
#include <QPrinter>
#include <QGraphicsOpacityEffect>
#include <QtWebEngine/qtwebengineglobal.h>
#include <Dialogs/importdialog.h>
#include <ExportDialogPages/tableprinter.h>
#include "Forms/currencyinfo_form.h"
#include "Dialogs/configdocuments_dialog.h"
#include "Forms/stocksyncform.h"

MainWindow* MainWindow::uniqueInstance = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    createCustomActions();
    createCustomMenus();
    createCustomToolButtons();
    QtWebEngine::initialize();

    // Set our version in the status bar
    ui->statusBar->addPermanentWidget(new QLabel(QString(tr("Versiune: %1.%2.%3")).arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(VERSION_PATCH)));

    // Assign the memory only once to avoid leaks
    tabAnimation = new QPropertyAnimation(ui->tab_frame, "maximumHeight");

    // Show the dashboard upon starting
    on_toolButton_dashboard_clicked();

#ifndef QT_DEBUG
    // Do not show development tab in release mode
    delete ui->tabDevelopment;
#endif

}

MainWindow::~MainWindow()
{
    delete ui;
}

//singleton design pattern for mainwindow instance
MainWindow* MainWindow::getInstance()
{
    if (0 == MainWindow::uniqueInstance){
        MainWindow::uniqueInstance = new MainWindow();
    }
    return MainWindow::uniqueInstance;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox msgBox(this);
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setText(tr("Ieșire ZIS Suite"));
    msgBox.setInformativeText(tr("Ești sigur că vrei să închizi aplicația?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    msgBox.setButtonText(QMessageBox::Yes, trUtf8("Da"));
    msgBox.setButtonText(QMessageBox::No, trUtf8("Nu"));
    msgBox.setButtonText(QMessageBox::Cancel, trUtf8("Anulare"));

    if (msgBox.exec() != QMessageBox::Yes)
    {
        event->ignore();
    } else
    {
        event->accept();
    }
}

/**
 * @brief MainWindow::AddWidgetToMain - Function that handles a new widget to be added to the main window
 * and deletes the previous one if it exists to avoid memory leak.
 * @param toBeAdded - the Widget that needs to be added
 */
void MainWindow::AddWidgetToMain(QWidget* toBeAdded, const QIcon icon, const QString label)
{
    bool present = false;

    // Check if widget is already present and focus on it
    for(int i=0; i < ui->tabWidget_Open->count(); i++)
    {
        if(label == ui->tabWidget_Open->tabText(i))
        {
            delete toBeAdded;
            present = true;
            ui->tabWidget_Open->setCurrentIndex(i);
        }
    }

    if(present == false)
    {
        ui->tabWidget_Open->insertTab(ui->tabWidget_Open->count(), toBeAdded, icon, label);
        // Select the latest added one
        ui->tabWidget_Open->setCurrentIndex(ui->tabWidget_Open->count()-1);
    }
}

void MainWindow::on_tabWidget_Open_tabCloseRequested(int index)
{
    QWidget* toBeDeleted = (QWidget*)ui->tabWidget_Open->widget(index);
    ui->tabWidget_Open->removeTab(index);
    delete toBeDeleted;
}

void MainWindow::on_button_about_clicked()
{
    About_Dialog* about_dialog = new About_Dialog(this);
    about_dialog->exec();
}

void MainWindow::on_button_nomenclator_clicked()
{
    QString tabText = "Nomenclator";
    QPixmap tabPix;
    tabPix.load(":/Icons/Product-48.png");

    nomenclator_form* nomenclator = new nomenclator_form(this);
    AddWidgetToMain(nomenclator, tabPix, tabText);
}

/**
 * @brief MainWindow::on_expand_collapse_button_clicked - this function is used to expand or collapse the ribbon view with animation.
 */
void MainWindow::on_expand_collapse_button_clicked()
{
    // collapse the tab with animation
    if(ui->tab_frame->maximumHeight() != 0)
    {
        tabAnimation->setDuration(200);
        tabAnimation->setStartValue(110);
        tabAnimation->setEndValue(0);

        tabAnimation->start();

        QIcon expand_icon(":/Icons/Expand Arrow-48.png");
        ui->expand_collapse_button->setIcon(expand_icon);
    }
    // expand the tab with animation
    else
    {
        tabAnimation->setDuration(200);
        tabAnimation->setStartValue(0);
        tabAnimation->setEndValue(110);

        tabAnimation->start();

        QIcon collapse_icon(":/Icons/Collapse Arrow-48.png");
        ui->expand_collapse_button->setIcon(collapse_icon);
    }
}

void MainWindow::createCustomActions()
{
    actionChoseFacturi = new QAction("Facturi", this);
    actionChoseBonuri  = new QAction("Bonuri", this);

    actionChoseFacturi->setIcon(QIcon(":/Icons/Vanzari-50.png"));
    actionChoseBonuri ->setIcon(QIcon(":/Icons/Bill-48.png"));

    QObject::connect(actionChoseFacturi, SIGNAL(triggered()),
                     this, SLOT(slotChoseFacturi()));
    QObject::connect(actionChoseBonuri, SIGNAL(triggered()),
                     this, SLOT(slotChoseBonuri()));
}

void MainWindow::createCustomMenus()
{
    choseCustomMenu = new QMenu;
    choseCustomMenu->addAction(actionChoseBonuri);
    choseCustomMenu->addAction(actionChoseFacturi);
    choseCustomMenu->setVisible(true);
}

void MainWindow::createCustomToolButtons()
{
    ui->Iesiri_ToolButton->setMenu(choseCustomMenu);
    ui->Iesiri_ToolButton->addAction(actionChoseFacturi);
}

QTableView* MainWindow::getCurrentTable() const
{
    GenericForm* tempForm =  dynamic_cast<GenericForm*>(ui->tabWidget_Open->currentWidget());

    if(tempForm)
        return tempForm->getTableInstance();
    else
        return nullptr;
}

void MainWindow::slotChoseFacturi()
{
    QString tabText = "Facturi";
    QPixmap tabPix;
    tabPix.load(":/Icons/Vanzari-50.png");

    FacturiForm* vanzari_form = new FacturiForm(this);
    AddWidgetToMain(vanzari_form, tabPix, tabText);
}

void MainWindow::slotChoseBonuri()
{
    QString tabText = "Bonuri";
    QPixmap tabPix;
    tabPix.load(":/Icons/Bill-48.png");

    BonuriForm* bonuri_form = new BonuriForm(this);
    AddWidgetToMain(bonuri_form, tabPix, tabText);
}

void MainWindow::on_toolButton_clicked()
{
    QString tabText = "Terti";
    QPixmap tabPix;
    tabPix.load(":/Icons/Business Contact-48.png");

    terti_form* terti = new terti_form(this);
    AddWidgetToMain(terti, tabPix, tabText);
}

void MainWindow::on_toolButton_ExportFile_clicked()
{
    Export_DialogBox dialog(this);
    dialog.exec();
}

void MainWindow::on_toolButton_ImportFile_clicked()
{
    QTableView* tempTable = getCurrentTable();

    if(tempTable)
    {
        QString filePath = QFileDialog::getOpenFileName(this, tr("Open xlsx file"), QString(), "*.xlsx");
        if (filePath.isEmpty())
            return;

        ImportDialog* dialog = new ImportDialog(this, filePath);
        dialog->exec();
    }
    else
    {
        QMessageBox::warning(this, tr("Avertizare"), tr("Niciun tabel nu este activ !"));
    }
}

QString  MainWindow::getStatusText()
{
    return ui->tabWidget_Open->tabText(ui->tabWidget_Open->currentIndex());
}

void MainWindow::on_toolButton_PrintFile_clicked()
{
    MainWindow *mainWindow = MainWindow::getInstance();
    QTableView *currentTable = mainWindow->getCurrentTable();

    if(currentTable != nullptr)
    {
        QPrinter printer;
        QPrintDialog *dialog = new QPrintDialog(&printer, this);
        dialog->setWindowTitle(tr("Print Document"));
        if (dialog->exec() != QDialog::Accepted)
            return;

        QPainter painter(&printer);
        painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
        printer.setOrientation(QPrinter::Landscape);

        TablePrinter tablePrinter(&painter, &printer);

        QFont headerFont( "Tokyo" );
        headerFont.setPointSize( 10 );
        headerFont.setWeight( QFont::Bold );
        tablePrinter.setHeadersFont(headerFont);
        QVector<int> columnStretch = QVector<int>();

        for(int i=0; i< currentTable->model()->columnCount(); i++)
        {
            //default values for column stretch
            columnStretch.insert(i, 1);
        }

        QVector<QString> headerVector = QVector<QString>();

        for(int i=0; i < currentTable->horizontalHeader()->model()->columnCount(); i++)
        {
            headerVector.insert(i, currentTable->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString());
        }

        if(! tablePrinter.printTable(currentTable->model(), columnStretch, headerVector))
        {
            qDebug() << tablePrinter.lastError();
        }

        painter.end();
    }
    else
    {
        QMessageBox::warning(this, tr("Avertizare"), tr("Niciun tabel nu este activ !"));
    }
}

void MainWindow::on_toolButtonIntrari_clicked()
{
    QString tabText = "Receptii";
    QPixmap tabPix;
    tabPix.load(":/Icons/Receptii-48.png");

    ReceptiiForm* receptii_form = new ReceptiiForm(this);
    AddWidgetToMain(receptii_form, tabPix, tabText);
}

void MainWindow::on_button_help_clicked()
{
    // Help framework needs to be developed

}

void MainWindow::on_toolButtonCodBare_clicked()
{
    QString tabText = "Coduri Bare";
    QPixmap tabPix;
    tabPix.load(":/Icons/Barcode-48.png");

    codBareHelper* codbare = new codBareHelper(this);
    AddWidgetToMain(codbare, tabPix, tabText);
}

void MainWindow::on_toolButtonGrafice_clicked()
{
    QString tabText = "Grafice";
    QPixmap tabPix;
    tabPix.load(":/Icons/Ratings-48.png");

    chartsForm* charts = new chartsForm(this);
    AddWidgetToMain(charts, tabPix, tabText);
}

void MainWindow::on_toolButton_Users_clicked()
{
    QString tabText = "Utilizatori";
    QPixmap tabPix;
    tabPix.load(":/Icons/Gender Neutral User-48.png");

    UsersForm* users = new UsersForm(this);
    AddWidgetToMain(users, tabPix, tabText);
}

void MainWindow::on_toolButton_firm_clicked()
{
    FirmDetails_Dialog *p_FirmInfo_Form = new FirmDetails_Dialog(this);
    p_FirmInfo_Form->exec();
}

void MainWindow::on_toolButton_dashboard_clicked()
{
    // Will be opened by default when the program starts

    QString tabText = "Tablou de bord";
    QPixmap tabPix;
    tabPix.load(":/Icons/Dashboard-48.png");

    DashboardForm* dashboard = new DashboardForm(this);
    AddWidgetToMain(dashboard, tabPix, tabText);
}

void MainWindow::on_toolButton_Captcha_clicked()
{
    QString tabText = "Verificare firma";
    QPixmap tabPix;
    tabPix.load(":/Icons/CAPTCHA-48.png");

    CaptchaAdapter_Form *captchaAdapter = new CaptchaAdapter_Form(this);
    AddWidgetToMain(captchaAdapter, tabPix, tabText);
}

void MainWindow::on_tabWidget_Open_currentChanged(int index)
{
    DashboardForm* dashboard = dynamic_cast<DashboardForm*>(ui->tabWidget_Open->currentWidget());

    if(dashboard)
        dashboard->loadData();
}

void MainWindow::on_toolButton_CursBNR_clicked()
{
    QString tabText = "Curs Curent BNR";
    QPixmap tabPix;
    tabPix.load(":/Icons/Currency Exchange-48.png");

    CurrencyInfo_Form* currencyInfoForm = new CurrencyInfo_Form(this);
    AddWidgetToMain(currencyInfoForm, tabPix, tabText);
}

void MainWindow::on_toolButton_EmitDocuments_clicked()
{
    ConfigDocuments_Dialog *p_ConfigDocuments_Dialog = new ConfigDocuments_Dialog(this);
    p_ConfigDocuments_Dialog->exec();
}

void MainWindow::on_toolButton_2_clicked()
{
    StockSyncForm stockSync;
    stockSync.show();
}
