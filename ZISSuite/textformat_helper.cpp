#include "textformat_helper.h"
#include <QMessageBox>

TextFormat_Helper::TextFormat_Helper()
{

}

void TextFormat_Helper::setLineAsHTMLMsg(QString& line, int level)
{
    QString warningIconHTML = "<body><img src=\":/Icons/Warning Shield-48.png\" class=\"Image\"/>";
    QString infoIconHTML = "<body><img src=\":/Icons/Info-48.png\" class=\"Image\"/>";
    QString alertHtml  = "<font color=\"#b71c1c\">";
    QString notifyHtml = "<font color=\"#827717\">";
    QString infoHtml   = "<font color=\"#0D47A1\">";
    QString endHtml    = "</font><br> </body>";

    switch(level)
    {
    case msg_alert:  line = warningIconHTML + alertHtml + line; break;
    case msg_notify: line = infoIconHTML + notifyHtml + line; break;
    case msg_info:   line = infoIconHTML + infoHtml + line; break;
    default:         line = warningIconHTML + infoHtml + line; break;
    }

    line = line + endHtml;
}
