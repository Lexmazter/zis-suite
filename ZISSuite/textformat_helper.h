#ifndef TEXTFORMAT_HELPER_H
#define TEXTFORMAT_HELPER_H

#include<QString>

class TextFormat_Helper
{
public:
    TextFormat_Helper();
    void setLineAsHTMLMsg(QString &line, int level);
    enum {msg_info, msg_notify, msg_alert};

private:
};

#endif // TEXTFORMAT_HELPER_H
