#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QPropertyAnimation>
#include <QAction>
#include <QMenu>
#include <QToolBar>
#include <QToolButton>
#include <QTableView>
#include "version.h"
#include "Dialogs/about_dialog.h"
#include "Forms/nomenclator_form.h"
#include "Forms/facturiform.h"
#include "Forms/bonuriform.h"
#include "Forms/receptiiform.h"
#include "Forms/terti_form.h"
#include <codbarehelper.h>
#include <Forms/chartsform.h>
#include <Forms/usersform.h>
#include <Forms/genericform.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QTableView* getCurrentTable() const;
    QString getStatusText();

    //singleton design pattern for mainwindow instance
    static MainWindow *getInstance();

    // Use the current user as a property of the main window, to be used globally
    Q_PROPERTY(QVariant currentUser MEMBER m_currentUser READ currentUser WRITE setCurrentUser)

    void setCurrentUser(QVariant currentUser)
    { m_currentUser = currentUser; }

    QVariant currentUser()
    { return m_currentUser; }

public slots:

private slots:
    void AddWidgetToMain(QWidget * toBeAdded, const QIcon icon, const QString label);

    void on_button_about_clicked();

    void on_button_nomenclator_clicked();

    void on_expand_collapse_button_clicked();

    void on_toolButton_clicked();

    void on_toolButton_ExportFile_clicked();

    void on_toolButton_ImportFile_clicked();

    void on_toolButton_PrintFile_clicked();

    void on_toolButtonIntrari_clicked();

    void slotChoseFacturi();

    void slotChoseBonuri();

    void on_button_help_clicked();

    void on_toolButtonCodBare_clicked();

    void on_tabWidget_Open_tabCloseRequested(int index);

    void on_toolButtonGrafice_clicked();

    void on_toolButton_Users_clicked();

    void on_toolButton_firm_clicked();

    void on_toolButton_dashboard_clicked();

    void on_toolButton_Captcha_clicked();

    void on_tabWidget_Open_currentChanged(int index);

    void on_toolButton_CursBNR_clicked();

    void on_toolButton_EmitDocuments_clicked();

    void on_toolButton_2_clicked();

private:
    void closeEvent(QCloseEvent *event);
    void createCustomActions();
    void createCustomMenus();
    void createCustomToolBars();
    void createCustomToolButtons();
    static MainWindow *uniqueInstance;

    QVariant m_currentUser;
    Ui::MainWindow *ui;
    QAction* actionChoseBonuri;
    QAction* actionChoseFacturi;
    QMenu* choseCustomMenu;
    QToolBar* editToolBar;
    QPropertyAnimation *tabAnimation;
};

#endif // MAINWINDOW_H
