import QtQuick 2.3
import "jbQuick.Charts"

Chart {
    id: chart_generic;
    width: 450;
    height: 450;
    chartAnimated: true;
    chartAnimationEasing: Easing.Linear;
    chartAnimationDuration: 600;
    chartType: Charts.ChartType.LINE;
    chartOptions: ({datasetFill: true,
                   bezierCurve: true});

    function setData(data)
    {
        chartData =  JSON.parse(data);
    }

    function setOptions(options)
    {
        chartOptions = JSON.parse(options);
    }

    function setType(type)
    {
        chartType = type;
    }
}
