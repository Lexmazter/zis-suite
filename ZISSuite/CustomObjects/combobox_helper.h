#ifndef combobox_helper_H
#define combobox_helper_H

#include<QComboBox>

#define COMBOBOX_UM 0
#define COMBOBOX_TERT 1
#define COMBOBOX_COUNTRY 2
#define COMBOBOX_JUDET 3
#define COMBOBOX_TVA 4

class ComboBoxHelper
{
public:
    ComboBoxHelper();
    void setUpComboBox(QComboBox *comboBox, int type);
    int indexOf(QString value) const;
    static QMap<QString, QString> *getComboboxMap(int type);
private:
    QMap<QString, QString>ComboBoxItems;
};

#endif // combobox_helper_H
