#ifndef CUSTOMLABEL_H
#define CUSTOMLABEL_H

#include <QLabel>
#include <QWidget>
#include <Qt>
#include<QMouseEvent>

class CustomLabel : public QLabel {
    Q_OBJECT

public:
    explicit CustomLabel(QWidget* parent = Q_NULLPTR, QString pixmapName="", Qt::WindowFlags f = Qt::WindowFlags());
    ~CustomLabel();
    void updateImage(QString imagePath);

signals:
    void clicked(QLabel*);

protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent *ev);
    QWidget* parent = nullptr;

private:
    QPoint offset;

};

#endif // CUSTOMLABEL_H
