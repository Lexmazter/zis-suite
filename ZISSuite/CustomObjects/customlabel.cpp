#include "customlabel.h"

CustomLabel::CustomLabel(QWidget* parent, QString pixmapName,  Qt::WindowFlags f)
    : QLabel(parent) {
    if(!pixmapName.isEmpty()){
        QPixmap pixmap(pixmapName);
        this->setPixmap(pixmap);
        this->parent = parent;
    }
    this->setScaledContents(true);
    this->setBackgroundRole(QPalette::Base);

}

void CustomLabel::updateImage(QString imagePath)
{
    if(!imagePath.isEmpty()){
        QPixmap pixmap(imagePath);
        this->setPixmap(pixmap);

//        int w = parent->width();
//        int h = parent->height();
//        this->setPixmap(pixmap.scaled(w, h, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
    }
}

CustomLabel::~CustomLabel() {}

void CustomLabel::mousePressEvent(QMouseEvent* event) {
    this->setBackgroundRole(QPalette::Highlight);
    emit clicked(this);
    offset = event->pos();
}

void CustomLabel::mouseMoveEvent(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton)
    {
        this->move(mapToParent(event->pos() - offset));
    }
}
