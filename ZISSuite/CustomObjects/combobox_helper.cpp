#include "combobox_helper.h"
#include <QComboBox>

ComboBoxHelper::ComboBoxHelper()
{

}

QMap<QString, QString>* ComboBoxHelper::getComboboxMap(int type)
{
    QMap<QString, QString>* retMap = new QMap<QString, QString>;

    switch (type) {
    case COMBOBOX_UM:
        retMap->insert("buc", "BUC");
        retMap->insert("kg", "KG");
        retMap->insert("metri", "METRI");
        retMap->insert("litri", "LITRI");
        retMap->insert("altele", "ALTELE");
        break;
    case COMBOBOX_TERT:
        retMap->insert("client","Client");
        retMap->insert("furnizor","Furnizor");
        retMap->insert("client & furnizor","Client & Furnizor");
        break;

    case COMBOBOX_JUDET:
        retMap->insert("alba","ALBA");
        retMap->insert("arad","ARAD");
        retMap->insert("argeş","ARGEŞ");
        retMap->insert("bacău","BACĂU");
        retMap->insert("bihor","BIHOR");
        retMap->insert("bistriţa-năsăud","BISTRIŢA-NĂSĂUD");
        retMap->insert("botoşani","BOTOŞANI");
        retMap->insert("braşov","BRAŞOV");
        retMap->insert("brăila","BRĂILA");
        retMap->insert("municipiul bucureşti","MUNICIPIUL BUCUREŞTI");
        retMap->insert("buzău","BUZĂU");
        retMap->insert("caraş-severin","CARAŞ-SEVERIN");
        retMap->insert("călăraşi","CĂLĂRAŞI");
        retMap->insert("cluj","CLUJ");
        retMap->insert("constanţa","CONSTANŢA");
        retMap->insert("covasna","COVASNA");
        retMap->insert("dâmboviţa","DÂMBOVIŢA");
        retMap->insert("dolj","DOLJ");
        retMap->insert("galaţi","GALAŢI");
        retMap->insert("giurgiu","GIURGIU");
        retMap->insert("gorj","GORJ");
        retMap->insert("harghita","HARGHITA");
        retMap->insert("hunedoara","HUNEDOARA");
        retMap->insert("ialomiţa","IALOMIŢA");
        retMap->insert("iaşi","IAŞI");
        retMap->insert("ilfov","ILFOV");
        retMap->insert("maramureş","MARAMUREŞ");
        retMap->insert("mehedinţi","MEHEDINŢI");
        retMap->insert("mureş","MUREŞ");
        retMap->insert("neamţ","NEAMŢ");
        retMap->insert("olt","OLT");
        retMap->insert("prahova","PRAHOVA");
        retMap->insert("satu mare","SATU MARE");
        retMap->insert("sălaj","SĂLAJ");
        retMap->insert("sibiu","SIBIU");
        retMap->insert("suceava","SUCEAVA");
        retMap->insert("teleorman","TELEORMAN");
        retMap->insert("timiş","TIMIŞ");
        retMap->insert("tulcea","TULCEA");
        retMap->insert("vaslui","VASLUI");
        retMap->insert("vâlcea","VÂLCEA");
        retMap->insert("vrancea","VRANCEA");
        break;

    case COMBOBOX_COUNTRY:
        retMap->insert("af","Afghanistan AF ");
        retMap->insert("al","Albania AL ");
        retMap->insert("dz","Algeria DZ ");
        retMap->insert("as","American Samoa AS ");
        retMap->insert("ad","Andorra AD ");
        retMap->insert("ao","Angola AO ");
        retMap->insert("ai","Anguilla AI ");
        retMap->insert("aq","Antarctica AQ ");
        retMap->insert("ag","Antigua and Barbuda AG ");
        retMap->insert("ar","Argentina AR ");
        retMap->insert("am","Armenia AM ");
        retMap->insert("aw","Aruba AW ");
        retMap->insert("au","Australia AU ");
        retMap->insert("at","Austria AT ");
        retMap->insert("az","Azerbaijan AZ ");
        retMap->insert("bs","Bahamas BS ");
        retMap->insert("bh","Bahrain BH ");
        retMap->insert("bd","Bangladesh BD ");
        retMap->insert("bb","Barbados BB ");
        retMap->insert("by","Belarus BY ");
        retMap->insert("be","Belgium BE ");
        retMap->insert("bz","Belize BZ ");
        retMap->insert("bj","Benin BJ ");
        retMap->insert("bm","Bermuda BM ");
        retMap->insert("bt","Bhutan BT ");
        retMap->insert("bo","Bolivia BO ");
        retMap->insert("bq","Bonaire BQ ");
        retMap->insert("ba","Bosnia and Herzegovina BA ");
        retMap->insert("bw","Botswana BW ");
        retMap->insert("bv","Bouvet Island BV ");
        retMap->insert("br","Brazil BR ");
        retMap->insert("io","British Indian Ocean Territory IO ");
        retMap->insert("bn","Brunei Darussalam BN ");
        retMap->insert("bg","Bulgaria BG ");
        retMap->insert("bf","Burkina Faso BF ");
        retMap->insert("bi","Burundi BI ");
        retMap->insert("kh","Cambodia KH ");
        retMap->insert("cm","Cameroon CM ");
        retMap->insert("ca","Canada CA ");
        retMap->insert("cv","Cape Verde CV ");
        retMap->insert("ky","Cayman Islands KY ");
        retMap->insert("cf","Central African Republic CF ");
        retMap->insert("td","Chad TD ");
        retMap->insert("cl","Chile CL ");
        retMap->insert("cn","China CN ");
        retMap->insert("cx","Christmas Island CX ");
        retMap->insert("cc","Cocos (Keeling) Islands CC ");
        retMap->insert("co","Colombia CO ");
        retMap->insert("km","Comoros KM ");
        retMap->insert("cg","Congo CG ");
        retMap->insert("cd","Democratic Republic of the Congo CD ");
        retMap->insert("ck","Cook Islands CK ");
        retMap->insert("cr","Costa Rica CR ");
        retMap->insert("hr","Croatia HR ");
        retMap->insert("cu","Cuba CU ");
        retMap->insert("cw","CuraÃ§ao CW ");
        retMap->insert("cy","Cyprus CY ");
        retMap->insert("cz","Czech Republic CZ ");
        retMap->insert("ci","CÃ´te d'Ivoire CI ");
        retMap->insert("dk","Denmark DK ");
        retMap->insert("dj","Djibouti DJ ");
        retMap->insert("dm","Dominica DM ");
        retMap->insert("do","Dominican Republic DO ");
        retMap->insert("ec","Ecuador EC ");
        retMap->insert("eg","Egypt EG ");
        retMap->insert("sv","El Salvador SV ");
        retMap->insert("gq","Equatorial Guinea GQ ");
        retMap->insert("er","Eritrea ER ");
        retMap->insert("ee","Estonia EE ");
        retMap->insert("et","Ethiopia ET ");
        retMap->insert("fk","Falkland Islands (Malvinas) FK ");
        retMap->insert("fo","Faroe Islands FO ");
        retMap->insert("fj","Fiji FJ ");
        retMap->insert("fi","Finland FI ");
        retMap->insert("fr","France FR ");
        retMap->insert("gf","French Guiana GF ");
        retMap->insert("pf","French Polynesia PF ");
        retMap->insert("tf","French Southern Territories TF ");
        retMap->insert("ga","Gabon GA ");
        retMap->insert("gm","Gambia GM ");
        retMap->insert("ge","Georgia GE ");
        retMap->insert("de","Germany DE ");
        retMap->insert("gh","Ghana GH ");
        retMap->insert("gi","Gibraltar GI ");
        retMap->insert("gr","Greece GR ");
        retMap->insert("gl","Greenland GL ");
        retMap->insert("gd","Grenada GD ");
        retMap->insert("gp","Guadeloupe GP ");
        retMap->insert("gu","Guam GU ");
        retMap->insert("gt","Guatemala GT ");
        retMap->insert("gg","Guernsey GG ");
        retMap->insert("gn","Guinea GN ");
        retMap->insert("gw","Guinea-Bissau GW ");
        retMap->insert("gy","Guyana GY ");
        retMap->insert("ht","Haiti HT ");
        retMap->insert("hm","Heard Island and McDonald Mcdonald Islands HM ");
        retMap->insert("va","Holy See (Vatican City State) VA ");
        retMap->insert("hn","Honduras HN ");
        retMap->insert("hk","Hong Kong HK ");
        retMap->insert("hu","Hungary HU ");
        retMap->insert("is","Iceland IS ");
        retMap->insert("in","India IN ");
        retMap->insert("id","Indonesia ID ");
        retMap->insert("ir","Iran, Islamic Republic of IR ");
        retMap->insert("iq","Iraq IQ ");
        retMap->insert("ie","Ireland IE ");
        retMap->insert("im","Isle of Man IM ");
        retMap->insert("il","Israel IL ");
        retMap->insert("it","Italy IT ");
        retMap->insert("jm","Jamaica JM ");
        retMap->insert("jp","Japan JP ");
        retMap->insert("je","Jersey JE ");
        retMap->insert("jo","Jordan JO ");
        retMap->insert("kz","Kazakhstan KZ ");
        retMap->insert("ke","Kenya KE ");
        retMap->insert("ki","Kiribati KI ");
        retMap->insert("kp","Korea, Democratic People's Republic of KP ");
        retMap->insert("kr","Korea, Republic of KR ");
        retMap->insert("kw","Kuwait KW ");
        retMap->insert("kg","Kyrgyzstan KG ");
        retMap->insert("la","Lao People's Democratic Republic LA ");
        retMap->insert("lv","Latvia LV ");
        retMap->insert("lb","Lebanon LB ");
        retMap->insert("ls","Lesotho LS ");
        retMap->insert("lr","Liberia LR ");
        retMap->insert("ly","Libya LY ");
        retMap->insert("li","Liechtenstein LI ");
        retMap->insert("lt","Lithuania LT ");
        retMap->insert("lu","Luxembourg LU ");
        retMap->insert("mo","Macao MO ");
        retMap->insert("mk","Macedonia, the Former Yugoslav Republic of MK ");
        retMap->insert("mg","Madagascar MG ");
        retMap->insert("mw","Malawi MW ");
        retMap->insert("my","Malaysia MY ");
        retMap->insert("mv","Maldives MV ");
        retMap->insert("ml","Mali ML ");
        retMap->insert("mt","Malta MT ");
        retMap->insert("mh","Marshall Islands MH ");
        retMap->insert("mq","Martinique MQ ");
        retMap->insert("mr","Mauritania MR ");
        retMap->insert("mu","Mauritius MU ");
        retMap->insert("yt","Mayotte YT ");
        retMap->insert("mx","Mexico MX ");
        retMap->insert("fm","Micronesia, Federated States of FM ");
        retMap->insert("md","Moldova, Republic of MD ");
        retMap->insert("mc","Monaco MC ");
        retMap->insert("mn","Mongolia MN ");
        retMap->insert("me","Montenegro ME ");
        retMap->insert("ms","Montserrat MS ");
        retMap->insert("ma","Morocco MA ");
        retMap->insert("mz","Mozambique MZ ");
        retMap->insert("mm","Myanmar MM ");
        retMap->insert("na","Namibia NA ");
        retMap->insert("nr","Nauru NR ");
        retMap->insert("np","Nepal NP ");
        retMap->insert("nl","Netherlands NL ");
        retMap->insert("nc","New Caledonia NC ");
        retMap->insert("nz","New Zealand NZ ");
        retMap->insert("ni","Nicaragua NI ");
        retMap->insert("ne","Niger NE ");
        retMap->insert("ng","Nigeria NG ");
        retMap->insert("nu","Niue NU ");
        retMap->insert("nf","Norfolk Island NF ");
        retMap->insert("mp","Northern Mariana Islands MP ");
        retMap->insert("no","Norway NO ");
        retMap->insert("om","Oman OM ");
        retMap->insert("pk","Pakistan PK ");
        retMap->insert("pw","Palau PW ");
        retMap->insert("ps","Palestine, State of PS ");
        retMap->insert("pa","Panama PA ");
        retMap->insert("pg","Papua New Guinea PG ");
        retMap->insert("py","Paraguay PY ");
        retMap->insert("pe","Peru PE ");
        retMap->insert("ph","Philippines PH ");
        retMap->insert("pn","Pitcairn PN ");
        retMap->insert("pl","Poland PL ");
        retMap->insert("pt","Portugal PT ");
        retMap->insert("pr","Puerto Rico PR ");
        retMap->insert("qa","Qatar QA ");
        retMap->insert("ro","Romania RO ");
        retMap->insert("ru","Russian Federation RU ");
        retMap->insert("rw","Rwanda RW ");
        retMap->insert("re","Reunion RE ");
        retMap->insert("bl","Saint Barthelemy BL ");
        retMap->insert("sh","Saint Helena SH ");
        retMap->insert("kn","Saint Kitts and Nevis KN ");
        retMap->insert("lc","Saint Lucia LC ");
        retMap->insert("mf","Saint Martin (French part) MF ");
        retMap->insert("pm","Saint Pierre and Miquelon PM ");
        retMap->insert("vc","Saint Vincent and the Grenadines VC ");
        retMap->insert("ws","Samoa WS ");
        retMap->insert("sm","San Marino SM ");
        retMap->insert("st","Sao Tome and Principe ST ");
        retMap->insert("sa","Saudi Arabia SA ");
        retMap->insert("sn","Senegal SN ");
        retMap->insert("rs","Serbia RS ");
        retMap->insert("sc","Seychelles SC ");
        retMap->insert("sl","Sierra Leone SL ");
        retMap->insert("sg","Singapore SG ");
        retMap->insert("sx","Sint Maarten (Dutch part) SX ");
        retMap->insert("sk","Slovakia SK ");
        retMap->insert("si","Slovenia SI ");
        retMap->insert("sb","Solomon Islands SB ");
        retMap->insert("so","Somalia SO ");
        retMap->insert("za","South Africa ZA ");
        retMap->insert("gs","South Georgia and the South Sandwich Islands GS ");
        retMap->insert("ss","South Sudan SS ");
        retMap->insert("es","Spain ES ");
        retMap->insert("lk","Sri Lanka LK ");
        retMap->insert("sd","Sudan SD ");
        retMap->insert("sr","Suriname SR ");
        retMap->insert("sj","Svalbard and Jan Mayen SJ ");
        retMap->insert("sz","Swaziland SZ ");
        retMap->insert("se","Sweden SE ");
        retMap->insert("ch","Switzerland CH ");
        retMap->insert("sy","Syrian Arab Republic SY ");
        retMap->insert("tw","Taiwan, Province of China TW ");
        retMap->insert("tj","Tajikistan TJ ");
        retMap->insert("tz","United Republic of Tanzania TZ ");
        retMap->insert("th","Thailand TH ");
        retMap->insert("tl","Timor-Leste TL ");
        retMap->insert("tg","Togo TG ");
        retMap->insert("tk","Tokelau TK ");
        retMap->insert("to","Tonga TO ");
        retMap->insert("tt","Trinidad and Tobago TT ");
        retMap->insert("tn","Tunisia TN ");
        retMap->insert("tr","Turkey TR ");
        retMap->insert("tm","Turkmenistan TM ");
        retMap->insert("tc","Turks and Caicos Islands TC ");
        retMap->insert("tv","Tuvalu TV ");
        retMap->insert("ug","Uganda UG ");
        retMap->insert("ua","Ukraine UA ");
        retMap->insert("ae","United Arab Emirates AE ");
        retMap->insert("gb","United Kingdom GB ");
        retMap->insert("us","United States US ");
        retMap->insert("um","United States Minor Outlying Islands UM ");
        retMap->insert("uy","Uruguay UY ");
        retMap->insert("uz","Uzbekistan UZ ");
        retMap->insert("vu","Vanuatu VU ");
        retMap->insert("ve","Venezuela VE ");
        retMap->insert("vn","Viet Nam VN ");
        retMap->insert("vg","British Virgin Islands VG ");
        retMap->insert("vi","US Virgin Islands VI ");
        retMap->insert("wf","Wallis and Futuna WF ");
        retMap->insert("eh","Western Sahara EH ");
        retMap->insert("ye","Yemen YE ");
        retMap->insert("zm","Zambia ZM ");
        retMap->insert("zw","Zimbabwe ZW ");
        retMap->insert("ax","Aland Islands AX ");
        break;
    case COMBOBOX_TVA:
        retMap->insert("24","24%");
        retMap->insert("20","20%");
        retMap->insert("19","19%");
        retMap->insert("9","9%");
        retMap->insert("5","5%");
        retMap->insert("0","0% - TVA Inclus");
        break;
    default:
        break;
    }

    return retMap;
}

void ComboBoxHelper::setUpComboBox(QComboBox *comboBox, int type)
{

    if(!ComboBoxItems.isEmpty())
        ComboBoxItems.clear();

    // TODO - Populate the combobox based on db query
    ComboBoxItems = *getComboboxMap(type);

    comboBox->addItems(ComboBoxItems.values());
}

int ComboBoxHelper::indexOf(QString value) const
{
    int index = std::distance(ComboBoxItems.begin(), ComboBoxItems.find(value));

    return index;
}
