#include "dbhelper.h"
#include "Delegates/colordelegate.h"
#include "firmsettings_helper.h"

// Define our private variables
QSqlDatabase DbHelper::db;

/**
 * @brief DbHelper::dbOpen - Opens the database as specified by the type identifier
 * @param type - currently we specify the type as SQLITE_DB_TYPE, MYSQL_DB_TYPE, ORACLE_DB_TYPE, more to be added later
 * @return a bool value representif if the db has been opened or not, to be used in error handling
 */
bool DbHelper::dbOpen(int type)
{
    // TODO - take info from external config file
    switch(type)
    {
    case SQLITE_DB_TYPE:
    {
        // Get local path
        QDir temp_path(QCoreApplication::applicationDirPath());

#ifdef QT_DEBUG
        // On debug we need to navigate out of the debug folder
        temp_path.cdUp();
        temp_path.cdUp();
#endif

        // We navigate to the demo db
        QString db_path = temp_path.absolutePath() + "\\Database\\zis_demo.db";
        FirmSettings_Helper::getINstance()->DB_Application_Path = db_path;

#ifdef QT_DEBUG
        qDebug(db_path.toLatin1());
#endif

        db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(db_path);

        break;
    }

    case MYSQL_DB_TYPE:
        // TODO - Add support for mysql
        break;

    case ORACLE_DB_TYPE:
        // TODO - Add support for oracle
        break;
    }
    return db.open();
}

/**
 * @brief DbHelper::dbClose - this function only closes the db.
 */
void DbHelper::dbClose()
{
    db.close();
}

/**
 * @brief DbHelper::Login - this function handles the login sequence and query.
 * @param username - this contains the username that will be searched for in the users table.
 * @param password - this contains the password that will be matched with the user in the table.
 * @return an int value that can be LOGIN_SUCCESFUL, PASSWORD_FAILED or USERNAME_FAILED for error handling.
 */
int DbHelper::Login(QString username, QString password)
{
    int ret_val = UNKNOWN_ERROR;

    bool db_ok = dbOpen(SQLITE_DB_TYPE);

    // Can we open the database?
    if(db_ok != true)
    {
        ret_val = DB_NOT_FOUND;
    }
    else
    {
        // Execute the query for login
        QSqlQuery query;
        // Partially protect against SQL injection by using bind values
        query.prepare("select count(*) as Found from Users where Utilizator = ? and Parola  = ?");
        query.addBindValue(username);
        query.addBindValue(password);
        query.exec();
        query.next();

#ifdef QT_DEBUG
        qDebug(QString("User: "+username+"  Password: " + password+"").toLatin1());
#endif

        int query_val = query.value(0).toInt();

        if(query_val == 1)
        {
            ret_val = LOGIN_SUCCESSFUL;
        }
        else
        {
            // Is the user incorrect or the password?
            query.exec("select count(*) as Found from Users where Utilizator = '"+username+"'");
            query.next();
            query_val = query.value(0).toInt();

            if(query_val == 1)
            {
                ret_val = PASSWORD_FAILED;
            }
            else
            {
                ret_val = USERNAME_FAILED;
            }
        }
    }

    return ret_val;
}

/**
 * @brief DbHelper::getNomenclator - this function returns all products from the Nomenclator table.
 * @return a query model to be desplayed in the Nomenclator tableview.
 */
QSqlQueryModel *DbHelper::getModelFromQuery(QObject* parent, QString table)
{
    QSqlQueryModel* genericQueryModel = new QSqlQueryModel(parent);
    QSqlQuery query;
    query.prepare("select * from "+table+"");
    query.exec();
    genericQueryModel->setQuery(query);

    return genericQueryModel;
}

void DbHelper::deleteFromTable(QString table, QString id)
{
    QSqlQuery query;
    query.prepare("delete from "+table+" where ID = ?");
    query.addBindValue(id);
    query.exec();
}

CustomSqlTable* DbHelper::getTableModel(QObject *parent, QString table, ColorDelegate* delegate)
{
    CustomSqlTable* genericTableModel = new CustomSqlTable(parent, db, delegate);
    genericTableModel->setTable(table);
    genericTableModel->setEditStrategy(CustomSqlTable::OnManualSubmit);

    if(genericTableModel->select())
    {

    }
    else
    {
        qDebug() << genericTableModel->lastError().driverText();
    }

    return genericTableModel;
}

void DbHelper::saveTableModelToDb(QAbstractItemModel *model)
{
    static_cast<CustomSqlTable*>(model)->submitAll();
}

/**
 * @brief DbHelper::getModelFromConditionalQuery - this function returns a model based on condition specified.
 * @return a query model to be displayed.
 */
QSqlQueryModel *DbHelper::getModelFromQuery(QObject* parent, QString table, QString *selectClause, QString *condition)
{
    QSqlQueryModel* genericQueryModel = new QSqlQueryModel(parent);
    QSqlQuery query;
    query.prepare("SELECT " + *selectClause + " FROM "+table+" WHERE " + *condition );
    query.exec();
    genericQueryModel->setQuery(query);

    return genericQueryModel;
}
/**
 * @brief DbHelper::getTotalBillValueForProducts - this function returns the total bill value for the products spcified by id.
 * @return total bill value.
 */
double DbHelper::getTotalBillValueForProducts(QString table, QString ids)
{
    double retVal=0;
    QSqlQuery query;
    query.prepare("SELECT Pret FROM "+table+" WHERE ID IN (" + ids + ")"  );
    query.exec();
    while (query.next()) {
        QSqlRecord record = query.record();
        retVal += record.value("Pret").toDouble();
    }
    return retVal;
}

/**
 * @brief DbHelper::getAmountById - this function returns the available amounts in the table by IDs.
 * @return list with columns data.
 */
void DbHelper::getAvailableQuantityForProductsById(QString table, QString IDs, QMap<int, float> *returnedMap)
{

    QSqlQuery query;
    query.prepare("SELECT ID, Stoc FROM " + table + " WHERE ID IN (" + IDs + ")" );
    query.exec();
    while (query.next()) {

        QSqlRecord record = query.record();
        int ID = record.value("ID").toInt();
        int Stoc = record.value("Stoc").toFloat();
        returnedMap->insert(ID, Stoc);
    }
}

/**
 * @brief DbHelper::getSelectedItemsInList - grab the items specified by selected clause and return as a list
 * @return true if data is available, false otherwise
 */
bool DbHelper::getSelectedItemsInList(QString table, QString selectClause, QString queryCondition, QStringList *returnedAttributes)
{
    if(returnedAttributes == nullptr)
        return false;
    QSqlQuery query;
    QStringList listItems = selectClause.split(", ");
    if(listItems.length()<1)
        return false;

    query.prepare("SELECT " + selectClause + " FROM " + table +" WHERE " + queryCondition );
    query.exec();
    while (query.next()) {
        QSqlRecord record = query.record();
        for(int i=0; i<listItems.length(); i++)
        {
            returnedAttributes->append(record.value(listItems.value(i)).toString());
        }
    }
    return true;
}

/**
 * @brief DbHelper::updateQuery(QString table, QString setClause, QString queryCondition) - updates the table fields which fulfill the given condition
 * @return true if data is updated, false otherwise
 */
bool DbHelper::updateQuery(QString table, QString *setClause, QString *whereClause)
{
    QSqlQuery query;
    query.prepare("UPDATE " + table + " SET " + *setClause + " WHERE "  + *whereClause);

    return query.exec();
}

/**
 * @brief DbHelper::queryToMap(QString table, QString selectClause, QString queryCondition) - grab desired data and create a key-value based map which fulfills the required condition
 * The condition shall be chosen such as only one row is returned in order to immerse data in one map otherwise it will contain the last row returned by query
 * For multimple rows use method queryToListOfMaps.
 * @return a map consisting of all data
 */
bool DbHelper::queryToMap(QString table, QString selectClause, QString queryCondition, QVariantMap *map)
{
    QSqlQuery query;
    if(!queryCondition.isEmpty())
        query.prepare("SELECT " + selectClause + " FROM " + table +" WHERE " + queryCondition + "");
    else
        query.prepare("SELECT " + selectClause + " FROM " + table);

    query.exec();
    while (query.next()) {
        QSqlRecord record = query.record();
        for(int i=0; i<record.count(); i++){
            QString key = record.fieldName(i);
            QVariant value = record.value(i);
            map->insert(key, value);
        }
    }
    if(map->isEmpty())
        return false;
    else
        return true;
}

/**
 * @brief bool DbHelper::queryToListOfMaps(QString table, QString selectClause, QString queryCondition, QVariantMap map[]) - grab desired data and create a map based on key-value and fulfill the desired condition
 */
bool DbHelper::queryToListOfMaps(QString table, QString selectClause, QString queryCondition, QList<QVariantMap> *listOfMaps)
{
    QSqlQuery query;
    query.prepare("SELECT " + selectClause + " FROM " + table +" WHERE " + queryCondition + "");
    query.exec();
    int idx =0;
    while (query.next()) {
        QVariantMap map;
        QSqlRecord record = query.record();
        for(int i=0; i<record.count(); i++){
            QString key = record.fieldName(i);
            QVariant value = record.value(i);
            map.insert(key, value);
            idx++;
        }
        listOfMaps->insert(idx, map);
    }
    if(listOfMaps->isEmpty())
        return false;
    else
        return true;
}

/**
 * @brief DbHelper::updateSQLFromMap(QString table, QVariantMap *map, QString whereClause) - update fields in DB where columns name are exactly the ones in map's key.
 * @return true if query processed else otherwise.
 */
bool DbHelper::updateSQLFromMap(QString table, QVariantMap *map, QString whereClause)
{
    if(map == nullptr)
        return false;

    QSqlQuery query;
    QString Params = "";//"(";
    QString formalValues = "(";
    QVariantMap::Iterator iter;
    for( iter = map->begin(); iter != map->end(); ++iter) {
        QVariant varKey = iter.key();
        QVariant varValue = iter.value();
        Params += varKey.toString() + "=:" + varKey.toString() +  ", ";
    }
    //remove the last comma if available in string
    if(Params.contains(", "))
        Params.remove(Params.lastIndexOf(", "), 2);
    if(formalValues.contains(", "))
        formalValues.remove(formalValues.lastIndexOf(", "), 2);

    formalValues += ")";
    if(whereClause == NULL)
        query.prepare("UPDATE " + table + " SET " + Params);
    else
        query.prepare("UPDATE " + table + " SET " + Params + " WHERE "  + whereClause);
    for( iter = map->begin(); iter != map->end(); ++iter) {
        QVariant varKey = iter.key();
        QVariant varValue = iter.value();
        query.bindValue(":"+ varKey.toString(), varValue);
    }

    return query.exec();
}

/**
 * @brief bool DbHelper::insertSQLFromMap(QString table, QVariantMap *map) - insert fields in DB where columns name are exactly the ones in map's key.
 * @return true if query processed else otherwise.
 */
bool DbHelper::insertSQLFromMap(QString table, QVariantMap *map)
{
    if(map == nullptr)
        return false;

    QSqlQuery query;
    QString Params = "(";
    QString formalValues = "(";
    QVariantMap::Iterator iter;
    for( iter = map->begin(); iter != map->end(); ++iter) {
        QVariant varKey = iter.key();
        QVariant varValue = iter.value();
        Params += varKey.toString() + ", ";
        formalValues += ":" + varKey.toString()  + ", ";
    }
    //remove the last comma if available in string
    if(Params.contains(", "))
        Params.remove(Params.lastIndexOf(", "), 2);
    if(formalValues.contains(", "))
        formalValues.remove(formalValues.lastIndexOf(", "), 2);

    Params += ")";
    formalValues += ")";
    query.prepare("INSERT INTO " + table + Params + " VALUES " + formalValues);
    for( iter = map->begin(); iter != map->end(); ++iter) {
        QVariant varKey = iter.key();
        QVariant varValue = iter.value();
        query.bindValue(":"+ varKey.toString(), varValue);
    }

    return query.exec();
}
