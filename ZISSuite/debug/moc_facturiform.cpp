/****************************************************************************
** Meta object code from reading C++ file 'facturiform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../facturiform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'facturiform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_FacturiForm_t {
    QByteArrayData data[15];
    char stringdata0[229];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FacturiForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FacturiForm_t qt_meta_stringdata_FacturiForm = {
    {
QT_MOC_LITERAL(0, 0, 11), // "FacturiForm"
QT_MOC_LITERAL(1, 12, 23), // "on_actionSave_triggered"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 20), // "on_tableView_clicked"
QT_MOC_LITERAL(4, 58, 5), // "index"
QT_MOC_LITERAL(5, 64, 11), // "sortChanged"
QT_MOC_LITERAL(6, 76, 13), // "Qt::SortOrder"
QT_MOC_LITERAL(7, 90, 5), // "order"
QT_MOC_LITERAL(8, 96, 25), // "on_actionDelete_triggered"
QT_MOC_LITERAL(9, 122, 22), // "on_actionAdd_triggered"
QT_MOC_LITERAL(10, 145, 26), // "on_actionRefresh_triggered"
QT_MOC_LITERAL(11, 172, 26), // "on_tableView_doubleClicked"
QT_MOC_LITERAL(12, 199, 16), // "on_filterChanged"
QT_MOC_LITERAL(13, 216, 6), // "column"
QT_MOC_LITERAL(14, 223, 5) // "value"

    },
    "FacturiForm\0on_actionSave_triggered\0"
    "\0on_tableView_clicked\0index\0sortChanged\0"
    "Qt::SortOrder\0order\0on_actionDelete_triggered\0"
    "on_actionAdd_triggered\0"
    "on_actionRefresh_triggered\0"
    "on_tableView_doubleClicked\0on_filterChanged\0"
    "column\0value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FacturiForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    1,   55,    2, 0x08 /* Private */,
       5,    2,   58,    2, 0x08 /* Private */,
       8,    0,   63,    2, 0x08 /* Private */,
       9,    0,   64,    2, 0x08 /* Private */,
      10,    0,   65,    2, 0x08 /* Private */,
      11,    1,   66,    2, 0x08 /* Private */,
      12,    2,   69,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    4,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 6,    4,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   13,   14,

       0        // eod
};

void FacturiForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FacturiForm *_t = static_cast<FacturiForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionSave_triggered(); break;
        case 1: _t->on_tableView_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 2: _t->sortChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< Qt::SortOrder(*)>(_a[2]))); break;
        case 3: _t->on_actionDelete_triggered(); break;
        case 4: _t->on_actionAdd_triggered(); break;
        case 5: _t->on_actionRefresh_triggered(); break;
        case 6: _t->on_tableView_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 7: _t->on_filterChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObject FacturiForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_FacturiForm.data,
      qt_meta_data_FacturiForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FacturiForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FacturiForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FacturiForm.stringdata0))
        return static_cast<void*>(const_cast< FacturiForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int FacturiForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
