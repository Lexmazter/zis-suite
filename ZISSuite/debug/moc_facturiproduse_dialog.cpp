/****************************************************************************
** Meta object code from reading C++ file 'facturiproduse_dialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../facturiproduse_dialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'facturiproduse_dialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_FacturiProduseDialog_t {
    QByteArrayData data[12];
    char stringdata0[210];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FacturiProduseDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FacturiProduseDialog_t qt_meta_stringdata_FacturiProduseDialog = {
    {
QT_MOC_LITERAL(0, 0, 20), // "FacturiProduseDialog"
QT_MOC_LITERAL(1, 21, 25), // "on_actionDelete_triggered"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 22), // "on_actionAdd_triggered"
QT_MOC_LITERAL(4, 71, 26), // "on_actionRefresh_triggered"
QT_MOC_LITERAL(5, 98, 23), // "on_actionSave_triggered"
QT_MOC_LITERAL(6, 122, 26), // "on_tableView_doubleClicked"
QT_MOC_LITERAL(7, 149, 5), // "index"
QT_MOC_LITERAL(8, 155, 20), // "on_tableView_clicked"
QT_MOC_LITERAL(9, 176, 13), // "onDataChanged"
QT_MOC_LITERAL(10, 190, 7), // "topLeft"
QT_MOC_LITERAL(11, 198, 11) // "bottomRight"

    },
    "FacturiProduseDialog\0on_actionDelete_triggered\0"
    "\0on_actionAdd_triggered\0"
    "on_actionRefresh_triggered\0"
    "on_actionSave_triggered\0"
    "on_tableView_doubleClicked\0index\0"
    "on_tableView_clicked\0onDataChanged\0"
    "topLeft\0bottomRight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FacturiProduseDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x08 /* Private */,
       3,    0,   50,    2, 0x08 /* Private */,
       4,    0,   51,    2, 0x08 /* Private */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    1,   53,    2, 0x08 /* Private */,
       8,    1,   56,    2, 0x08 /* Private */,
       9,    2,   59,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    7,
    QMetaType::Void, QMetaType::QModelIndex,    7,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,   10,   11,

       0        // eod
};

void FacturiProduseDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FacturiProduseDialog *_t = static_cast<FacturiProduseDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionDelete_triggered(); break;
        case 1: _t->on_actionAdd_triggered(); break;
        case 2: _t->on_actionRefresh_triggered(); break;
        case 3: _t->on_actionSave_triggered(); break;
        case 4: _t->on_tableView_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 5: _t->on_tableView_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 6: _t->onDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObject FacturiProduseDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_FacturiProduseDialog.data,
      qt_meta_data_FacturiProduseDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FacturiProduseDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FacturiProduseDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FacturiProduseDialog.stringdata0))
        return static_cast<void*>(const_cast< FacturiProduseDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int FacturiProduseDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
