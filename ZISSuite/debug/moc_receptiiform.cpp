/****************************************************************************
** Meta object code from reading C++ file 'receptiiform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../receptiiform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'receptiiform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ReceptiiForm_t {
    QByteArrayData data[15];
    char stringdata0[230];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ReceptiiForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ReceptiiForm_t qt_meta_stringdata_ReceptiiForm = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ReceptiiForm"
QT_MOC_LITERAL(1, 13, 23), // "on_actionSave_triggered"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 20), // "on_tableView_clicked"
QT_MOC_LITERAL(4, 59, 5), // "index"
QT_MOC_LITERAL(5, 65, 11), // "sortChanged"
QT_MOC_LITERAL(6, 77, 13), // "Qt::SortOrder"
QT_MOC_LITERAL(7, 91, 5), // "order"
QT_MOC_LITERAL(8, 97, 25), // "on_actionDelete_triggered"
QT_MOC_LITERAL(9, 123, 22), // "on_actionAdd_triggered"
QT_MOC_LITERAL(10, 146, 26), // "on_actionRefresh_triggered"
QT_MOC_LITERAL(11, 173, 26), // "on_tableView_doubleClicked"
QT_MOC_LITERAL(12, 200, 16), // "on_filterChanged"
QT_MOC_LITERAL(13, 217, 6), // "column"
QT_MOC_LITERAL(14, 224, 5) // "value"

    },
    "ReceptiiForm\0on_actionSave_triggered\0"
    "\0on_tableView_clicked\0index\0sortChanged\0"
    "Qt::SortOrder\0order\0on_actionDelete_triggered\0"
    "on_actionAdd_triggered\0"
    "on_actionRefresh_triggered\0"
    "on_tableView_doubleClicked\0on_filterChanged\0"
    "column\0value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ReceptiiForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    1,   55,    2, 0x08 /* Private */,
       5,    2,   58,    2, 0x08 /* Private */,
       8,    0,   63,    2, 0x08 /* Private */,
       9,    0,   64,    2, 0x08 /* Private */,
      10,    0,   65,    2, 0x08 /* Private */,
      11,    1,   66,    2, 0x08 /* Private */,
      12,    2,   69,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    4,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 6,    4,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   13,   14,

       0        // eod
};

void ReceptiiForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ReceptiiForm *_t = static_cast<ReceptiiForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionSave_triggered(); break;
        case 1: _t->on_tableView_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 2: _t->sortChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< Qt::SortOrder(*)>(_a[2]))); break;
        case 3: _t->on_actionDelete_triggered(); break;
        case 4: _t->on_actionAdd_triggered(); break;
        case 5: _t->on_actionRefresh_triggered(); break;
        case 6: _t->on_tableView_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 7: _t->on_filterChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObject ReceptiiForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ReceptiiForm.data,
      qt_meta_data_ReceptiiForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ReceptiiForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ReceptiiForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ReceptiiForm.stringdata0))
        return static_cast<void*>(const_cast< ReceptiiForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int ReceptiiForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
