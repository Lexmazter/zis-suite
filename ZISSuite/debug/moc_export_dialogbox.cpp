/****************************************************************************
** Meta object code from reading C++ file 'export_dialogbox.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../export_dialog_pages/export_dialogbox.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'export_dialogbox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Export_DialogBox_t {
    QByteArrayData data[9];
    char stringdata0[144];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Export_DialogBox_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Export_DialogBox_t qt_meta_stringdata_Export_DialogBox = {
    {
QT_MOC_LITERAL(0, 0, 16), // "Export_DialogBox"
QT_MOC_LITERAL(1, 17, 25), // "on_listWidget_itemClicked"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(4, 61, 4), // "item"
QT_MOC_LITERAL(5, 66, 32), // "on_listWidget_currentItemChanged"
QT_MOC_LITERAL(6, 99, 7), // "current"
QT_MOC_LITERAL(7, 107, 8), // "previous"
QT_MOC_LITERAL(8, 116, 27) // "on_pushButton_Close_clicked"

    },
    "Export_DialogBox\0on_listWidget_itemClicked\0"
    "\0QListWidgetItem*\0item\0"
    "on_listWidget_currentItemChanged\0"
    "current\0previous\0on_pushButton_Close_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Export_DialogBox[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x08 /* Private */,
       5,    2,   32,    2, 0x08 /* Private */,
       8,    0,   37,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    6,    7,
    QMetaType::Void,

       0        // eod
};

void Export_DialogBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Export_DialogBox *_t = static_cast<Export_DialogBox *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_listWidget_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 1: _t->on_listWidget_currentItemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QListWidgetItem*(*)>(_a[2]))); break;
        case 2: _t->on_pushButton_Close_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject Export_DialogBox::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Export_DialogBox.data,
      qt_meta_data_Export_DialogBox,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Export_DialogBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Export_DialogBox::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Export_DialogBox.stringdata0))
        return static_cast<void*>(const_cast< Export_DialogBox*>(this));
    return QDialog::qt_metacast(_clname);
}

int Export_DialogBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
