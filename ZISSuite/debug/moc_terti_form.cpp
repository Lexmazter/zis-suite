/****************************************************************************
** Meta object code from reading C++ file 'terti_form.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../terti_form.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'terti_form.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_terti_form_t {
    QByteArrayData data[14];
    char stringdata0[207];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_terti_form_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_terti_form_t qt_meta_stringdata_terti_form = {
    {
QT_MOC_LITERAL(0, 0, 10), // "terti_form"
QT_MOC_LITERAL(1, 11, 11), // "sortChanged"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 5), // "index"
QT_MOC_LITERAL(4, 30, 13), // "Qt::SortOrder"
QT_MOC_LITERAL(5, 44, 5), // "order"
QT_MOC_LITERAL(6, 50, 25), // "on_actionDelete_triggered"
QT_MOC_LITERAL(7, 76, 22), // "on_actionAdd_triggered"
QT_MOC_LITERAL(8, 99, 26), // "on_actionRefresh_triggered"
QT_MOC_LITERAL(9, 126, 23), // "on_actionSave_triggered"
QT_MOC_LITERAL(10, 150, 16), // "on_filterChanged"
QT_MOC_LITERAL(11, 167, 6), // "column"
QT_MOC_LITERAL(12, 174, 5), // "value"
QT_MOC_LITERAL(13, 180, 26) // "on_tableView_doubleClicked"

    },
    "terti_form\0sortChanged\0\0index\0"
    "Qt::SortOrder\0order\0on_actionDelete_triggered\0"
    "on_actionAdd_triggered\0"
    "on_actionRefresh_triggered\0"
    "on_actionSave_triggered\0on_filterChanged\0"
    "column\0value\0on_tableView_doubleClicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_terti_form[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,   49,    2, 0x08 /* Private */,
       6,    0,   54,    2, 0x08 /* Private */,
       7,    0,   55,    2, 0x08 /* Private */,
       8,    0,   56,    2, 0x08 /* Private */,
       9,    0,   57,    2, 0x08 /* Private */,
      10,    2,   58,    2, 0x08 /* Private */,
      13,    1,   63,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, 0x80000000 | 4,    3,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   11,   12,
    QMetaType::Void, QMetaType::QModelIndex,    3,

       0        // eod
};

void terti_form::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        terti_form *_t = static_cast<terti_form *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sortChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< Qt::SortOrder(*)>(_a[2]))); break;
        case 1: _t->on_actionDelete_triggered(); break;
        case 2: _t->on_actionAdd_triggered(); break;
        case 3: _t->on_actionRefresh_triggered(); break;
        case 4: _t->on_actionSave_triggered(); break;
        case 5: _t->on_filterChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->on_tableView_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject terti_form::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_terti_form.data,
      qt_meta_data_terti_form,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *terti_form::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *terti_form::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_terti_form.stringdata0))
        return static_cast<void*>(const_cast< terti_form*>(this));
    return QWidget::qt_metacast(_clname);
}

int terti_form::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
