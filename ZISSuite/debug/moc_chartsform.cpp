/****************************************************************************
** Meta object code from reading C++ file 'chartsform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../chartsform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'chartsform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_chartsForm_t {
    QByteArrayData data[10];
    char stringdata0[123];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_chartsForm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_chartsForm_t qt_meta_stringdata_chartsForm = {
    {
QT_MOC_LITERAL(0, 0, 10), // "chartsForm"
QT_MOC_LITERAL(1, 11, 11), // "dataChanged"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 4), // "data"
QT_MOC_LITERAL(4, 29, 13), // "heightChanged"
QT_MOC_LITERAL(5, 43, 6), // "height"
QT_MOC_LITERAL(6, 50, 12), // "widthChanged"
QT_MOC_LITERAL(7, 63, 5), // "width"
QT_MOC_LITERAL(8, 69, 21), // "on_toolButton_clicked"
QT_MOC_LITERAL(9, 91, 31) // "on_pushButtonAddDataset_clicked"

    },
    "chartsForm\0dataChanged\0\0data\0heightChanged\0"
    "height\0widthChanged\0width\0"
    "on_toolButton_clicked\0"
    "on_pushButtonAddDataset_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_chartsForm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       4,    1,   42,    2, 0x06 /* Public */,
       6,    1,   45,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   48,    2, 0x08 /* Private */,
       9,    0,   49,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void chartsForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        chartsForm *_t = static_cast<chartsForm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dataChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->heightChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->widthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_toolButton_clicked(); break;
        case 4: _t->on_pushButtonAddDataset_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (chartsForm::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&chartsForm::dataChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (chartsForm::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&chartsForm::heightChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (chartsForm::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&chartsForm::widthChanged)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject chartsForm::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_chartsForm.data,
      qt_meta_data_chartsForm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *chartsForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *chartsForm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_chartsForm.stringdata0))
        return static_cast<void*>(const_cast< chartsForm*>(this));
    return QWidget::qt_metacast(_clname);
}

int chartsForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void chartsForm::dataChanged(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void chartsForm::heightChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void chartsForm::widthChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
