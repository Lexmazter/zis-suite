/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[23];
    char stringdata0[475];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 15), // "AddWidgetToMain"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 8), // "QWidget*"
QT_MOC_LITERAL(4, 37, 9), // "toBeAdded"
QT_MOC_LITERAL(5, 47, 4), // "icon"
QT_MOC_LITERAL(6, 52, 5), // "label"
QT_MOC_LITERAL(7, 58, 23), // "on_button_about_clicked"
QT_MOC_LITERAL(8, 82, 29), // "on_button_nomenclator_clicked"
QT_MOC_LITERAL(9, 112, 33), // "on_expand_collapse_button_cli..."
QT_MOC_LITERAL(10, 146, 21), // "on_toolButton_clicked"
QT_MOC_LITERAL(11, 168, 32), // "on_toolButton_ExportFile_clicked"
QT_MOC_LITERAL(12, 201, 32), // "on_toolButton_ImportFile_clicked"
QT_MOC_LITERAL(13, 234, 31), // "on_toolButton_PrintFile_clicked"
QT_MOC_LITERAL(14, 266, 28), // "on_toolButtonIntrari_clicked"
QT_MOC_LITERAL(15, 295, 16), // "slotChoseFacturi"
QT_MOC_LITERAL(16, 312, 15), // "slotChoseBonuri"
QT_MOC_LITERAL(17, 328, 22), // "on_button_help_clicked"
QT_MOC_LITERAL(18, 351, 28), // "on_toolButtonCodBare_clicked"
QT_MOC_LITERAL(19, 380, 35), // "on_tabWidget_Open_tabCloseReq..."
QT_MOC_LITERAL(20, 416, 5), // "index"
QT_MOC_LITERAL(21, 422, 28), // "on_toolButtonGrafice_clicked"
QT_MOC_LITERAL(22, 451, 23) // "on_toolButton_2_clicked"

    },
    "MainWindow\0AddWidgetToMain\0\0QWidget*\0"
    "toBeAdded\0icon\0label\0on_button_about_clicked\0"
    "on_button_nomenclator_clicked\0"
    "on_expand_collapse_button_clicked\0"
    "on_toolButton_clicked\0"
    "on_toolButton_ExportFile_clicked\0"
    "on_toolButton_ImportFile_clicked\0"
    "on_toolButton_PrintFile_clicked\0"
    "on_toolButtonIntrari_clicked\0"
    "slotChoseFacturi\0slotChoseBonuri\0"
    "on_button_help_clicked\0"
    "on_toolButtonCodBare_clicked\0"
    "on_tabWidget_Open_tabCloseRequested\0"
    "index\0on_toolButtonGrafice_clicked\0"
    "on_toolButton_2_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    3,   94,    2, 0x08 /* Private */,
       7,    0,  101,    2, 0x08 /* Private */,
       8,    0,  102,    2, 0x08 /* Private */,
       9,    0,  103,    2, 0x08 /* Private */,
      10,    0,  104,    2, 0x08 /* Private */,
      11,    0,  105,    2, 0x08 /* Private */,
      12,    0,  106,    2, 0x08 /* Private */,
      13,    0,  107,    2, 0x08 /* Private */,
      14,    0,  108,    2, 0x08 /* Private */,
      15,    0,  109,    2, 0x08 /* Private */,
      16,    0,  110,    2, 0x08 /* Private */,
      17,    0,  111,    2, 0x08 /* Private */,
      18,    0,  112,    2, 0x08 /* Private */,
      19,    1,  113,    2, 0x08 /* Private */,
      21,    0,  116,    2, 0x08 /* Private */,
      22,    0,  117,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QIcon, QMetaType::QString,    4,    5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->AddWidgetToMain((*reinterpret_cast< QWidget*(*)>(_a[1])),(*reinterpret_cast< const QIcon(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 1: _t->on_button_about_clicked(); break;
        case 2: _t->on_button_nomenclator_clicked(); break;
        case 3: _t->on_expand_collapse_button_clicked(); break;
        case 4: _t->on_toolButton_clicked(); break;
        case 5: _t->on_toolButton_ExportFile_clicked(); break;
        case 6: _t->on_toolButton_ImportFile_clicked(); break;
        case 7: _t->on_toolButton_PrintFile_clicked(); break;
        case 8: _t->on_toolButtonIntrari_clicked(); break;
        case 9: _t->slotChoseFacturi(); break;
        case 10: _t->slotChoseBonuri(); break;
        case 11: _t->on_button_help_clicked(); break;
        case 12: _t->on_toolButtonCodBare_clicked(); break;
        case 13: _t->on_tabWidget_Open_tabCloseRequested((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->on_toolButtonGrafice_clicked(); break;
        case 15: _t->on_toolButton_2_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QWidget* >(); break;
            }
            break;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
