#ifndef configdocuments_dialog_H
#define configdocuments_dialog_H

#include <QDialog>
#include<QLabel>
#include "Forms/billseries_form.h"
#include "Forms/vat_form.h"
#include "infocodfiscalhelper.h"
#include "Delegates/colordelegate.h"

namespace Ui {
class ConfigDocuments_Dialog;
}

class ConfigDocuments_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConfigDocuments_Dialog(QWidget *parent = 0);
    ~ConfigDocuments_Dialog();
    void updateParamsShowedOnBillsFromDB(void);
    void updateBillSeriesFromDB(void);
    void updateVATFormFromDB(void);

private slots:

    void on_btn_Save_clicked();

    void on_btn_Exit_clicked();

    void on_tabwidget_Firm_currentChanged(int index);

private:
    Ui::ConfigDocuments_Dialog *ui;
    QVariantMap *documentsConfiguration_Map=nullptr;
    bool DB_Table_Empty_b;
    BillSeriesForm *p_BillSeriesForm = nullptr;
    VATForm *p_VATForm = nullptr;
};

#endif // configdocuments_dialog_H
