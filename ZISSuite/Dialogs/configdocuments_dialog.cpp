#include <QFileDialog>
#include "configdocuments_dialog.h"
#include "ui_configdocuments_dialog.h"
#include "dbhelper.h"
#include "textformat_helper.h"
#include "CustomObjects/combobox_helper.h"

ConfigDocuments_Dialog::ConfigDocuments_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDocuments_Dialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Configurare Documente");
    QIcon dialog_icon(":/Icons/Edit Property-48.png");
    this->setWindowIcon(dialog_icon);
    documentsConfiguration_Map = new QVariantMap;
    DB_Table_Empty_b = true;
    updateParamsShowedOnBillsFromDB();
    updateBillSeriesFromDB();
    updateVATFormFromDB();
    this->setAttribute(Qt::WA_DeleteOnClose);
}

ConfigDocuments_Dialog::~ConfigDocuments_Dialog()
{
    delete ui;
    if(documentsConfiguration_Map != nullptr)
        delete documentsConfiguration_Map;
}

void ConfigDocuments_Dialog::updateParamsShowedOnBillsFromDB()
{
    bool retVal = DbHelper::queryToMap("DateAfisateInDocumente", "*", NULL, documentsConfiguration_Map);
    if(retVal == true){
        DB_Table_Empty_b = false;

        ui->checkBox_Tel->setChecked(documentsConfiguration_Map->value("Telefon").toBool());
        ui->checkBox_Fax->setChecked(documentsConfiguration_Map->value("Fax").toBool());
        ui->checkBox_Email->setChecked(documentsConfiguration_Map->value("Email").toBool());
        ui->checkBox_Web->setChecked(documentsConfiguration_Map->value("Web").toBool());
        ui->checkBox_BckDoc->setChecked(documentsConfiguration_Map->value("Fundal").toBool());
        ui->checkBox_Logo->setChecked(documentsConfiguration_Map->value("Logo").toBool());
        ui->checkBox_Stamp->setChecked(documentsConfiguration_Map->value("Stampila").toBool());
        ui->checkBox_AutofillDate->setChecked(documentsConfiguration_Map->value("AutocompletareDataExp").toBool());
        ui->checkBox_DueDate->setChecked(documentsConfiguration_Map->value("DataScadentaEmitere").toBool());
        ui->checkBox_Eur->setChecked(documentsConfiguration_Map->value("Eur").toBool());
        ui->checkBox_Usd->setChecked(documentsConfiguration_Map->value("Usd").toBool());
        ui->checkBox_GBP->setChecked(documentsConfiguration_Map->value("Gbp").toBool());
        ui->checkBox_Cad->setChecked(documentsConfiguration_Map->value("Cad").toBool());
        ui->checkBox_Aud->setChecked(documentsConfiguration_Map->value("Aud").toBool());
        ui->checkBox_Chf->setChecked(documentsConfiguration_Map->value("Chf").toBool());
        ui->checkBox_Huf->setChecked(documentsConfiguration_Map->value("Huf").toBool());
        ui->checkBox_Bgn->setChecked(documentsConfiguration_Map->value("Bgn").toBool());
        ui->checkBox_Rsd->setChecked(documentsConfiguration_Map->value("Rsd").toBool());
        ui->checkBox_Mdl->setChecked(documentsConfiguration_Map->value("Mdl").toBool());
        ui->checkBox_Pln->setChecked(documentsConfiguration_Map->value("Pln").toBool());
        ui->checkBox_Czk->setChecked(documentsConfiguration_Map->value("Czk").toBool());
        ui->checkBox_Try->setChecked(documentsConfiguration_Map->value("Try").toBool());
        ui->checkBox_Sek->setChecked(documentsConfiguration_Map->value("Sek").toBool());
        ui->checkBox_Dkk->setChecked(documentsConfiguration_Map->value("Dkk").toBool());
        ui->checkBox_Nok->setChecked(documentsConfiguration_Map->value("Nok").toBool());
        ui->checkBox_Jpy->setChecked(documentsConfiguration_Map->value("Jpy").toBool());
        ui->checkBox_Rub->setChecked(documentsConfiguration_Map->value("Rub").toBool());


        /* update the status label */
        QString line = "Selectati datele care doriti sa apara pe documentele tiparite";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_info);
        ui->label_Info->setText(line);
    }
    else
    {

        QString line = "Nu a fost salvata nicio configuratie a datelor tiparite pe facturi";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_alert);
        ui->label_Info->setText(line);
    }
}

void ConfigDocuments_Dialog::updateBillSeriesFromDB()
{
    if(p_BillSeriesForm != nullptr)
        delete p_BillSeriesForm;

    p_BillSeriesForm = new BillSeriesForm (this);
    ui->gridLayout_DocuSeries->addWidget(p_BillSeriesForm);
    QString line = "Seriile de facturi folosesc pentru a sorta si identifica usor facturile utilizate";
    TextFormat_Helper lineFormatter;
    lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_info);
    ui->label_Info->setText(line);
}

void ConfigDocuments_Dialog::updateVATFormFromDB()
{
    if(p_VATForm != nullptr)
        delete p_VATForm;
    p_VATForm = new VATForm (this);
    ui->gridLayout_TVA->addWidget(p_VATForm);
    QString line = "Aceasta fereastra ve permite sa configurati cotele TVA care vor fi afisate pe facturi";
    TextFormat_Helper lineFormatter;
    lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_info);
    ui->label_Info->setText(line);
}

void ConfigDocuments_Dialog::on_btn_Save_clicked()
{
    p_BillSeriesForm->on_actionSave_triggered();
    p_VATForm->on_actionSave_triggered();

    if(!documentsConfiguration_Map->isEmpty())
        delete documentsConfiguration_Map;
    documentsConfiguration_Map = new  QVariantMap;

    bool retVal;
    documentsConfiguration_Map->insert("Telefon", ui->checkBox_Tel->isChecked() ? 1 : 0);
    documentsConfiguration_Map->insert("Fax", ui->checkBox_Fax->isChecked() ? 1 : 0);
    documentsConfiguration_Map->insert("Email", ui->checkBox_Email->isChecked() ? 1 : 0);
    documentsConfiguration_Map->insert("Web", ui->checkBox_Web->isChecked() ? 1 : 0);
    documentsConfiguration_Map->insert("Fundal", ui->checkBox_BckDoc->isChecked() ? 1 : 0);
    documentsConfiguration_Map->insert("Logo", ui->checkBox_Logo->isChecked() ? 1 : 0);
    documentsConfiguration_Map->insert("Stampila", ui->checkBox_Stamp->isChecked());
    documentsConfiguration_Map->insert("AutocompletareDataExp", ui->checkBox_AutofillDate->isChecked());
    documentsConfiguration_Map->insert("DataScadentaEmitere", ui->checkBox_DueDate->isChecked());
    documentsConfiguration_Map->insert("Eur", ui->checkBox_Eur->isChecked());
    documentsConfiguration_Map->insert("Usd", ui->checkBox_Usd->isChecked());
    documentsConfiguration_Map->insert("Gbp", ui->checkBox_GBP->isChecked());
    documentsConfiguration_Map->insert("Cad", ui->checkBox_Cad->isChecked());
    documentsConfiguration_Map->insert("Aud", ui->checkBox_Aud->isChecked());
    documentsConfiguration_Map->insert("Chf", ui->checkBox_Chf->isChecked());
    documentsConfiguration_Map->insert("Huf", ui->checkBox_Huf->isChecked());
    documentsConfiguration_Map->insert("Bgn", ui->checkBox_Bgn->isChecked());
    documentsConfiguration_Map->insert("Rsd", ui->checkBox_Rsd->isChecked());
    documentsConfiguration_Map->insert("Mdl", ui->checkBox_Mdl->isChecked());
    documentsConfiguration_Map->insert("Pln", ui->checkBox_Pln->isChecked());
    documentsConfiguration_Map->insert("Czk", ui->checkBox_Czk->isChecked());
    documentsConfiguration_Map->insert("Try", ui->checkBox_Try->isChecked());
    documentsConfiguration_Map->insert("Sek", ui->checkBox_Sek->isChecked());
    documentsConfiguration_Map->insert("Dkk", ui->checkBox_Dkk->isChecked());
    documentsConfiguration_Map->insert("Nok", ui->checkBox_Nok->isChecked());
    documentsConfiguration_Map->insert("Jpy", ui->checkBox_Jpy->isChecked());
    documentsConfiguration_Map->insert("Rub", ui->checkBox_Rub->isChecked());
    if(DB_Table_Empty_b == true)
        retVal = DbHelper::insertSQLFromMap("DateAfisateInDocumente", documentsConfiguration_Map);
    else
        retVal = DbHelper::updateSQLFromMap("DateAfisateInDocumente", documentsConfiguration_Map, NULL);
    if(retVal)
    {

        QString line = "Datele firmei au fost salvate cu succes!";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_info);
        ui->label_Info->setText(line);

    }
    else
    {
        QString line = "Eroare la salvarea datelor!";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_alert);
        ui->label_Info->setText(line);
    }

}

void ConfigDocuments_Dialog::on_btn_Exit_clicked()
{
    this->close();
}

void ConfigDocuments_Dialog::on_tabwidget_Firm_currentChanged(int index)
{
    if(index == 0)
        updateBillSeriesFromDB();
    else if(index == 1)
        updateVATFormFromDB();
    else if(index == 2)
        updateParamsShowedOnBillsFromDB();
}
