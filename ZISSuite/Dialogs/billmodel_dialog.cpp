#include "billmodel_dialog.h"
#include "ui_billmodel_dialog.h"

BillModel_Dialog::BillModel_Dialog(QWidget *parent, int storedItem) :
    QDialog(parent)
  , scaleFactor(1),
    ui(new Ui::BillModel_Dialog)
{
    ui->setupUi(this);
    ui->listWidget->setViewMode(QListWidget::ListMode);
    ui->listWidget->setIconSize(QSize(100,150));
    ui->listWidget->setResizeMode(QListWidget::Adjust);
    ui->listWidget->setBackgroundRole(QPalette::Base);
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/Pictures/BillTemplate_0.jpg"),"Factura1"));
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/Pictures/BillTemplate_1.jpg"),"Factura2"));
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/Pictures/BillTemplate_2.jpg"),"Factura3"));
    p_ActualSelectedImage_Label = new CustomLabel(ui->imageLabel, ":/Pictures/BillTemplate_" + QString::number(storedItem) + ".jpg");
    ui->listWidget->setCurrentRow(storedItem);


    ui->imageLabel->setBackgroundRole(QPalette::Dark);
    // get our context menu up and running, last added appears first
    ui->imageLabel->insertAction(NULL,ui->actionZoomIn);
    ui->imageLabel->insertAction(ui->actionZoomIn, ui->actionZoomOut);
    ui->imageLabel->insertAction(ui->actionZoomOut,ui->actionNormalSize);
    ui->imageLabel->insertAction(ui->actionNormalSize, ui->actionFitToWindow);
    ui->infoLabel->setText("Modelul ales va fi foloit la toate facturile cu seria specificata anterior");
}

BillModel_Dialog::~BillModel_Dialog()
{
    delete ui;
}

void BillModel_Dialog::updateActions()
{
    ui->actionZoomIn->setEnabled(!ui->actionFitToWindow->isChecked());
    ui->actionZoomOut->setEnabled(!ui->actionFitToWindow->isChecked());
    ui->actionNormalSize->setEnabled(!ui->actionFitToWindow->isChecked());
}

void BillModel_Dialog::scaleImage(double factor)
{
    Q_ASSERT(p_ActualSelectedImage_Label->pixmap());
    scaleFactor *= factor;
    p_ActualSelectedImage_Label->resize(scaleFactor * p_ActualSelectedImage_Label->pixmap()->size());
    ui->actionZoomIn->setEnabled(scaleFactor < 3.0);
    ui->actionZoomOut->setEnabled(scaleFactor > 0.333);
}

void BillModel_Dialog::on_actionZoomIn_triggered()
{
    scaleImage(1.25);
}

void BillModel_Dialog::on_actionZoomOut_triggered()
{
    scaleImage(0.8);
}

void BillModel_Dialog::on_actionNormalSize_triggered()
{
    p_ActualSelectedImage_Label->adjustSize();
    scaleFactor = 1.0;
}

void BillModel_Dialog::on_actionFitToWindow_triggered()
{
    bool fitToWindow = ui->actionFitToWindow->isChecked();
    if (!fitToWindow)
        on_actionNormalSize_triggered();
    updateActions();
}

//bool BillModel_Dialog::loadFile(const QString &fileName)
//{
//    QImageReader reader(fileName);
//    reader.setAutoTransform(true);
//    const QImage newImage = reader.read();
//    if (newImage.isNull()) {
//        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
//                                 tr("Cannot load %1: %2")
//                                 .arg(QDir::toNativeSeparators(fileName), reader.errorString()));
//        return false;
//    }

//    setImage(newImage);

//    setWindowFilePath(fileName);

//    const QString message = tr("Opened \"%1\", %2x%3, Depth: %4")
//            .arg(QDir::toNativeSeparators(fileName)).arg(image.width()).arg(image.height()).arg(image.depth());
//    statusBar()->showMessage(message);
//    return true;
//}

//void BillModel_Dialog::setImage(const QImage &newImage)
//{
//    image = newImage;
//    ui->imageLabel_1->setPixmap(QPixmap::fromImage(image));

//    scaleFactor = 1.0;

//    ui->scrollArea->setVisible(true);
//    printAct->setEnabled(true);
//    fitToWindowAct->setEnabled(true);
//    updateActions();

//    if (!fitToWindowAct->isChecked())
//        ui->imageLabel_1->adjustSize();
//}

//static void initializeImageFileDialog(QFileDialog &dialog, QFileDialog::AcceptMode acceptMode)
//{
//    static bool firstDialog = true;

//    if (firstDialog) {
//        firstDialog = false;
//        const QStringList picturesLocations = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation);
//        dialog.setDirectory(picturesLocations.isEmpty() ? QDir::currentPath() : picturesLocations.last());
//    }

//    QStringList mimeTypeFilters;
//    const QByteArrayList supportedMimeTypes = acceptMode == QFileDialog::AcceptOpen
//            ? QImageReader::supportedMimeTypes() : QImageWriter::supportedMimeTypes();
//    foreach (const QByteArray &mimeTypeName, supportedMimeTypes)
//        mimeTypeFilters.append(mimeTypeName);
//    mimeTypeFilters.sort();
//    dialog.setMimeTypeFilters(mimeTypeFilters);
//    dialog.selectMimeTypeFilter("image/jpeg");
//    if (acceptMode == QFileDialog::AcceptSave)
//        dialog.setDefaultSuffix("jpg");
//}

void BillModel_Dialog::emitSaveBillModelNr()
{
    emit signalSaveBillModelNr(BillModel_NR);
}

int BillModel_Dialog::getBillModel_NR()
{
    return BillModel_NR;
}

void BillModel_Dialog::on_listWidget_currentRowChanged(int currentRow)
{
    BillModel_NR = currentRow;
    if(currentRow == 0)
    {
        p_ActualSelectedImage_Label->updateImage(":/Pictures/BillTemplate_0.jpg");
    }
    else if(currentRow == 1)
    {
        p_ActualSelectedImage_Label->updateImage(":/Pictures/BillTemplate_1.jpg");
    }
    else if(currentRow == 2)
    {
        p_ActualSelectedImage_Label->updateImage(":/Pictures/BillTemplate_2.jpg");
    }
}

void BillModel_Dialog::on_btn_Close_clicked()
{
    this->close();
}
