#ifndef BILLMODEL_DIALOG_H
#define BILLMODEL_DIALOG_H

#include <QDialog>
#include <QScrollBar>
#include <QLabel>
#include "CustomObjects/customlabel.h"
#include <QListWidgetItem>

namespace Ui {
class BillModel_Dialog;
}

class BillModel_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit BillModel_Dialog(QWidget *parent = 0, int storedItem = 0);
    ~BillModel_Dialog();
    int getBillModel_NR();

signals: void signalSaveBillModelNr(int modelNr);

private slots:

    void on_actionZoomIn_triggered();

    void on_actionZoomOut_triggered();

    void on_actionNormalSize_triggered();

    void on_actionFitToWindow_triggered();

    //    void onImageClicked(QLabel*);

    void on_listWidget_currentRowChanged(int currentRow);

    void on_btn_Close_clicked();

private:
    Ui::BillModel_Dialog *ui;
    void updateActions();
    void scaleImage(double factor);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);
    void emitSaveBillModelNr();

    double scaleFactor;
    CustomLabel *p_ActualSelectedImage_Label = nullptr;
    int BillModel_NR=0;

};

#endif // BILLMODEL_DIALOG_H
