#ifndef BONURIPRODUSE_DIALOG_H
#define BONURIPRODUSE_DIALOG_H

#include <QDialog>
#include <QWidget>
#include "dbhelper.h"
#include <QString>
#include <QTableView>
#include "Delegates/spinboxdelegate.h"

namespace Ui {
class BonuriProduse_Dialog;
}

class BonuriProduse_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit BonuriProduse_Dialog(QWidget *parent = 0, QTableView *p_Table=nullptr, int actualRaw=0);
    ~BonuriProduse_Dialog();

private:
    Ui::BonuriProduse_Dialog *ui;
    QAbstractItemDelegate* EmptyDelegate;
    QAbstractTableModel* customTableModel;
    QTableView *ParrentTable;
    QWidget *parent;
    QString SoldItemsOutOfParent;
    QString AmountSoldOutOfParent;
    QMap<int, float> *availableQuantityForProducts_Map;
    QMap<int, float> *changedProductsAmount;
    SpinBoxDelegate *spinBoxDelegate_Amount;
    int CurrentRaw_ParrentTable;
    int itemsIndex;
    int amountIndex;
    int totalIndex;
    int dataIndex;
    int denumireIndex;
    int tertIndex;
    int columnIndex_Pret;
    int columnIndex_Amount;

    void getProductsToModel();
    void saveSoldItemsAndQuantities(void);
    double calculateTotalBillValue(void);
private slots:

    void on_actionDelete_triggered();

    void on_actionAdd_triggered();

    void on_actionRefresh_triggered();

    void on_actionSave_triggered();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_tableView_clicked(const QModelIndex &index);
    void onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
};

#endif // BONURIPRODUSE_DIALOG_H
