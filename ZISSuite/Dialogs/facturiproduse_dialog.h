#ifndef VANZARIPRODUSEDIALOG_H
#define VANZARIPRODUSEDIALOG_H

#include <QDialog>
#include <QWidget>
#include "dbhelper.h"
#include <QString>
#include <QTableView>
#include <QMenu>

#include "Delegates/spinboxdelegate.h"
#include "cutereport_helper.h"
#include "firmsettings_helper.h"
#include "Forms/newthird_form.h"

namespace Ui {
class FacturiProduseDialog;
}

class FacturiProduseDialog : public QDialog
{
    Q_OBJECT

public:

    explicit FacturiProduseDialog(QWidget *parent = 0, QTableView *p_Table=nullptr, int actualRaw=0);
    ~FacturiProduseDialog();

private:
    Ui::FacturiProduseDialog *ui;
    QAbstractItemDelegate* EmptyDelegate;
    QAbstractTableModel* customTableModel;
    QTableView *ParrentTable;
    QWidget *parent;
    int CurrentRaw_ParrentTable;
    int parent_IDIndex;
    int parent_ItemsIndex;
    int parent_AmountIndex;
    int parent_TotalIndex;
    int parent_DataIndex;
    int parent_DataScadentaIndex;
    int parent_DenumireIndex;
    int parent_TertIndex;
    int parent_SerieIndex;
    int parent_AchitatIndex;
    int Bill_ID = 0;
    QString SoldItemsOutOfParent;
    QString AmountSoldOutOfParent;

    int columnIndex_ID;
    int columnIndex_Denumire;
    int columnIndex_UM;
    int columnIndex_Pret;
    int columnIndex_Amount;
    int columnIndex_Stoc;
    QMap<int, float> *availableQuantityForProducts_Map;
    QMap<int, float> *changedProductsAmount;
    SpinBoxDelegate *spinBoxDelegate_Amount;
    CuteReport_Helper *reportHelper = nullptr;
    QMenu *contextMenu;
    NewThird_Form *NewThirdForm=nullptr;

    void getProductsToModel();
    void saveSoldItemsAndQuantitiesToParent(void);
    double calculateTotalBillValue(void);
    void setContextMenuActionsForTable();
private slots:

    void onDataChanged(const QModelIndex& ,const QModelIndex&);

    void on_actionDelete_triggered();

    void on_actionAdd_triggered();

    void on_actionRefresh_triggered();

    void on_actionSave_triggered();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_tableView_customContextMenuRequested(const QPoint &pos);

public slots:
    void on_actionPrintBill_triggered();
    void on_actionProformaInvoice_triggered();
    void on_actionReceipt_triggered();
    void on_btnAddThird_clicked();
};

#endif // VANZARIPRODUSEDIALOG_H
