#include <QFileDialog>
#include "firmdetails_dialog.h"
#include "ui_firmdetails_dialog.h"
#include "dbhelper.h"
#include "textformat_helper.h"
#include "firmsettings_helper.h"
#include "CustomObjects/combobox_helper.h"

const QString FirmDetails_Dialog::FirmID = "1";

FirmDetails_Dialog::FirmDetails_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FirmDetails_Dialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Informatii firma");
    QIcon dialog_icon(":/Icons/Dossier-48.png");
    this->setWindowIcon(dialog_icon);
    firmData = new QVariantMap;
    DB_Table_Empty_b = true;
    updateFrimDatafromDB();
    updateBankAccountsFromDB();
    this->setAttribute(Qt::WA_DeleteOnClose);
}

FirmDetails_Dialog::~FirmDetails_Dialog()
{
    delete ui;
    if(firmData != nullptr)
        delete firmData;
}

void FirmDetails_Dialog::updateFrimDatafromDB()
{
    QString conditionalQuery = "ID = " + FirmID;
    bool retVal = DbHelper::queryToMap("DateFirma", "*", conditionalQuery, firmData);
    if(retVal == true){
        DB_Table_Empty_b = false;
        /* mandatory fields */
        ui->lineEdit_FirmName->setText(firmData->value("Denumire").toString());
        ui->lineEdit_CIFData->setText(firmData->value("CIF").toString());
        ui->lineEdit_RegData->setText(firmData->value("RegComert").toString());
        ui->lineEdit_LocalData->setText(firmData->value("Localitate").toString());

        //set up the ComboBox
        ComboBoxHelper *combobox_helper = new ComboBoxHelper();
        QString tempString = firmData->value("Judet").toString();
        combobox_helper->setUpComboBox(ui->comboBox_Judet, COMBOBOX_JUDET);
        ui->comboBox_Judet->setCurrentIndex(combobox_helper->indexOf(tempString.toLower()));

        ui->lineEdit_AdrData->setText(firmData->value("Adresa").toString());
        ui->lineEdit_CapitalData->setText(firmData->value("Capital").toString());
        if(firmData->value("PlatitorTVA").toInt() == 1)
            ui->btnPlatitorTVA_YES->setChecked(true);
        else
            ui->btnPlatitorTVA_NO->setChecked(true);
        if(firmData->value("ImpozitProfit").toInt() == 1)
            ui->btnProfitContribution_YES->setChecked(true);
        else
            ui->btnProfitContribution_NO->setChecked(true);
        /* optional fields */
        ui->lineEdit_Tel->setText(firmData->value("Telefon").toString());
        ui->lineEdit_Fax->setText(firmData->value("Fax").toString());
        ui->lineEdit_Email->setText(firmData->value("Email").toString());
        ui->lineEdit_Web->setText(firmData->value("Web").toString());
        ui->lineEdit_Additional->setText(firmData->value("Aditionale").toString());
        /* logo image */
        QByteArray outByteArray = firmData->value("Logo").toByteArray();
        setLabelAsPixmap(ui->label_Logo_Img, outByteArray);
        /*background image */
        outByteArray = firmData->value("Fundal").toByteArray();
        setLabelAsPixmap(ui->label_Background_Img, outByteArray);
        /* stamp image */
        outByteArray = firmData->value("Stampila").toByteArray();
        setLabelAsPixmap(ui->label_Stamp_Img, outByteArray);

        /* update the status label */
        QString line = "Datele firmei pe care o conduceti.";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_info);
        ui->label_info->setText(line);
    }
    else
    {

        QString line = "Nu ati introdus inca datele firmei. Pentru o functionare optima introduceti cel putin datele obligatorii!";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_info);
        ui->label_info->setText(line);
    }
}

void FirmDetails_Dialog::updateBankAccountsFromDB()
{
    //bring up the bank accounts into the second tab
    bankForm = new BankAccountsForm (this);
    ui->layout_Accounts->addWidget(bankForm);
    QString line = "In tabel puteti adauga cate conturi bancare aveti nevoie. Implicit contul este bifat ca si \"Folosit\". "
                   "Toate conturile folosite vor fi afisate pe documente. Pentru a evita aglomerarea acestora bifati doar conturile necesare.";
    TextFormat_Helper lineFormatter;
    lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_info);
    ui->label_InfoAccounts->setText(line);
}

void FirmDetails_Dialog::setLabelAsPixmap(QLabel *label, QByteArray &outByteArray)
{
    QPixmap outPixmap = QPixmap();
    outPixmap.loadFromData( outByteArray );
    // get label dimensions
    int w = label->width();
    int h = label->height();
    label->setPixmap(outPixmap.scaled(w, h, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

void FirmDetails_Dialog::on_btnSave_clicked()
{
    //    if(firmData != nullptr)
    //        delete firmData;
    if(ui->lineEdit_FirmName->text().isEmpty() ||
            ui->lineEdit_CIFData->text().isEmpty() ||
            ui->lineEdit_CIFData->text().isEmpty() ||
            ui->lineEdit_RegData->text().isEmpty() ||
            ui->lineEdit_LocalData->text().isEmpty() ||
            ui->comboBox_Judet->currentText().isEmpty() ||
            ui->lineEdit_AdrData->text().isEmpty() ||
            ui->lineEdit_CapitalData->text().isEmpty())

    {
        QString line = "Verificati si setati corespunzator toate datele obligatorii necompletate pentru firma dumneavoastra !";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_alert);
        ui->label_info->setText(line);
    }

    //    firmData = new QVariantMap;
    firmData->insert("ID", FirmID);
    firmData->insert("Denumire", ui->lineEdit_FirmName->text());
    firmData->insert("CIF", ui->lineEdit_CIFData->text());
    firmData->insert("RegComert", ui->lineEdit_RegData->text());
    firmData->insert("Localitate", ui->lineEdit_LocalData->text());
    firmData->insert("Judet", ui->comboBox_Judet->currentText());
    firmData->insert("Adresa", ui->lineEdit_AdrData->text());
    firmData->insert("Capital", ui->lineEdit_CapitalData->text().toDouble());
    firmData->insert("PlatitorTVA", (ui->btnPlatitorTVA_YES->isChecked())? 1 : 0);
    firmData->insert("ImpozitProfit", (ui->btnProfitContribution_YES->isChecked())? 1 : 0);
    firmData->insert("Telefon", ui->lineEdit_Tel->text());
    firmData->insert("Fax", ui->lineEdit_Fax->text());
    firmData->insert("Email", ui->lineEdit_Email->text());
    firmData->insert("Web", ui->lineEdit_Web->text());
    firmData->insert("Aditionale", ui->lineEdit_Additional->text());

    QString whereClause = "ID = " + FirmID;
    bool retVal=false;
    if(DB_Table_Empty_b == true)
    {
        retVal = DbHelper::insertSQLFromMap("DateFirma", firmData);

    }
    else
    {
        retVal = DbHelper::updateSQLFromMap("DateFirma", firmData, whereClause);
    }
    if(retVal)
    {

        QString line = "Datele firmei au fost salvate cu succes!";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_info);
        ui->label_info->setText(line);

    }
    else
    {
        QString line = "Eroare la salvarea datelor!";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_alert);
        ui->label_info->setText(line);
    }
}

void FirmDetails_Dialog::on_btnExit_clicked()
{
    this->close();
}

void FirmDetails_Dialog::on_btn_ChoseLogo_clicked()
{
    QFileDialog dialog(this);
    dialog.setViewMode(QFileDialog::Detail);
    QString fileName = dialog.getOpenFileName(this, tr("Alege Imaginea"), QDir::currentPath(), tr("*.png"));
    // Alternatively, load an image file directly into a QByteArray
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        QString line = "Imaginea selectata nu poate fi deschisa !";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_alert);
        ui->label_info->setText(line);
    }
    else
    {
        QByteArray inByteArray = file.readAll();
        firmData->insert("Logo", inByteArray);
        setLabelAsPixmap(ui->label_Logo_Img, inByteArray);
    }
}

void FirmDetails_Dialog::on_btn_DeleteLogo_clicked()
{
    ui->label_Logo_Img->clear();
    firmData->insert("Logo", NULL);
}

void FirmDetails_Dialog::on_btn_ChoseBackground_clicked()
{
    QFileDialog dialog(this);
    dialog.setViewMode(QFileDialog::Detail);
    QString fileName = dialog.getOpenFileName(this, tr("Alege Imaginea"), QDir::currentPath(), tr("*.png"));
    // Alternatively, load an image file directly into a QByteArray
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        QString line = "Imaginea selectata nu poate fi deschisa !";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_alert);
        ui->label_info->setText(line);
    }
    else
    {
        QByteArray inByteArray = file.readAll();
        firmData->insert("Fundal", inByteArray);
        setLabelAsPixmap(ui->label_Background_Img, inByteArray);
    }

}

void FirmDetails_Dialog::on_btn_DeleteBackground_clicked()
{
    ui->label_Background_Img->clear();
    firmData->insert("Fundal", NULL);
}

void FirmDetails_Dialog::on_btn_ChoseStamp_clicked()
{
    QFileDialog dialog(this);
    dialog.setViewMode(QFileDialog::Detail);
    QString fileName = dialog.getOpenFileName(this, tr("Alege Imaginea"), QDir::currentPath(), tr("*.png"));
    // Alternatively, load an image file directly into a QByteArray
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        QString line = "Imaginea selectata nu poate fi deschisa !";
        TextFormat_Helper lineFormatter;
        lineFormatter.setLineAsHTMLMsg(line, TextFormat_Helper::msg_alert);
        ui->label_info->setText(line);
    }
    else
    {
        QByteArray inByteArray = file.readAll();
        firmData->insert("Stampila", inByteArray);
        setLabelAsPixmap(ui->label_Stamp_Img, inByteArray);
    }
}

void FirmDetails_Dialog::on_btn_DeleteStamp_clicked()
{
    ui->label_Stamp_Img->clear();
    firmData->insert("Stampila", NULL);
}

void FirmDetails_Dialog::on_btn_AccountSave_clicked()
{
    bankForm->on_actionSave_triggered();
}

void FirmDetails_Dialog::on_btn_AccountExit_clicked()
{
    this->close();
}

void FirmDetails_Dialog::on_btnTakeOverInfo_clicked()
{
    QString codFiscal = ui->lineEdit_CIFData->text();

    // Get the data
    codFiscalHelper = new InfoCodFiscalHelper(this, codFiscal);

    // We can parse the data only after we get it from the web
    connect(codFiscalHelper, SIGNAL(loadFinished(bool)),
            this, SLOT(infoFiscalLoadFinished(bool)));
}

void FirmDetails_Dialog::infoFiscalLoadFinished(bool state)
{
    switch(state)
    {
    // Company exists, no error
    case true:
    {
        QHash <QString, QString> tempInfoFirm_Map = codFiscalHelper->getInfoFirma();

        ui->lineEdit_FirmName->setText(tempInfoFirm_Map.value("Denumire platitor:"));
        ui->lineEdit_Tel->setText(tempInfoFirm_Map.value("Telefon:"));
        ui->lineEdit_Fax->setText(tempInfoFirm_Map.value("Fax:"));
        ui->comboBox_Judet->setCurrentIndex(ui->comboBox_Judet->findText(tempInfoFirm_Map.value("Judetul:").toUpper(), Qt::MatchWrap));
        ui->lineEdit_AdrData->setText(tempInfoFirm_Map.value("Adresa:"));
        ui->lineEdit_RegData->setText(tempInfoFirm_Map.value("Numar de inmatriculare la RegistrulComertului:"));
        ui->lineEdit_LocalData->setText("-");
        ui->lineEdit_CapitalData->setText("0");
        ui->lineEdit_Email->setText("-");
        ui->lineEdit_Web->setText("-");

        //process the activity status and show it
        QString tempString =tempInfoFirm_Map.value("Stare societate:");
        if(tempString.contains("INREGISTRAT")){
            tempString.replace("INREGISTRAT", "ACTIV");
        }
        else if(tempString.contains("DIZOLVARE CU LICHIDARE(RADIERE)"))
        {
            tempString.replace("DIZOLVARE CU LICHIDARE(RADIERE)", "INACTIV");
        }
        ui->lineEdit_Additional->setText("Stare Activitate:" + tempString);

        //process if the third is paying VAT
        tempString =tempInfoFirm_Map.value("Taxa pe valoarea adaugata (data luarii in evidenta):");
        if(tempString.isEmpty() || tempString.contains("NU") || tempString==("-"))
            ui->btnPlatitorTVA_NO->setChecked(true);
        else
            ui->btnPlatitorTVA_YES->setChecked(true);

        //process if the third is paying contribution
        tempString =tempInfoFirm_Map.value("Impozit pe profit (data luarii in evidenta):");
        if(tempString.isEmpty() || tempString.contains("NU") || tempString==("-"))
            ui->btnProfitContribution_NO->setChecked(true);
        else
            ui->btnProfitContribution_YES->setChecked(true);

        //remove the images as well since the firm information could have been changed
        on_btn_DeleteLogo_clicked();
        on_btn_DeleteBackground_clicked();
        on_btn_DeleteStamp_clicked();

        break;
    }
        // Company does not exist, or some error happened
    default:
        break;
    }

    // Delete the object at the end
    //delete codFiscalHelper;
}
