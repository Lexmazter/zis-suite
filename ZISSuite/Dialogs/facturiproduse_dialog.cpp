#include "facturiproduse_dialog.h"
#include "ui_facturiproduse_dialog.h"
#include "Delegates/lineedit_completer_delegate.h"
#include <QCompleter>
#include <QDate>
#include "completer_lineedit.h"
#include "custommodel.h"
#include "Forms/facturiform.h"
#include "numberspeller_helper.h"
#include "Delegates/readonlydelegate.h"
#include "Forms/newthird_form.h"

FacturiProduseDialog::FacturiProduseDialog(QWidget *parent, QTableView *p_Table, int actualRaw) :
    QDialog(parent),
    ui(new Ui::FacturiProduseDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Detalii Factura");
    QIcon bill_icon(":/Icons/Vanzari-50.png");
    this->setWindowIcon(bill_icon);

    this->parent = parent;
    ParrentTable = p_Table;
    CurrentRaw_ParrentTable = actualRaw;
    customTableModel = NULL;

    // Get all the headers so we know where to put what
    QMap<QString, int> parentHeaderMap;
    for(int i=0; i < ParrentTable->horizontalHeader()->model()->columnCount(); i++)
    {
        parentHeaderMap.insert(ParrentTable->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    parent_IDIndex = parentHeaderMap.find("ID").value();
    parent_ItemsIndex = parentHeaderMap.find("Produse").value();
    parent_AmountIndex = parentHeaderMap.find("Cantitate").value();
    parent_TotalIndex = parentHeaderMap.find("Total").value();
    parent_DataIndex = parentHeaderMap.find("Data").value();
    parent_DataScadentaIndex = parentHeaderMap.find("Data Scadenta").value();
    parent_DenumireIndex = parentHeaderMap.find("Descriere").value();
    parent_TertIndex = parentHeaderMap.find("Tert").value();
    parent_SerieIndex = parentHeaderMap.find("Serie").value();
    parent_AchitatIndex = parentHeaderMap.find("Achitat").value();

    getProductsToModel();
    ui->tableView->setEditTriggers(QAbstractItemView::AllEditTriggers);
    ui->tableView->show();

    // Beautification
    ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

    //set text fields with relevant data from nomenclator
    QModelIndex index;
    QString temp_val;
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_IDIndex);
    Bill_ID = ParrentTable->model()->data(index).toInt();

    // get denumire
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_DenumireIndex);
    temp_val = ParrentTable->model()->data(index).toString();
    ui->lineEditDenumire->setText(temp_val);

    // get tert
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_TertIndex);
    temp_val = ParrentTable->model()->data(index).toString();

    //setting up the model for tert combobox
    ui->comboBoxTert->setModel(DbHelper::getModelFromQuery(ui->comboBoxTert, "Terti"));
    ui->comboBoxTert->setModelColumn(2);
    int tempIndex = ui->comboBoxTert->findText(temp_val);
    ui->comboBoxTert->setCurrentIndex(tempIndex);

    // get Serie
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_SerieIndex);
    temp_val = ParrentTable->model()->data(index).toString();

    //setting up the model for serie combobox
    ui->comboBoxSerie->setModel(DbHelper::getModelFromQuery(ui->comboBoxSerie, "SeriiFacturi"));
    ui->comboBoxSerie->setModelColumn(1);
    tempIndex = ui->comboBoxSerie->findText(temp_val);
    ui->comboBoxSerie->setCurrentIndex(tempIndex);

    // get item index
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_ItemsIndex);
    temp_val = ParrentTable->model()->data(index).toString();

    // get achitat
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_AchitatIndex);
    bool tempBool = ParrentTable->model()->data(index).toBool();

    ui->achitatCheckBox->setChecked(tempBool);

    // get data
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_DataIndex);
    temp_val = ParrentTable->model()->data(index).toString();

    QDate tempDate;

    if(!temp_val.isEmpty())
    {
        tempDate = tempDate.fromString(temp_val,"dd-MM-yyyy");
        ui->dateEdit->setDate(tempDate);
    }
    else
    {
        // set our current date as default
        ui->dateEdit->setDate(QDate::currentDate());
    }

    // get data scadenta
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_DataScadentaIndex);
    temp_val = ParrentTable->model()->data(index).toString();

    if(!temp_val.isEmpty())
    {
        tempDate = tempDate.fromString(temp_val,"dd-MM-yyyy");
        ui->dateEditScadenta->setDate(tempDate);
    }
    else
    {
        // 15 days default for data scadenta
        ui->dateEditScadenta->setDate(QDate::currentDate().addDays(15));
    }

    //grab the available products amount out of nomenclator
    availableQuantityForProducts_Map = new QMap<int, float>;
    DbHelper::getAvailableQuantityForProductsById("Nomenclator", SoldItemsOutOfParent, availableQuantityForProducts_Map);

    //after setting up the model table the horizontal header is available as well
    QMap<QString, int> actualTableHeaderMap;
    for(int i=0; i < ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        actualTableHeaderMap.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }
    columnIndex_ID = actualTableHeaderMap.value("ID");
    columnIndex_Denumire = actualTableHeaderMap.value("Denumire");
    columnIndex_UM = actualTableHeaderMap.value("UM");
    columnIndex_Pret = actualTableHeaderMap.value("Pret");
    columnIndex_Amount = actualTableHeaderMap.value("Cantitate");

    spinBoxDelegate_Amount = new SpinBoxDelegate(ui->tableView);
    ui->tableView->setItemDelegateForColumn(columnIndex_Amount, spinBoxDelegate_Amount);
    spinBoxDelegate_Amount->setEditorRange(availableQuantityForProducts_Map);
    spinBoxDelegate_Amount->setShowToolTipFlag(true);
    changedProductsAmount = new QMap<int, float>;

    //setting up the completer for products name
    QCompleter *completer = new QCompleter(this);
    completer->setParent(this);
    completer->setModel(DbHelper::getModelFromQuery(completer, "Nomenclator"));
    completer->setCompletionColumn(1);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setWrapAround(false);
    LineEditCompleterDelegate *completerDelegate = new LineEditCompleterDelegate(ui->tableView, completer);
    ui->tableView->setItemDelegateForColumn(columnIndex_Denumire, completerDelegate);

    connect(ui->tableView->model(), SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(onDataChanged(const QModelIndex&, const QModelIndex&)));
    setContextMenuActionsForTable();

    // calculate and show the total value
    ui->totalLabel->setText(QString::number(calculateTotalBillValue()));

    // hide what's this
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    // disable editors for the following columns
    ReadOnlyDelegate* dummyDelegate = new ReadOnlyDelegate(ui->tableView);

    ui->tableView->setItemDelegateForColumn(columnIndex_ID, dummyDelegate);
    ui->tableView->setItemDelegateForColumn(columnIndex_UM, dummyDelegate);
}

void FacturiProduseDialog::setContextMenuActionsForTable()
{
    contextMenu= new QMenu(ui->tableView);
    QMenu* subMenuPrintBill = new QMenu("Scoate Factura");
    subMenuPrintBill->setIcon(QIcon(":/Icons/Print-48.png"));
    subMenuPrintBill->addAction(ui->actionPrintBill);
    subMenuPrintBill->addAction(ui->actionProformaInvoice);
    subMenuPrintBill->addAction(ui->actionReceipt);
    contextMenu->addMenu(subMenuPrintBill);
    contextMenu->addAction(ui->actionAdd);
    contextMenu->addAction(ui->actionRefresh);
    contextMenu->addAction(ui->actionSave);
    contextMenu->addAction(ui->actionDelete);

    // we need this for keyboard shortcuts
    ui->tableView->addAction(ui->actionAdd);
    ui->tableView->addAction(ui->actionRefresh);
    ui->tableView->addAction(ui->actionSave);
    ui->tableView->addAction(ui->actionDelete);
    ui->tableView->addAction(ui->actionReceipt);
    ui->tableView->addAction(ui->actionProformaInvoice);
    ui->tableView->addAction(ui->actionPrintBill);
}

FacturiProduseDialog::~FacturiProduseDialog()
{
    delete ui;
    if(NewThirdForm != nullptr)
    {
        delete NewThirdForm;
    }
}

void FacturiProduseDialog::getProductsToModel()
{
    SoldItemsOutOfParent =  ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_ItemsIndex).data().toString();
    AmountSoldOutOfParent =  ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_AmountIndex).data().toString();

    QString selectClause = "ID, Denumire, UM, Pret";

    if((SoldItemsOutOfParent == "NULL") || (AmountSoldOutOfParent == NULL))
    {
        SoldItemsOutOfParent == "0";
        AmountSoldOutOfParent == "0";
    }

    QString conditionQuery = "ID IN (" + SoldItemsOutOfParent + ")"; //this string can become pretty large hence a pointer to it shoud be used

    // Prevent memory leak on refresh
    if(customTableModel != NULL)
    {
        delete customTableModel;
    }

    customTableModel = new CustomModel(ui->tableView, DbHelper::getModelFromQuery(ui->tableView, "Nomenclator", &selectClause, &conditionQuery));
    ((CustomModel*)customTableModel)->setHeaderData(0, Qt::Horizontal, &selectClause);
    ((CustomModel*)customTableModel)->insertAdditionalColumn("Cantitate", &AmountSoldOutOfParent);
    ui->tableView->setModel(customTableModel);

    // connect our signal after creation
    connect(ui->tableView->model(), SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)),
            this, SLOT(onDataChanged(const QModelIndex&, const QModelIndex&)));
}

void FacturiProduseDialog::onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight)
{   
    // Parameters are dummy only - see custommodel for details
    Q_UNUSED(topLeft);
    Q_UNUSED(bottomRight);

    ui->totalLabel->setText(QString::number(calculateTotalBillValue()));
}

void FacturiProduseDialog::on_actionDelete_triggered()
{
    // We now mark for deletion the row
    ((CustomModel*)ui->tableView->model())->removeRow(ui->tableView->currentIndex().row());

    // Now calculate the total
    ui->totalLabel->setText(QString::number(calculateTotalBillValue()));
}

void FacturiProduseDialog::on_actionAdd_triggered()
{

    ui->tableView->model()->insertRow(ui->tableView->currentIndex().row());
}

void FacturiProduseDialog::on_actionRefresh_triggered()
{
    getProductsToModel();
    ui->totalLabel->setText(QString::number(calculateTotalBillValue()));
}

void FacturiProduseDialog::on_actionSave_triggered()
{
    QString details = ui->lineEditDenumire->text();
    QVariant dat(details);
    QModelIndex index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_DenumireIndex);
    ParrentTable->model()->setData(index, dat);

    saveSoldItemsAndQuantitiesToParent();

    double billValue = calculateTotalBillValue();

    if(billValue >0){
        ui->totalLabel->setText(QString::number(billValue));
        QVariant billValueData(billValue);
        QModelIndex index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_TotalIndex);
        ParrentTable->model()->setData(index, billValueData);
    }

    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_DataIndex);
    ParrentTable->model()->setData(index, ui->dateEdit->date().toString("dd-MM-yyyy"));

    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_DataScadentaIndex);
    ParrentTable->model()->setData(index, ui->dateEditScadenta->date().toString("dd-MM-yyyy"));

    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_TertIndex);

    // Save the ID from the third comboBox, not the string
    int key = ui->comboBoxTert->model()->index(ui->comboBoxTert->currentIndex(), 0).data().toInt();
    ParrentTable->model()->setData(index, key);
    qDebug() << key;

    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_SerieIndex);

    // Save the ID from the Series comboBox, not the string
    key = ui->comboBoxSerie->model()->index(ui->comboBoxSerie->currentIndex(), 0).data().toInt();
    ParrentTable->model()->setData(index, key);
    qDebug() << key;

    // set achitat
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_AchitatIndex);
    int achitat = ui->achitatCheckBox->isChecked();
    ParrentTable->model()->setData(index, achitat);

    ((FacturiForm*)this->parent)->on_actionSave_triggered();

    //update the available amount in nomenclator
    QMap<int, float>::iterator i;
    QString setClause = "Stoc  = CASE ID";
    QString whereClause = "ID IN ( ";
    if(changedProductsAmount->size() >= 1){
        for(i = changedProductsAmount->begin(); i != changedProductsAmount->end(); ++i)
        {
            setClause += " WHEN " + QString::number(i.key()) + " THEN " + QString::number(i.value());
            whereClause += QString::number(i.key()) + " ,";
        }
        whereClause.remove(whereClause.lastIndexOf(" ,"), 2);
        setClause += " END ";
        whereClause += " )";
        DbHelper::updateQuery("Nomenclator", &setClause, &whereClause);
        availableQuantityForProducts_Map = changedProductsAmount;
    }

    // We close this dialog because when we add a new item in the parent model,
    // on refresh we get wrong data
    this->close();
}

/**
 * @abstract: void VanzariProduseDialog::calculateAndSave_TotalBillValue()
 * @brief: calculate and save the total bill value based on the sold items in the list
 */
double FacturiProduseDialog::calculateTotalBillValue()
{
    int noItems = ui->tableView->model()->rowCount();
    double billValue = 0;

    for(int i=0; i<noItems; i++)
    {
        QModelIndex index = ui->tableView->model()->index(i, columnIndex_Pret);
        double itemPrice = ui->tableView->model()->data(index).toDouble();

        index = ui->tableView->model()->index(i, columnIndex_Amount);
        double itemQuantity = ui->tableView->model()->data(index).toDouble();

        billValue += (itemPrice * itemQuantity);
    }

    return billValue;
}

/**
 * @abstract: void VanzariProduseDialog::saveSoldItems_And_Quantities()
 * @brief: save the sold items IDs and quantities into the corresponding parent table - factura
 */
void FacturiProduseDialog::saveSoldItemsAndQuantitiesToParent()
{
    int noRows = ui->tableView->model()->rowCount();
    QString productsIDs = "";
    QString productsQantity = "";

    for(int i=0; i<noRows; i++)
    {
        if(i < noRows-1)
        {
            //use the separator after each element id in order to easisly split the string when parsing
            productsIDs += ui->tableView->model()->data(ui->tableView->model()->index(i, columnIndex_ID)).toString() + ", ";
            productsQantity += ui->tableView->model()->data(ui->tableView->model()->index(i, columnIndex_Amount)).toString() + ", ";
        }
        else
        {
            //the last element in string do not need a separator
            productsIDs += ui->tableView->model()->data(ui->tableView->model()->index(i, columnIndex_ID)).toString();
            productsQantity += ui->tableView->model()->data(ui->tableView->model()->index(i, columnIndex_Amount)).toString();
        }
    }

    ParrentTable->model()->setData(ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_ItemsIndex), productsIDs, Qt::EditRole);
    ParrentTable->model()->setData(ParrentTable->model()->index(CurrentRaw_ParrentTable, parent_AmountIndex), productsQantity, Qt::EditRole);
}
void FacturiProduseDialog::on_tableView_doubleClicked(const QModelIndex &index)
{

}

void FacturiProduseDialog::on_tableView_customContextMenuRequested(const QPoint &pos)
{
    contextMenu->exec(QCursor::pos());
}

void FacturiProduseDialog::on_actionPrintBill_triggered()
{    
    if(reportHelper != nullptr)
        delete reportHelper;

    //get the corresponding model for this bill series
    int key = ui->comboBoxSerie->model()->index(ui->comboBoxSerie->currentIndex(), 0).data().toInt();
    QVariantMap BillModels_Map;
    QString selectClause = "*";
    QString conditionForQuery = "ID LIKE \"" +  QString::number(key) + "\"";
    DbHelper::queryToMap("SeriiFacturi", selectClause, conditionForQuery, &BillModels_Map);

    //based on the actual model select a corresponding template for the bill
    QString billTemplate_Path = "";
    billTemplate_Path = "Factura_Template_" + BillModels_Map.value("Model").toString() + ".qtrp";

#ifdef QT_DEBUG
    billTemplate_Path = "../../BillsTemplates/" + billTemplate_Path;
#else
    billTemplate_Path = "BillsTemplates/ + billTemplate_Path;
        #endif

    reportHelper = new CuteReport_Helper(this->parent ,billTemplate_Path, "Factura_Template");
    reportHelper->fillReportDataForBill(Bill_ID, customTableModel);
    QString totalPrice = ui->totalLabel->text();
    reportHelper->fillNamedMemo("memo_totalVal", totalPrice);
    reportHelper->renderRaportPreview(this, "Previzualizare factura");
}

void FacturiProduseDialog::on_actionProformaInvoice_triggered()
{
    if(reportHelper != nullptr)
        delete reportHelper;

#ifdef QT_DEBUG
    reportHelper = new CuteReport_Helper(this->parent, "../../BillsTemplates/FacturaProforma_Template.qtrp", "Proforma_Template");
#else
    reportHelper = new CuteReport_Helper(this->parent, "BillsTemplates/FacturaProforma_Template.qtrp", "Proforma_Template");
#endif

    reportHelper->fillReportDataForProforma(Bill_ID, customTableModel);
    QString totalPrice = ui->totalLabel->text();
    reportHelper->renderRaportPreview(this, "Previzualizare factura proforma");
}

void FacturiProduseDialog::on_actionReceipt_triggered()
{

    if(reportHelper != nullptr)
        delete reportHelper;

#ifdef QT_DEBUG
    reportHelper = new CuteReport_Helper(this->parent, "../../BillsTemplates/Chitanta_Template.qtrp", "Chitanta_Template");
#else
    reportHelper = new CuteReport_Helper(this->parent, "BillsTemplates/Chitanta_Template.qtrp", "Chitanta_Template");
#endif

    QString totalPrice = ui->totalLabel->text();
    QString dateBill = ui->dateEdit->text();
    QString currentDate = QDateTime::currentDateTime().toString("dd.MM.yyyy");
    QString billID_Formatted = QString("%1").arg(Bill_ID, 6, 'g', -1, '0');
    NumberSpeller_Helper numberToTextHelper;
    QString splledNumber = numberToTextHelper.numberToText(totalPrice.toFloat());
    QString currentThird = ui->comboBoxTert->currentText();
    QVariantMap QueryElementsForThirds;
    QString selectClause = "*";
    QString conditionForQuery = "Denumire LIKE \"" + currentThird + "\"";
    DbHelper::queryToMap("Terti", selectClause, conditionForQuery, &QueryElementsForThirds);
    reportHelper->fillReportDataForReceipt(&QueryElementsForThirds, currentDate, dateBill, totalPrice, splledNumber, billID_Formatted);

    //    reportHelper->fillNamedMemo("memo_totalVal", totalPrice);
    reportHelper->renderRaportPreview(this, "Previzualizare chitanta");
}

void FacturiProduseDialog::on_btnAddThird_clicked()
{
    if(NewThirdForm != nullptr)
        delete NewThirdForm;

    QPoint p = QCursor::pos();
    NewThirdForm = new NewThird_Form(this->ui->tableView, p.rx(), p.ry(), "client");
    NewThirdForm->show();
}
