#include "importdialog.h"
#include "ui_importdialog.h"
#include <QtConcurrent/QtConcurrent>
#include "Delegates/readonlydelegate.h"
#include "Delegates/comboboxdelegate.h"

ImportDialog::ImportDialog(QWidget *parent, QString filePath) :
    QDialog(parent),
    ui(new Ui::ImportDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Import");
    QIcon export_icon(":/Icons/Import-48.png");
    this->setWindowIcon(export_icon);

    this->filePath = filePath;

    MainWindow *mainWindow = MainWindow::getInstance();
    QTableView *currentTable = mainWindow->getCurrentTable();

    for(int i=0; i < currentTable->horizontalHeader()->model()->columnCount(); i++)
    {
        headerMap.insert(QString::number(i), currentTable->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString());
    }

    // Insert empty entry for deselecting whatever was selected before
    headerMap.insert(" ", " ");

    QXlsx::Document xlsx(filePath);

    foreach (QString sheetName, xlsx.sheetNames())
    {
        QXlsx::Worksheet *sheet = dynamic_cast<QXlsx::Worksheet *>(xlsx.sheet(sheetName));

        if (sheet)
        {
            QTableView* tempView = new QTableView(ui->tabWidget);
            tempView->verticalHeader()->setVisible(false);
            tempView->setAlternatingRowColors(true);

            ReadOnlyDelegate* dummyDelegate = new ReadOnlyDelegate(tempView);
            tempView->setItemDelegateForColumn(0, dummyDelegate);

            ComboBoxDelegate* comboDelegate = new ComboBoxDelegate(tempView, &headerMap);
            tempView->setItemDelegateForColumn(1, comboDelegate);

            QXlsx::SheetModel tempSheetModel(sheet, tempView);
            QStandardItemModel* tempModel = new QStandardItemModel(tempView);

            QModelIndex tempIndex;

            // Column for excel and for table data
            tempModel->insertColumn(0);
            tempModel->insertColumn(0);

            // Column header labels
            QStringList tempStringList;
            tempStringList.append("Coloana Excel");
            tempStringList.append("Coloana tabel curent");

            tempModel->setHorizontalHeaderLabels(tempStringList);

            // Get all the columns from excel
            for(int i = 0; i < tempSheetModel.columnCount(); i++)
            {
                tempModel->insertRow(i);

                // Get data from the sheet
                tempIndex = tempSheetModel.index(0,i);
                QVariant tempVar = tempSheetModel.data(tempIndex);

                // Set the excel columns first
                tempIndex = tempModel->index(i,0);
                tempModel->setData(tempIndex, tempVar);

                // Preselect the same name columns
                tempIndex = tempModel->index(i,1);
                tempModel->setData(tempIndex, headerMap.key(tempVar.toString()));
            }

            tempView->setModel(tempModel);
            tempView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
            tempView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

            ui->tabWidget->addTab(tempView, sheetName);
        }
    }

    // Set our loading animation
    ui->animationLabel->setVisible(false);
}

ImportDialog::~ImportDialog()
{
    delete ui;
    delete watcher;
}

void ImportDialog::on_closeButton_clicked()
{
    this->close();
}

void ImportDialog::importFunction()
{
    MainWindow *mainWindow = MainWindow::getInstance();
    QTableView *currentTable = mainWindow->getCurrentTable();

    QXlsx::Document xlsx(filePath);

    // Parse all the sheets to find which one we actually import
    foreach (QString sheetName, xlsx.sheetNames())
    {
        // If we found our sheet
        if(sheetName == ui->tabWidget->tabText(ui->tabWidget->currentIndex()))
        {
            // Start the actual import logic
            QXlsx::Worksheet *sheet = static_cast<QXlsx::Worksheet *>(xlsx.sheet(sheetName));

            if (sheet)
            {
                QXlsx::SheetModel sheetModel(sheet);

                QTableView* tempView = static_cast<QTableView *>(ui->tabWidget->currentWidget());

                if(tempView)
                {
                    int initialRowCount = currentTable->model()->rowCount();

                    for(int col = 0; col <  tempView->model()->rowCount(); col++)
                    {
                        // Get the name of the assigned table column
                        QModelIndex tempIndex = tempView->model()->index(col,1);
                        int tableColumn = tempView->model()->data(tempIndex).toInt();

                        // We skip the first row as it's used for headers
                        for(int row = 1; row < sheetModel.rowCount(); row ++)
                        {
                            // insert a row only for the first parsed column
                            if (col == 0)
                                currentTable->model()->insertRow(currentTable->model()->rowCount());

                            // Get the data from excel sheet
                            tempIndex = sheetModel.index(row, col);
                            QVariant tempVar = sheetModel.data(tempIndex);

                            // Set the data into the importing table
                            tempIndex = currentTable->model()->index(initialRowCount+(row-1), tableColumn);
                            currentTable->model()->setData(tempIndex, tempVar);
                        }
                    }
                }
            }
        }

    }
}

void ImportDialog::on_importButton_clicked()
{
    QTableView* tempView = static_cast<QTableView *>(ui->tabWidget->currentWidget());

    // Disable everything during import
    tempView->setEnabled(false);
    ui->closeButton->setEnabled(false);
    ui->importButton->setEnabled(false);

    // Show our loading animation
    ui->animationLabel->setVisible(true);
    QMovie *movie = new QMovie(this);
    movie->setFileName(":/Animations/loader.gif");
    ui->animationLabel->setMovie(movie);
    movie->start();

    // Start the import in a new thread
    watcher = new QFutureWatcher<void>();
    connect(watcher, SIGNAL(finished()), this, SLOT(handleFinished()));

    QFuture<void> future = QtConcurrent::run(this, &ImportDialog::importFunction);
    watcher->setFuture(future);
}

void ImportDialog::handleFinished()
{
    this->close();
}
