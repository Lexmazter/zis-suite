#ifndef IMPORTDIALOG_H
#define IMPORTDIALOG_H

#include <QWidget>
#include <QDialog>
#include <mainwindow.h>
#include "xlsxdocument.h"
#include "xlsxworksheet.h"
#include "xlsxworkbook.h"
#include "xlsxcellrange.h"
#include "CustomObjects/xlsxsheetmodel.h"

namespace Ui {
class ImportDialog;
}

class ImportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ImportDialog(QWidget *parent = 0, QString filePath = "");
    ~ImportDialog();

private slots:
    void on_closeButton_clicked();

    void on_importButton_clicked();

    void handleFinished();
private:
    Ui::ImportDialog *ui;
    QString filePath;
    QMap<QString, QString> headerMap;
    QFutureWatcher<void>* watcher;
    void importFunction();
};

#endif // IMPORTDIALOG_H
