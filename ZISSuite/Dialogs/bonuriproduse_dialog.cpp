#include "bonuriproduse_dialog.h"
#include "ui_bonuriproduse_dialog.h"
#include "Delegates//lineedit_completer_delegate.h"
#include <QListWidget>
#include <QCompleter>
#include <QDateTime>
#include "completer_lineedit.h"
#include "custommodel.h"
#include "Delegates/readonlydelegate.h"
#include "Forms/bonuriform.h"

BonuriProduse_Dialog::BonuriProduse_Dialog(QWidget *parent, QTableView *p_Table, int actualRaw) :
    QDialog(parent),
    ui(new Ui::BonuriProduse_Dialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Detalii Bon");
    QIcon bill_icon(":/Icons/Bill-48.png");
    this->setWindowIcon(bill_icon);

    this->parent = parent;
    ParrentTable = p_Table;
    CurrentRaw_ParrentTable = actualRaw;
    customTableModel = NULL;

    // Get all the headers so we know where to put what
    QMap<QString, int> headerMap;
    for(int i=0; i < ParrentTable->horizontalHeader()->model()->columnCount(); i++)
    {
        headerMap.insert(ParrentTable->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    itemsIndex = headerMap.find("Produse").value();
    amountIndex = headerMap.find("Cantitate").value();
    totalIndex = headerMap.find("Total").value();
    dataIndex = headerMap.find("Data").value();
    denumireIndex = headerMap.find("Denumire").value();
    tertIndex = headerMap.find("Tert").value();

    getProductsToModel();
    ui->tableView->setEditTriggers(QAbstractItemView::AllEditTriggers);
    ui->tableView->show();

    //after setting up the model table the horizontal header is available as well
    QMap<QString, int> actualTableHeaderMap;
    for(int i=0; i < ui->tableView->horizontalHeader()->model()->columnCount(); i++)
    {
        actualTableHeaderMap.insert(ui->tableView->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
    }

    int columnIndex_ID = actualTableHeaderMap.value("ID");
    int columnIndex_UM = actualTableHeaderMap.value("UM");
    columnIndex_Pret = actualTableHeaderMap.value("Pret");
    columnIndex_Amount = actualTableHeaderMap.value("Cantitate");

    //grab the available products amount out of nomenclator
    availableQuantityForProducts_Map = new QMap<int, float>;
    DbHelper::getAvailableQuantityForProductsById("Nomenclator", SoldItemsOutOfParent, availableQuantityForProducts_Map);

    spinBoxDelegate_Amount = new SpinBoxDelegate(ui->tableView);
    ui->tableView->setItemDelegateForColumn(columnIndex_Amount, spinBoxDelegate_Amount);
    spinBoxDelegate_Amount->setEditorRange(availableQuantityForProducts_Map);
    spinBoxDelegate_Amount->setShowToolTipFlag(true);
    changedProductsAmount = new QMap<int, float>;

    // Beautification
    ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

    //set text fields with relevant data from nomenclator
    QModelIndex index = ParrentTable->model()->index(CurrentRaw_ParrentTable, 2);
    QString temp_val = ParrentTable->model()->data(index).toString();

    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, itemsIndex);
    temp_val = ParrentTable->model()->data(index).toString();
    double d_val = DbHelper::getTotalBillValueForProducts("Nomenclator", temp_val);
    temp_val = QString("%1").arg(d_val);
    ui->totalLabel->setText(temp_val);

    QCompleter *completer = new QCompleter(this);
    completer->setParent(this);

    completer->setModel(DbHelper::getModelFromQuery(completer, "Nomenclator"));
    completer->setCompletionColumn(1);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setWrapAround(false);

    LineEditCompleterDelegate *completerDelegate = new LineEditCompleterDelegate(ui->tableView, completer);
    completerDelegate->setParent(this);

    ui->tableView->setItemDelegateForColumn(1, completerDelegate);

    // get data
    index = ParrentTable->model()->index(CurrentRaw_ParrentTable, dataIndex);
    temp_val = ParrentTable->model()->data(index).toString();

    QDateTime tempDate;

    if(!temp_val.isEmpty())
    {
        tempDate = tempDate.fromString(temp_val,"dd-MM-yyyy HH:mm");
        ui->dateTimeEdit->setDateTime(tempDate);
    }
    else
    {
        // set our current date as default
        ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());
    }

    // get our context menu up and running, last added appears first
    ui->tableView->insertAction(NULL,ui->actionDelete);
    ui->tableView->insertAction(ui->actionDelete,ui->actionSave);
    ui->tableView->insertAction(ui->actionSave,ui->actionRefresh);
    ui->tableView->insertAction(ui->actionRefresh,ui->actionAdd);

    // hide what's this
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    // disable editors for the following columns
    ReadOnlyDelegate* dummyDelegate = new ReadOnlyDelegate(ui->tableView);

    ui->tableView->setItemDelegateForColumn(columnIndex_ID, dummyDelegate);
    ui->tableView->setItemDelegateForColumn(columnIndex_UM, dummyDelegate);
    ui->tableView->setItemDelegateForColumn(columnIndex_Pret, dummyDelegate);
}

BonuriProduse_Dialog::~BonuriProduse_Dialog()
{
    delete ui;
}

void BonuriProduse_Dialog::getProductsToModel()
{
    SoldItemsOutOfParent =  ParrentTable->model()->index(CurrentRaw_ParrentTable, itemsIndex).data().toString();
    AmountSoldOutOfParent =  ParrentTable->model()->index(CurrentRaw_ParrentTable, amountIndex).data().toString();

    QString selectClause = "ID, Denumire, UM, Pret";

    if((SoldItemsOutOfParent == "NULL") || (AmountSoldOutOfParent == NULL))
    {
        SoldItemsOutOfParent == "0";
        AmountSoldOutOfParent == "0";
    }

    QString conditionQuery = "ID IN (" + SoldItemsOutOfParent + ")"; //this string can become pretty large hence a pointer to it shoud be used

    // Prevent memory leak on refresh
    if(customTableModel != NULL)
    {
        delete customTableModel;
    }

    customTableModel = new CustomModel(ui->tableView, DbHelper::getModelFromQuery(ui->tableView, "Nomenclator", &selectClause, &conditionQuery));
    ((CustomModel*)customTableModel)->setHeaderData(0, Qt::Horizontal, &selectClause);
    ((CustomModel*)customTableModel)->insertAdditionalColumn("Cantitate", &AmountSoldOutOfParent);
    ui->tableView->setModel(customTableModel);

    // connect our signal after creation
    connect(ui->tableView->model(), SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)),
            this, SLOT(onDataChanged(const QModelIndex&, const QModelIndex&)));
}

void BonuriProduse_Dialog::onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight)
{
    // Parameters are dummy only - see custommodel for details
    Q_UNUSED(topLeft);
    Q_UNUSED(bottomRight);

    ui->totalLabel->setText(QString::number(calculateTotalBillValue()));
}

void BonuriProduse_Dialog::on_actionDelete_triggered()
{
    // We now mark for deletion the row
    ((CustomModel*)ui->tableView->model())->removeRow(ui->tableView->currentIndex().row());
}

void BonuriProduse_Dialog::on_actionAdd_triggered()
{

    ui->tableView->model()->insertRow(ui->tableView->currentIndex().row());
}

void BonuriProduse_Dialog::on_actionRefresh_triggered()
{
    getProductsToModel();
}

void BonuriProduse_Dialog::on_actionSave_triggered()
{
    saveSoldItemsAndQuantities();

    double billValue = calculateTotalBillValue();

    if(billValue >0){
        ui->totalLabel->setText(QString::number(billValue));
        QVariant billValueData(billValue);
        QModelIndex index = ParrentTable->model()->index(CurrentRaw_ParrentTable, totalIndex);
        ParrentTable->model()->setData(index, billValueData);
    }

    QModelIndex index = ParrentTable->model()->index(CurrentRaw_ParrentTable, dataIndex);
    ParrentTable->model()->setData(index, ui->dateTimeEdit->dateTime().toString("dd-MM-yyyy HH:mm"));

    ((BonuriForm*)this->parent)->on_actionSave_triggered();

    //update the available amount in nomenclator
    QMap<int, float>::iterator i;
    QString setClause = "Stoc  = CASE ID";
    QString whereClause = "ID IN ( ";
    if(changedProductsAmount->size() >= 1){
        for(i = changedProductsAmount->begin(); i != changedProductsAmount->end(); ++i)
        {
            setClause += " WHEN " + QString::number(i.key()) + " THEN " + QString::number(i.value());
            whereClause += QString::number(i.key()) + " ,";
        }
        whereClause.remove(whereClause.lastIndexOf(" ,"), 2);
        setClause += " END ";
        whereClause += " )";
        DbHelper::updateQuery("Nomenclator", &setClause, &whereClause);
        availableQuantityForProducts_Map = changedProductsAmount;
    }

    // We close this dialog because when we add a new item in the parent model,
    // on refresh we get wrong data
    this->close();
}

/**
 * @abstract: void VanzariProduseDialog::calculateAndSave_TotalBillValue()
 * @brief: calculate and save the total bill value based on the sold items in the list
 */
double BonuriProduse_Dialog::calculateTotalBillValue()
{
    int noItems = ui->tableView->model()->rowCount();
    double billValue = 0;

    for(int i=0; i<noItems; i++)
    {
        QModelIndex index = ui->tableView->model()->index(i, columnIndex_Pret);
        double itemPrice = ui->tableView->model()->data(index).toDouble();

        index = ui->tableView->model()->index(i, columnIndex_Amount);
        double itemQuantity = ui->tableView->model()->data(index).toDouble();

        billValue += (itemPrice * itemQuantity);
    }

    return billValue;
}

/**
 * @abstract: void VanzariProduseDialog::saveSoldItems_And_Quantities()
 * @brief: save the sold items IDs and quantities into the corresponding parent table - factura
 */
void BonuriProduse_Dialog::saveSoldItemsAndQuantities()
{
    int noRows = ui->tableView->model()->rowCount();
    QString productsIDs = "";
    QString productsQantity = "";

    for(int i=0; i<noRows; i++)
    {
        if(i < noRows-1)
        {
            //use the separator after each element id in order to easisly split the string when parsing
            productsIDs += ui->tableView->model()->data(ui->tableView->model()->index(i, 0)).toString() + ", ";
            productsQantity += ui->tableView->model()->data(ui->tableView->model()->index(i, 4)).toString() + ", ";
        }
        else
        {
            //the last element in string do not need a separator
            productsIDs += ui->tableView->model()->data(ui->tableView->model()->index(i, 0)).toString();
            productsQantity += ui->tableView->model()->data(ui->tableView->model()->index(i, 4)).toString();
        }
    }

    ParrentTable->model()->setData(ParrentTable->model()->index(CurrentRaw_ParrentTable, itemsIndex), productsIDs, Qt::EditRole);
    ParrentTable->model()->setData(ParrentTable->model()->index(CurrentRaw_ParrentTable, amountIndex), productsQantity, Qt::EditRole);
}
void BonuriProduse_Dialog::on_tableView_doubleClicked(const QModelIndex &index)
{

}

void BonuriProduse_Dialog::on_tableView_clicked(const QModelIndex &index)
{
    // Implement total calculation without save here
}
