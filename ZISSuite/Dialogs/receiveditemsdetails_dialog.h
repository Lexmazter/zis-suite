#ifndef ReceivedItemsDetails_Dialog_H
#define ReceivedItemsDetails_Dialog_H

#include <QDialog>
#include <QWidget>
#include "dbhelper.h"
#include <QString>
#include <QTableView>
#include "Delegates/spinboxdelegate.h"
#include "Forms/newthird_form.h"

namespace Ui {
class ReceivedItemsDetails_Dialog;
}

class ReceivedItemsDetails_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit ReceivedItemsDetails_Dialog(QWidget *parent = 0, QTableView *p_Table=nullptr, int actualRaw=0);
    ~ReceivedItemsDetails_Dialog();

private:
    Ui::ReceivedItemsDetails_Dialog *ui;
    QAbstractItemDelegate* EmptyDelegate;
    QAbstractTableModel* customTableModel;
    QTableView *ParrentTable;
    QWidget *parent;
    int CurrentRaw_ParrentTable;
    int parent_ItemsIndex;
    int parent_AmountIndex;
    int parent_TotalIndex;
    int parent_DataIndex;
    int parent_DataScadentaIndex;
    int parent_DescriereIndex;
    int parent_TertIndex;
    int parent_AchitatIndex;
    QString SoldItemsOutOfParent;
    QString AmountSoldOutOfParent;

    int columnIndex_ID;
    int columnIndex_Denumire;
    int columnIndex_UM;
    int columnIndex_Pret;
    int columnIndex_Amount;
    int columnIndex_Stoc;
    QMap<int, float> *availableQuantityForProducts_Map;
    QMap<int, float> *changedProductsAmount;
    SpinBoxDelegate *spinBoxDelegate_Amount;
    NewThird_Form *NewThirdForm=nullptr;

    void getProductsToModel();
    void saveSoldItemsAndQuantities(void);
    double calculateTotalBillValue(void);

private slots:

    void on_actionDelete_triggered();

    void on_actionAdd_triggered();

    void on_actionRefresh_triggered();

    void on_actionSave_triggered();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_tableView_clicked(const QModelIndex &index);

    void onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight);
    void on_btnAddThird_clicked();
};

#endif // ReceivedItemsDetails_Dialog_H
