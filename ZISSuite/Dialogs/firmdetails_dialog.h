#ifndef firmdetails_dialog_H
#define firmdetails_dialog_H

#include <QDialog>
#include<QLabel>
#include "Forms/bankaccountsform.h"
#include "infocodfiscalhelper.h"

namespace Ui {
class FirmDetails_Dialog;
}

class FirmDetails_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit FirmDetails_Dialog(QWidget *parent = 0);
    ~FirmDetails_Dialog();
    void updateFrimDatafromDB(void);
    void updateBankAccountsFromDB(void);
    void sendMessage(QString& line, int level);
    static const QString FirmID;

    void setLabelAsPixmap(QLabel *label, QByteArray &outByteArray);

private slots:

    void on_btnSave_clicked();

    void on_btnExit_clicked();

    void on_btn_ChoseLogo_clicked();

    void on_btn_DeleteLogo_clicked();

    void on_btn_ChoseBackground_clicked();

    void on_btn_DeleteBackground_clicked();

    void on_btn_ChoseStamp_clicked();

    void on_btn_DeleteStamp_clicked();

    void on_btn_AccountSave_clicked();

    void on_btn_AccountExit_clicked();

    void on_btnTakeOverInfo_clicked();

    void infoFiscalLoadFinished(bool state);

private:
    Ui::FirmDetails_Dialog *ui;
    QVariantMap *firmData=nullptr;
    bool DB_Table_Empty_b;
    BankAccountsForm *bankForm = nullptr;
    InfoCodFiscalHelper* codFiscalHelper;
};

#endif // firmdetails_dialog_H
